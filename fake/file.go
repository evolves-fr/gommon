package fake

import (
	"io"
	"io/fs"
	"time"
)

// File implements fs.File and also fs.FileInfo.
type File struct {
	FileName   string
	Contents   string
	FileMode   fs.FileMode
	Offset     int
	StatError  error
	ReadError  error
	CloseError error
}

// fs.File methods.

func (f *File) Stat() (fs.FileInfo, error) {
	if f.StatError != nil {
		return nil, f.StatError
	}

	return f, nil
}

func (f *File) Read(reader []byte) (int, error) {
	if f.ReadError != nil {
		return 0, f.ReadError
	}

	if f.Offset >= len(f.Contents) {
		return 0, io.EOF
	}

	n := copy(reader, f.Contents[f.Offset:])
	f.Offset += n

	return n, nil
}

func (f *File) Close() error {
	return f.CloseError
}

// fs.FileInfo methods.

func (f *File) Name() string {
	// A bit of a cheat: we only have a basename, so that's also ok for FileInfo.
	return f.FileName
}

func (f *File) Size() int64 {
	return int64(len(f.Contents))
}

func (f *File) Mode() fs.FileMode {
	return f.FileMode
}

func (f *File) ModTime() time.Time {
	return time.Time{}
}

func (f *File) IsDir() bool {
	return false
}

func (f *File) Sys() any {
	return nil
}
