package encoding_test

import (
	"errors"
	"strings"
)

type Object struct {
	Value1 string
	Value2 string
}

func (v *Object) MarshalText() (text []byte, err error) {
	return []byte(v.Value1 + "|" + v.Value2), nil
}

func (v *Object) UnmarshalText(text []byte) error {
	values := strings.SplitN(string(text), "|", 2)

	if len(values) < 2 {
		return errors.New("invalid input")
	}

	v.Value1 = values[0]
	v.Value2 = values[1]

	return nil
}
