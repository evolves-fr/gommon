package tests_test

import (
	"errors"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/evolves-fr/gommon/tests"
)

func TestErrReadWriter(t *testing.T) {
	invalidReader := tests.ErrReadWriter(errors.New("readError"))
	readNumber, readError := invalidReader.Read(nil)
	assert.Error(t, readError)
	assert.Equal(t, 0, readNumber)

	invalidWriter := tests.ErrReadWriter(errors.New("writeError"))
	writeNumber, writeError := invalidWriter.Write(nil)
	assert.Error(t, writeError)
	assert.Equal(t, 0, writeNumber)
}
