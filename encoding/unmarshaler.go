package encoding

import (
	"encoding"

	"errors"
)

var ErrNotImplementTextUnmarshaler = errors.New("destination not implemented encoding.TextUnmarshaler")

type TextUnmarshaler encoding.TextUnmarshaler

func Unmarshal(data []byte, v any) error {
	fn, ok := v.(TextUnmarshaler)
	if !ok {
		return ErrNotImplementTextUnmarshaler
	}

	return fn.UnmarshalText(data)
}
