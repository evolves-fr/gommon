package gommon_test

import (
	"strings"
	"testing"

	"github.com/stretchr/testify/suite"
	"gitlab.com/evolves-fr/gommon"
)

func TestRandom(t *testing.T) {
	t.Parallel()

	suite.Run(t, new(RandomSuite))
}

type RandomSuite struct {
	suite.Suite
}

func (suite *RandomSuite) TestDefault() {
	value := gommon.Random(nil, 16)
	suite.Equal(16, len(value))
	suite.True(strings.ContainsAny(value, gommon.LowerTable+gommon.UpperTable+gommon.NumericTable+gommon.SymbolsTable))
}

func (suite *RandomSuite) TestCustom() {
	suite.Equal("aaa", gommon.Random(gommon.NewCustomRandom("a"), 3))
}

func (suite *RandomSuite) TestLowerLetters() {
	value := gommon.Random(gommon.NewLowerLettersRandom(), 16)
	suite.Equal(16, len(value))
	suite.True(strings.ContainsAny(value, gommon.LowerTable))
}

func (suite *RandomSuite) TestUpperLetters() {
	value := gommon.Random(gommon.NewUpperLettersRandom(), 16)
	suite.Equal(16, len(value))
	suite.True(strings.ContainsAny(value, gommon.UpperTable))
}

func (suite *RandomSuite) TestLetters() {
	value := gommon.Random(gommon.NewLettersRandom(), 16)
	suite.Equal(16, len(value))
	suite.True(strings.ContainsAny(value, gommon.LowerTable+gommon.UpperTable))
}

func (suite *RandomSuite) TestNumber() {
	value := gommon.Random(gommon.NewNumberRandom(), 16)
	suite.Equal(16, len(value))
	suite.True(strings.ContainsAny(value, gommon.NumericTable))
}

func (suite *RandomSuite) TestAlphanumeric() {
	value := gommon.Random(gommon.NewAlphanumericRandom(), 16)
	suite.Equal(16, len(value))
	suite.True(strings.ContainsAny(value, gommon.LowerTable+gommon.UpperTable+gommon.NumericTable))
}

func (suite *RandomSuite) TestStandard() {
	value := gommon.Random(gommon.NewStandardRandom(), 16)
	suite.Equal(16, len(value))
	suite.True(strings.ContainsAny(value, gommon.LowerTable+gommon.UpperTable+gommon.NumericTable+gommon.SymbolsTable))
}
