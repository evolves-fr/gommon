package gommon

import (
	"errors"
	"fmt"
	"reflect"
)

func NewStructMapEncoder(in any) *StructMapEncoder { return &StructMapEncoder{in: in} }

type StructMapEncoder struct {
	in  any
	tag string
}

func (enc *StructMapEncoder) SetTag(tag string) *StructMapEncoder {
	enc.tag = tag

	return enc
}

func (enc *StructMapEncoder) Encode(out map[string]any) error {
	// Verify if output is not set
	if enc.in == nil {
		return errors.New("input is nil")
	}

	// Verify if output is not set
	if out == nil {
		return errors.New("output is nil")
	}

	// Reflect input
	value := reflect.ValueOf(enc.in)

	// Verify if input is not struct
	if value.Type().Kind() != reflect.Struct {
		return errors.New("invalid input type, only struct")
	}

	// Set output from input
	for i := 0; i < value.NumField(); i++ {
		field := value.Field(i)

		// Check if unexported field
		if !field.CanInterface() {
			continue
		}

		// Get tag label, or use field name
		label := Default(value.Type().Field(i).Tag.Get(enc.tag), value.Type().Field(i).Name)

		// Encode field
		enc.encode(label, out, field)
	}

	return nil
}

func (enc *StructMapEncoder) encode(label string, val map[string]any, value reflect.Value) {
	switch value.Kind() {
	case reflect.Ptr:
		if !value.IsNil() {
			enc.encode(label, val, value.Elem())
		}
	case reflect.Struct:
		// Prepare child of output
		val[label] = make(map[string]any)

		// Load new encoder for child
		subEnc := NewStructMapEncoder(value.Interface()).SetTag(enc.tag)

		// Encode child
		_ = subEnc.Encode(val[label].(map[string]any))
	default:
		val[label] = value.Interface()
	}
}

func NewStructMapDecoder(in map[string]any) *StructMapDecoder { return &StructMapDecoder{in: in} }

type StructMapDecoder struct {
	in  map[string]any
	tag string
}

func (dec *StructMapDecoder) SetTag(tag string) *StructMapDecoder {
	dec.tag = tag

	return dec
}

func (dec *StructMapDecoder) Decode(out any) error {
	// Verify if output is not set
	if dec.in == nil {
		return errors.New("input is nil")
	}

	// Verify if output is not set
	if out == nil {
		return errors.New("output is nil")
	}

	// Reflect output
	value := reflect.ValueOf(out)

	// Remove output pointer
	for value.Kind() == reflect.Ptr {
		value = value.Elem()
	}

	// Verify if output is not struct
	if value.Type().Kind() != reflect.Struct {
		return fmt.Errorf("%s should be struct", value.Type().Name())
	}

	// Verify if output is writable
	if !value.CanAddr() || !value.CanInterface() {
		return fmt.Errorf("%s should be addressable", value.Type().Name())
	}

	// Set output from input
	for i := 0; i < value.NumField(); i++ {
		field := value.Field(i)

		// Get tag label, or use field name
		label := Default(value.Type().Field(i).Tag.Get(dec.tag), value.Type().Field(i).Name)

		// Get input value
		val, exist := dec.in[label]
		if !exist {
			continue
		}

		// Decode field
		if err := dec.decode(label, val, field); err != nil {
			return err
		}
	}

	return nil
}

func (dec *StructMapDecoder) decode(label string, val any, value reflect.Value) error {
	switch value.Kind() {
	case reflect.Ptr:
		// Prepare addressable sub-field value
		subValue := reflect.New(value.Type().Elem())

		// Decode sub-field
		if err := dec.decode(label, val, subValue.Elem()); err != nil {
			return err
		}

		// Set sub-field to field
		value.Set(subValue)
	case reflect.Struct:
		// Verify map type
		sub, valid := val.(map[string]any)
		if !valid {
			return fmt.Errorf("invalid map type")
		}

		// Prepare addressable sub-field value
		subValue := reflect.New(value.Type())

		// Load new decoder for child
		subDec := NewStructMapDecoder(sub).SetTag(dec.tag)

		// Decode map to sub-field struct
		if err := subDec.Decode(subValue.Interface()); err != nil {
			return err
		}

		// Set sub-field to field
		value.Set(subValue.Elem())
	default:
		// Reflect map value
		sub := reflect.ValueOf(val)

		// Verify if compatible
		if !sub.CanConvert(value.Type()) {
			return nil
		}

		// Set sub-field to field
		value.Set(sub.Convert(value.Type()))
	}

	return nil
}
