package encoding

import (
	"encoding"
	"errors"
)

var ErrNotImplementTextMarshaler = errors.New("destination not implemented encoding.TextMarshaler")

type TextMarshaler encoding.TextMarshaler

func Marshal(v any) ([]byte, error) {
	fn, ok := v.(TextMarshaler)
	if !ok {
		return nil, ErrNotImplementTextMarshaler
	}

	return fn.MarshalText()
}
