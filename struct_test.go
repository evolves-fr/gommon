package gommon_test

import (
	"testing"

	"github.com/stretchr/testify/suite"
	"gitlab.com/evolves-fr/gommon"
)

func TestStructMap(t *testing.T) {
	t.Parallel()

	suite.Run(t, new(StructMapSuite))
}

type StructMapSuite struct{ suite.Suite }

func (suite *StructMapSuite) TestEncode() {
	suite.Run("inputNil", func() {
		err := gommon.NewStructMapEncoder(nil).Encode(make(map[string]any))
		suite.EqualError(err, "input is nil")
	})

	suite.Run("outputNil", func() {
		err := gommon.NewStructMapEncoder(struct{}{}).Encode(nil)
		suite.EqualError(err, "output is nil")
	})

	suite.Run("inputInvalid", func() {
		err := gommon.NewStructMapEncoder(int64(123)).Encode(make(map[string]any))
		suite.EqualError(err, "invalid input type, only struct")
	})

	suite.Run("valid", func() {
		in := StructMapTest{
			Key1: "value1",
			Key2: []string{"value2.1", "value2.2", "value2.3"},
			Key3: 3.1,
			Key4: StructMapAnotherTest{SubKey1: "subValue1", SubKey2: "subValue2"},
			Key5: &StructMapAnotherTest{SubKey1: "subPointerValue1", SubKey2: "subPointerValue2"},
			Key6: nil,
			key7: "value6",
		}

		out := make(map[string]any)

		res := map[string]any{
			"key1": "value1",
			"key2": []string{"value2.1", "value2.2", "value2.3"},
			"key3": float32(3.1),
			"key4": map[string]any{"subKey1": "subValue1", "subKey2": "subValue2"},
			"key5": map[string]any{"subKey1": "subPointerValue1", "subKey2": "subPointerValue2"},
		}

		enc := gommon.NewStructMapEncoder(in).SetTag("json")
		err := enc.Encode(out)

		suite.NoError(err)
		suite.Equal(res, out)
	})
}

func (suite *StructMapSuite) TestDecode() {
	suite.Run("inputNil", func() {
		err := gommon.NewStructMapDecoder(nil).Decode(new(struct{}))
		suite.EqualError(err, "input is nil")
	})

	suite.Run("outputNil", func() {
		err := gommon.NewStructMapDecoder(make(map[string]any)).Decode(nil)
		suite.EqualError(err, "output is nil")
	})

	suite.Run("outputTypeInvalid", func() {
		err := gommon.NewStructMapDecoder(make(map[string]any)).Decode(new(int64))
		suite.EqualError(err, "int64 should be struct")
	})

	suite.Run("outputNotAdressable", func() {
		err := gommon.NewStructMapDecoder(make(map[string]any)).Decode(StructMapTest{})
		suite.EqualError(err, "StructMapTest should be addressable")
	})

	suite.Run("invalidMapType", func() {
		in := map[string]any{
			"key1": "value1",
			"key2": []string{"value2.1", "value2.2", "value2.3"},
			"key3": "3.1",
			"key4": map[string]any{"subKey1": "subValue1", "subKey2": "subValue2"},
			"key5": map[string]string{"subKey1": "subPointerValue1", "subKey2": "subPointerValue2"},
		}

		var out StructMapTest

		dec := gommon.NewStructMapDecoder(in).SetTag("json")
		err := dec.Decode(&out)

		suite.EqualError(err, "invalid map type")
	})

	suite.Run("error", func() {
		in := map[string]any{
			"key1": "value1",
			"key2": []string{"value2.1", "value2.2", "value2.3"},
			"key3": "3.1",
			"key4": (map[string]any)(nil),
			"key5": (map[string]any)(nil),
		}

		var out StructMapTest

		dec := gommon.NewStructMapDecoder(in).SetTag("json")
		err := dec.Decode(&out)

		suite.EqualError(err, "input is nil")
	})

	suite.Run("valid", func() {
		in := map[string]any{
			"key1": "value1",
			"key2": []string{"value2.1", "value2.2", "value2.3"},
			"key3": float32(3.1),
			"key4": map[string]any{"subKey1": "subValue1", "subKey2": "subValue2"},
			"key5": map[string]any{"subKey1": "subPointerValue1", "subKey2": "subPointerValue2"},
		}

		var out StructMapTest

		res := StructMapTest{
			Key1: "value1",
			Key2: []string{"value2.1", "value2.2", "value2.3"},
			Key3: 3.1,
			Key4: StructMapAnotherTest{SubKey1: "subValue1", SubKey2: "subValue2"},
			Key5: &StructMapAnotherTest{SubKey1: "subPointerValue1", SubKey2: "subPointerValue2"},
		}

		dec := gommon.NewStructMapDecoder(in).SetTag("json")
		err := dec.Decode(&out)

		suite.NoError(err)
		suite.Equal(res, out)
	})
}

type (
	StructMapTest struct {
		Key1 string                `json:"key1"`
		Key2 []string              `json:"key2"`
		Key3 float32               `json:"key3"`
		Key4 StructMapAnotherTest  `json:"key4"`
		Key5 *StructMapAnotherTest `json:"key5"`
		Key6 *StructMapTest        `json:"key6"`
		key7 string
	}
	StructMapAnotherTest struct {
		SubKey1 string `json:"subKey1"`
		SubKey2 string `json:"subKey2"`
	}
)
