package env_test

import (
	"bytes"
	"errors"
	"os"
	"testing"

	"github.com/stretchr/testify/suite"
	"gitlab.com/evolves-fr/gommon/env"
	"gitlab.com/evolves-fr/gommon/tests"
)

func TestEnv(t *testing.T) {
	suite.Run(t, new(EnvSuite))
}

type EnvSuite struct {
	suite.Suite

	environ env.Env
	dotenv  env.Env
}

func (suite *EnvSuite) SetupTest() {
	os.Clearenv()
	suite.T().Setenv("ENV_KEY1", "value1")
	suite.T().Setenv("ENV_KEY2", "value2")
	suite.T().Setenv("ENV_STRING", "string")

	var dotenv bytes.Buffer
	dotenv.WriteString("DOTENV_KEY1=value1\n")
	dotenv.WriteString("DOTENV_KEY2=value2\n")
	dotenv.WriteString("DOTENV_STRING=string\n")

	var err error

	suite.environ, err = env.LoadEnviron()
	suite.NoError(err)

	suite.dotenv, err = env.LoadDotEnv(&dotenv)
	suite.NoError(err)

	readerNil, readerErr := env.LoadDotEnv(tests.ErrReadWriter(errors.New("reader error")))
	suite.Error(readerErr)
	suite.Nil(readerNil)
}

func (suite *EnvSuite) TestKeys() {
	suite.Equal([]string{"ENV_KEY1", "ENV_KEY2", "ENV_STRING"}, suite.environ.Keys())
	suite.Equal([]string{"DOTENV_KEY1", "DOTENV_KEY2", "DOTENV_STRING"}, suite.dotenv.Keys())
}

func (suite *EnvSuite) TestKeysWithRegex() {
	suite.Equal([]string{"ENV_KEY1", "ENV_KEY2"}, suite.environ.KeysWithRegex("^ENV_KEY"))
	suite.Equal([]string{"DOTENV_KEY1", "DOTENV_KEY2"}, suite.dotenv.KeysWithRegex("^DOTENV_KEY"))

	suite.Equal([]string{}, suite.environ.KeysWithRegex("^UNKNOWN"))
	suite.Equal([]string{}, suite.dotenv.KeysWithRegex("^UNKNOWN"))
}

func (suite *EnvSuite) TestHas() {
	suite.Equal(true, suite.environ.Has("ENV_STRING"))
	suite.Equal(true, suite.dotenv.Has("DOTENV_STRING"))

	suite.Equal(false, suite.environ.Has("ENV_UNKNOWN"))
	suite.Equal(false, suite.dotenv.Has("DOTENV_UNKNOWN"))
}

func (suite *EnvSuite) TestHasWithRegex() {
	suite.Equal(true, suite.environ.HasWithRegex("^ENV_KEY"))
	suite.Equal(true, suite.dotenv.HasWithRegex("^DOTENV_KEY"))

	suite.Equal(false, suite.environ.HasWithRegex("^ENV_UNKNOWN"))
	suite.Equal(false, suite.dotenv.HasWithRegex("^DOTENV_UNKNOWN"))
}

func (suite *EnvSuite) TestGet() {
	suite.Equal("string", suite.environ.Get("ENV_STRING"))
	suite.Equal("string", suite.dotenv.Get("DOTENV_STRING"))

	suite.Equal("", suite.environ.Get("ENV_UNKNOWN"))
	suite.Equal("", suite.dotenv.Get("DOTENV_UNKNOWN"))
}

func (suite *EnvSuite) TestClone() {
	environ := env.Environment{"ENV_KEY1": "value1", "ENV_KEY2": "value2", "ENV_STRING": "string"}
	suite.Equal(environ, suite.environ.Clone())
	dotenv := env.Environment{"DOTENV_KEY1": "value1", "DOTENV_KEY2": "value2", "DOTENV_STRING": "string"}
	suite.Equal(dotenv, suite.dotenv.Clone())
}

func (suite *EnvSuite) TestEncode() {
	environRaw := []byte("ENV_KEY1=value1\nENV_KEY2=value2\nENV_STRING=string\n")
	suite.Equal(environRaw, suite.environ.Encode())

	dotenvRaw := []byte("DOTENV_KEY1=value1\nDOTENV_KEY2=value2\nDOTENV_STRING=string\n")
	suite.Equal(dotenvRaw, suite.dotenv.Encode())
}
