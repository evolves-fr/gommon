package env

import "bytes"

type Unmarshaler interface {
	UnmarshalENV([]byte) error
}

func Unmarshal(v any) error {
	return UnmarshalWithPrefix(v, "")
}

func UnmarshalWithPrefix(v any, prefix string) error {
	return NewDecoder(Environ()).Prefix(prefix).Decode(v)
}

func UnmarshalDotEnv(data []byte, v any) error {
	return UnmarshalDotEnvWithPrefix(data, v, "")
}

func UnmarshalDotEnvWithPrefix(data []byte, v any, prefix string) error {
	return NewDecoder(bytes.NewReader(data)).Prefix(prefix).Decode(v)
}
