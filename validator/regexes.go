package validator

import "regexp"

const (
	domainRegexString = `(?im)^(?:[_a-z0-9](?:[_a-z0-9-]{0,61}[a-z0-9])?\.)+(?:[a-z](?:[a-z0-9-]{0,61}[a-z0-9])?)?$`
)

var domainRegex = regexp.MustCompile(domainRegexString)
