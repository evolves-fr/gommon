package gommon_test

import (
	"bytes"
	"errors"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/suite"
	"gitlab.com/evolves-fr/gommon"
	"gitlab.com/evolves-fr/gommon/tests"
)

func TestNewSubstitution(t *testing.T) {
	validInput := `{{ true }}`
	errorInput := `{{ "on" | unknown }}`

	var writer bytes.Buffer
	assert.NoError(t, gommon.NewSubstitution(strings.NewReader(validInput)).Execute(&writer))
	assert.Equal(t, "true", writer.String())

	subst := gommon.NewSubstitution(strings.NewReader(`{{ toto }}-{{ ok }}`))
	subst.Funcs(map[string]any{
		"toto": func() string { return "tata" },
		"ok":   func() bool { return true },
	})
	var fmWriter bytes.Buffer
	assert.NoError(t, subst.Execute(&fmWriter))
	assert.Equal(t, "tata-true", fmWriter.String())

	invalidReader := tests.ErrReadWriter(errors.New("readError"))
	invalidWriter := tests.ErrReadWriter(errors.New("writeError"))
	assert.Error(t, gommon.NewSubstitution(nil).Execute(&bytes.Buffer{}))
	assert.Error(t, gommon.NewSubstitution(strings.NewReader(validInput)).Execute(nil))
	assert.Error(t, gommon.NewSubstitution(invalidReader).Execute(&bytes.Buffer{}))
	assert.Error(t, gommon.NewSubstitution(strings.NewReader(validInput)).Execute(invalidWriter))
	assert.Error(t, gommon.NewSubstitution(strings.NewReader(errorInput)).Execute(&bytes.Buffer{}))
}

func TestSubst(t *testing.T) {
	suite.Run(t, new(SubstSuite))
}

type SubstSuite struct {
	suite.Suite
}

func (suite *SubstSuite) TestBool() {
	output, err := gommon.Subst([]byte(`{{ "on" | bool }}`))
	suite.NoError(err)
	suite.Equal("true", string(output))

	output, err = gommon.Subst([]byte(`{{ "invalid" | bool }}`))
	suite.Error(err)
	suite.Nil(output)
}

func (suite *SubstSuite) TestInt() {
	output, err := gommon.Subst([]byte(`{{ "-9" | int }}`))
	suite.NoError(err)
	suite.Equal("-9", string(output))

	output, err = gommon.Subst([]byte(`{{ "invalid" | int }}`))
	suite.Error(err)
	suite.Nil(output)
}

func (suite *SubstSuite) TestInt8() {
	output, err := gommon.Subst([]byte(`{{ "-9" | int8 }}`))
	suite.NoError(err)
	suite.Equal("-9", string(output))

	output, err = gommon.Subst([]byte(`{{ "invalid" | int8 }}`))
	suite.Error(err)
	suite.Nil(output)
}

func (suite *SubstSuite) TestInt16() {
	output, err := gommon.Subst([]byte(`{{ "-9" | int16 }}`))
	suite.NoError(err)
	suite.Equal("-9", string(output))

	output, err = gommon.Subst([]byte(`{{ "invalid" | int16 }}`))
	suite.Error(err)
	suite.Nil(output)
}

func (suite *SubstSuite) TestInt32() {
	output, err := gommon.Subst([]byte(`{{ "-9" | int32 }}`))
	suite.NoError(err)
	suite.Equal("-9", string(output))

	output, err = gommon.Subst([]byte(`{{ "invalid" | int32 }}`))
	suite.Error(err)
	suite.Nil(output)
}

func (suite *SubstSuite) TestInt64() {
	output, err := gommon.Subst([]byte(`{{ "-9" | int64 }}`))
	suite.NoError(err)
	suite.Equal("-9", string(output))

	output, err = gommon.Subst([]byte(`{{ "invalid" | int64 }}`))
	suite.Error(err)
	suite.Nil(output)
}

func (suite *SubstSuite) TestUint() {
	output, err := gommon.Subst([]byte(`{{ "9" | uint }}`))
	suite.NoError(err)
	suite.Equal("9", string(output))

	output, err = gommon.Subst([]byte(`{{ "invalid" | uint }}`))
	suite.Error(err)
	suite.Nil(output)
}

func (suite *SubstSuite) TestUint8() {
	output, err := gommon.Subst([]byte(`{{ "9" | uint8 }}`))
	suite.NoError(err)
	suite.Equal("9", string(output))

	output, err = gommon.Subst([]byte(`{{ "invalid" | uint8 }}`))
	suite.Error(err)
	suite.Nil(output)
}

func (suite *SubstSuite) TestUint16() {
	output, err := gommon.Subst([]byte(`{{ "9" | uint16 }}`))
	suite.NoError(err)
	suite.Equal("9", string(output))

	output, err = gommon.Subst([]byte(`{{ "invalid" | uint16 }}`))
	suite.Error(err)
	suite.Nil(output)
}

func (suite *SubstSuite) TestUint32() {
	output, err := gommon.Subst([]byte(`{{ "9" | uint32 }}`))
	suite.NoError(err)
	suite.Equal("9", string(output))

	output, err = gommon.Subst([]byte(`{{ "invalid" | uint32 }}`))
	suite.Error(err)
	suite.Nil(output)
}

func (suite *SubstSuite) TestUint64() {
	output, err := gommon.Subst([]byte(`{{ "9" | uint64 }}`))
	suite.NoError(err)
	suite.Equal("9", string(output))

	output, err = gommon.Subst([]byte(`{{ "invalid" | uint64 }}`))
	suite.Error(err)
	suite.Nil(output)
}

func (suite *SubstSuite) TestUintPtr() {
	output, err := gommon.Subst([]byte(`{{ "9" | uintPtr }}`))
	suite.NoError(err)
	suite.Equal("9", string(output))

	output, err = gommon.Subst([]byte(`{{ "invalid" | uintPtr }}`))
	suite.Error(err)
	suite.Nil(output)
}

func (suite *SubstSuite) TestFloat32() {
	output, err := gommon.Subst([]byte(`{{ "9.1" | float32 }}`))
	suite.NoError(err)
	suite.Equal("9.1", string(output))

	output, err = gommon.Subst([]byte(`{{ "invalid" | float32 }}`))
	suite.Error(err)
	suite.Nil(output)
}

func (suite *SubstSuite) TestFloat64() {
	output, err := gommon.Subst([]byte(`{{ "9.1" | float64 }}`))
	suite.NoError(err)
	suite.Equal("9.1", string(output))

	output, err = gommon.Subst([]byte(`{{ "invalid" | float64 }}`))
	suite.Error(err)
	suite.Nil(output)
}

func (suite *SubstSuite) TestComplex64() {
	output, err := gommon.Subst([]byte(`{{ "9.1" | complex64 }}`))
	suite.NoError(err)
	suite.Equal("(9.1+0i)", string(output))

	output, err = gommon.Subst([]byte(`{{ "invalid" | complex64 }}`))
	suite.Error(err)
	suite.Nil(output)
}

func (suite *SubstSuite) TestComplex128() {
	output, err := gommon.Subst([]byte(`{{ "9.1" | complex128 }}`))
	suite.NoError(err)
	suite.Equal("(9.1+0i)", string(output))

	output, err = gommon.Subst([]byte(`{{ "invalid" | complex128 }}`))
	suite.Error(err)
	suite.Nil(output)
}

func (suite *SubstSuite) TestDuration() {
	output, err := gommon.Subst([]byte(`{{ "90s" | duration }}`))
	suite.NoError(err)
	suite.Equal("1m30s", string(output))

	output, err = gommon.Subst([]byte(`{{ "invalid" | duration }}`))
	suite.Error(err)
	suite.Nil(output)
}

func (suite *SubstSuite) TestDSN() {
	output, err := gommon.Subst([]byte(`{{ ("s3://username:password@endpoint/path" | dsn).Endpoint }}`))
	suite.NoError(err)
	suite.Equal("endpoint", string(output))

	output, err = gommon.Subst([]byte(`{{ ":/unknown" | dsn }}`))
	suite.Error(err)
	suite.Nil(output)
}

func (suite *SubstSuite) TestTimestamp() {
	output, err := gommon.Subst([]byte(`{{ ("1673955869" | timestamp).Time.UTC }}`))
	suite.NoError(err)
	suite.Equal("2023-01-17 11:44:29 +0000 UTC", string(output))
}

func (suite *SubstSuite) TestTime() {
	output, err := gommon.Subst([]byte(`{{ ("2023-01-17T12:40:05Z" | time).Format "02 Jan 06 15:04 MST" }}`))
	suite.NoError(err)
	suite.Equal("17 Jan 23 12:40 UTC", string(output))

	output, err = gommon.Subst([]byte(`{{ "invalid" | time }}`))
	suite.Error(err)
	suite.Nil(output)
}

func (suite *SubstSuite) TestBase64() {
	output, err := gommon.Subst([]byte(`{{ "dG90bw==" | base64 }}`))
	suite.NoError(err)
	suite.Equal("toto", string(output))

	output, err = gommon.Subst([]byte(`{{ "invalid" | base64 }}`))
	suite.Error(err)
	suite.Nil(output)
}

func (suite *SubstSuite) TestURL() {
	output, err := gommon.Subst([]byte(`{{ ("http://localhost:9999/path" | url).Host }}`))
	suite.NoError(err)
	suite.Equal("localhost:9999", string(output))

	output, err = gommon.Subst([]byte(`{{ ":/invalid" | url }}`))
	suite.Error(err)
	suite.Nil(output)
}

func (suite *SubstSuite) TestValues() {
	output, err := gommon.Subst([]byte(`{{ ("key1=value1&key2=value2" | values).Get "key2" }}`))
	suite.NoError(err)
	suite.Equal("value2", string(output))

	output, err = gommon.Subst([]byte(`{{ "invalid;" | values }}`))
	suite.Error(err)
	suite.Nil(output)
}

func (suite *SubstSuite) TestDefault() {
	output, err := gommon.Subst([]byte(`{{ "tata" | default "toto" }}`))
	suite.NoError(err)
	suite.Equal("tata", string(output))

	output, err = gommon.Subst([]byte(`{{ "" | default "toto" }}`))
	suite.NoError(err)
	suite.Equal("toto", string(output))
}

func (suite *SubstSuite) TestTernary() {
	output, err := gommon.Subst([]byte(`{{ ternary true "value1" "value2" }}`))
	suite.NoError(err)
	suite.Equal("value1", string(output))

	output, err = gommon.Subst([]byte(`{{ ternary false "value1" "value2" }}`))
	suite.NoError(err)
	suite.Equal("value2", string(output))
}

func (suite *SubstSuite) TestEmpty() {
	output, err := gommon.Subst([]byte(`{{ "" | empty }}`))
	suite.NoError(err)
	suite.Equal("true", string(output))

	output, err = gommon.Subst([]byte(`{{ "false" | empty }}`))
	suite.NoError(err)
	suite.Equal("false", string(output))
}

func (suite *SubstSuite) TestEnv() {
	suite.T().Setenv("MY_ENV_VAR", "value")

	output, err := gommon.Subst([]byte(`{{ "MY_ENV_VAR" | env }}`))
	suite.NoError(err)
	suite.Equal("value", string(output))

	output, err = gommon.Subst([]byte(`{{ "UNKNOWN_VAR" | env }}`))
	suite.NoError(err)
	suite.Equal("", string(output))
}

func (suite *SubstSuite) TestIndent() {
	output, err := gommon.Subst([]byte(`{{ "test" | indent 4 }}`))
	suite.NoError(err)
	suite.Equal("    test", string(output))
}

func (suite *SubstSuite) TestNIndent() {
	output, err := gommon.Subst([]byte(`{{ "test" | nindent 4 }}`))
	suite.NoError(err)
	suite.Equal("\n    test", string(output))
}

func (suite *SubstSuite) TestRegexMatch() {
	output, err := gommon.Subst([]byte(`{{ "toto" | regexMatch "^toto$" }}`))
	suite.NoError(err)
	suite.Equal("true", string(output))

	output, err = gommon.Subst([]byte(`{{ "toto" | regexMatch "^tata$" }}`))
	suite.NoError(err)
	suite.Equal("false", string(output))
}

func (suite *SubstSuite) TestContains() {
	output, err := gommon.Subst([]byte(`{{ "toto" | contains "to" }}`))
	suite.NoError(err)
	suite.Equal("true", string(output))

	output, err = gommon.Subst([]byte(`{{ "toto" | contains "ta" }}`))
	suite.NoError(err)
	suite.Equal("false", string(output))
}

func (suite *SubstSuite) TestHasPrefix() {
	output, err := gommon.Subst([]byte(`{{ "hello" | hasPrefix "he" }}`))
	suite.NoError(err)
	suite.Equal("true", string(output))

	output, err = gommon.Subst([]byte(`{{ "hello" | hasPrefix "lo" }}`))
	suite.NoError(err)
	suite.Equal("false", string(output))
}

func (suite *SubstSuite) TestHasSuffix() {
	output, err := gommon.Subst([]byte(`{{ "hello" | hasSuffix "lo" }}`))
	suite.NoError(err)
	suite.Equal("true", string(output))

	output, err = gommon.Subst([]byte(`{{ "hello" | hasSuffix "he" }}`))
	suite.NoError(err)
	suite.Equal("false", string(output))
}

func (suite *SubstSuite) TestTrimSpace() {
	output, err := gommon.Subst([]byte(`{{ "   hello world   " | trimSpace }}`))
	suite.NoError(err)
	suite.Equal("hello world", string(output))
}

func (suite *SubstSuite) TestUpper() {
	output, err := gommon.Subst([]byte(`{{ "hello" | upper }}`))
	suite.NoError(err)
	suite.Equal("HELLO", string(output))
}

func (suite *SubstSuite) TestLower() {
	output, err := gommon.Subst([]byte(`{{ "HELLO" | lower }}`))
	suite.NoError(err)
	suite.Equal("hello", string(output))
}

func (suite *SubstSuite) TestTitle() {
	output, err := gommon.Subst([]byte(`{{ "HELLO" | lower }}`))
	suite.NoError(err)
	suite.Equal("hello", string(output))
}

func (suite *SubstSuite) TestSplit() {
	output, err := gommon.Subst([]byte(`{{ "hello;world" | split ";" }}`))
	suite.NoError(err)
	suite.Equal("[hello world]", string(output))
}

func (suite *SubstSuite) TestPathBase() {
	output, err := gommon.Subst([]byte(`{{ "item1/item2/item3" | pathBase }}`))
	suite.NoError(err)
	suite.Equal("item3", string(output))
}

func (suite *SubstSuite) TestPathDir() {
	output, err := gommon.Subst([]byte(`{{ "item1/item2/item3" | pathDir }}`))
	suite.NoError(err)
	suite.Equal("item1/item2", string(output))
}

func (suite *SubstSuite) TestPathClean() {
	output, err := gommon.Subst([]byte(`{{ "item1/item2/../item3" | pathClean }}`))
	suite.NoError(err)
	suite.Equal("item1/item3", string(output))
}

func (suite *SubstSuite) TestPathExt() {
	output, err := gommon.Subst([]byte(`{{ "item1/item2.json" | pathExt }}`))
	suite.NoError(err)
	suite.Equal(".json", string(output))
}

func (suite *SubstSuite) TestPathIsAbs() {
	output, err := gommon.Subst([]byte(`{{ "item1/item2/item3" | pathIsAbs }}`))
	suite.NoError(err)
	suite.Equal("false", string(output))

	output, err = gommon.Subst([]byte(`{{ "/item1/item2/item3" | pathIsAbs }}`))
	suite.NoError(err)
	suite.Equal("true", string(output))
}

func (suite *SubstSuite) TestSemverCompare() {
	output, err := gommon.Subst([]byte(`{{ "1.2.3" | semverCompare "^1.0.0" }}`))
	suite.NoError(err)
	suite.Equal("true", string(output))

	output, err = gommon.Subst([]byte(`{{ "1.2.3" | semverCompare "^2.0.0" }}`))
	suite.NoError(err)
	suite.Equal("false", string(output))
}
