package errors_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/evolves-fr/gommon/errors"
)

func TestNew(t *testing.T) {
	t.Parallel()

	assert.EqualError(t, errors.New("hello world"), "hello world")
}
