package signature

import (
	"net/http"
	"time"
)

var std = New("APIAuth", time.Minute)

func StandardSignature() Signature {
	return std
}

func Generate(req *http.Request, clientID, secretID string) string {
	return std.Generate(req, clientID, secretID)
}

func Sign(req *http.Request, clientID, secretID string) {
	std.Sign(req, clientID, secretID)
}

func Check(req *http.Request, credentials map[string]string) error {
	return std.Check(req, credentials)
}
