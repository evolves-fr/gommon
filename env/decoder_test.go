package env_test

import (
	"errors"
	"reflect"
	"strings"
	"testing"

	"github.com/stretchr/testify/suite"
	"gitlab.com/evolves-fr/gommon"
	"gitlab.com/evolves-fr/gommon/env"
	"gitlab.com/evolves-fr/gommon/tests"
)

var reader = strings.NewReader

func TestDecoder(t *testing.T) {
	t.Parallel()

	suite.Run(t, new(DecoderSuite))
}

type DecoderSuite struct {
	suite.Suite
}

func (suite *DecoderSuite) TestReaderError() {
	suite.EqualError(env.NewDecoder(tests.ErrReadWriter(errors.New("reader error"))).Decode(nil), "reader error")
}

func (suite *DecoderSuite) TestNilType() {
	suite.EqualError(env.NewDecoder(reader("")).Decode(nil), "type is invalid")
}

func (suite *DecoderSuite) TestErrorType() {
	var test *error
	suite.EqualError(env.NewDecoder(reader("")).Decode(&test), "type is invalid")
}

func (suite *DecoderSuite) TestChanType() {
	var test chan bool
	suite.EqualError(env.NewDecoder(reader("")).Decode(&test), "type `chan` not available")
}

func (suite *DecoderSuite) TestFuncType() {
	var test func()
	suite.EqualError(env.NewDecoder(reader("")).Decode(&test), "type `func` not available")
}

func (suite *DecoderSuite) TestInterfaceType() {
	var test func()
	suite.EqualError(env.NewDecoder(reader("")).Decode(&test), "type `func` not available")
}

func (suite *DecoderSuite) TestUnsafePointer() {
	var test = reflect.ValueOf(gommon.Ptr(gommon.Zero[string]())).UnsafePointer()
	suite.EqualError(env.NewDecoder(reader("")).Decode(&test), "type `unsafe.Pointer` not available")
}

func (suite *DecoderSuite) TestNotAddressable() {
	var test int
	suite.EqualError(env.NewDecoder(reader("")).Decode(test), "int should be addressable")
}

func (suite *DecoderSuite) TestUnmarshaler() {
	type Test struct {
		Directly validUnmarshaler  `env:"TEST_DIRECTLY"`
		Pointer  *validUnmarshaler `env:"TEST_POINTER"`
	}

	var valid Test
	err := env.NewDecoder(strings.NewReader("TEST_DIRECTLY=directly\nTEST_POINTER=pointer")).Decode(&valid)
	suite.NoError(err)
	suite.Equal(validUnmarshaler("directly"), valid.Directly)
	suite.Equal(validUnmarshaler("pointer"), *valid.Pointer)

	var invalid errorUnmarshaler
	suite.EqualError(env.NewDecoder(strings.NewReader("")).Decode(&invalid), "error")
}

func (suite *DecoderSuite) TestDefault() {
	type Test struct {
		Value string `env:"TEST_VALUE" default:"defaultValue"`
	}

	var test Test
	err := env.NewDecoder(strings.NewReader("")).Decode(&test)
	suite.NoError(err)
	suite.Equal("defaultValue", test.Value)
}

func (suite *DecoderSuite) TestInline() {
	type Test struct {
		Value  string `env:"TEST_VALUE"`
		Inline struct {
			Value string `env:"TEST_INLINE_VALUE"`
		} `env:",inline"`
	}

	var test Test
	suite.NoError(env.NewDecoder(reader("TEST_VALUE=value1\nTEST_INLINE_VALUE=value2")).Decode(&test))
	suite.Equal("value1", test.Value)
	suite.Equal("value2", test.Inline.Value)
}

func (suite *DecoderSuite) TestBool() {
	var test bool
	suite.NoError(env.NewDecoder(reader("TEST_VALID=true")).Prefix("TEST_VALID").Decode(&test))
	suite.Equal(true, test)

	var testNotExist bool
	suite.NoError(env.NewDecoder(reader("")).Prefix("TEST_NOT_EXIST").Decode(&testNotExist))
	suite.Equal(false, testNotExist)

	var testError bool
	suite.Error(env.NewDecoder(reader("TEST_ERROR=error")).Prefix("TEST_ERROR").Decode(&testError))
	suite.Equal(false, testError)
}

func (suite *DecoderSuite) TestInt() {
	var test int
	suite.NoError(env.NewDecoder(reader("TEST_VALID=123")).Prefix("TEST_VALID").Decode(&test))
	suite.Equal(int(123), test)

	var testNotExist int
	suite.NoError(env.NewDecoder(reader("")).Prefix("TEST_NOT_EXIST").Decode(&testNotExist))
	suite.Equal(int(0), testNotExist)

	var testError int
	suite.Error(env.NewDecoder(reader("TEST_ERROR=error")).Prefix("TEST_ERROR").Decode(&testError))
	suite.Equal(int(0), testError)
}

func (suite *DecoderSuite) TestInt8() {
	var test int8
	suite.NoError(env.NewDecoder(reader("TEST_VALID=123")).Prefix("TEST_VALID").Decode(&test))
	suite.Equal(int8(123), test)

	var testNotExist int8
	suite.NoError(env.NewDecoder(reader("")).Prefix("TEST_NOT_EXIST").Decode(&testNotExist))
	suite.Equal(int8(0), testNotExist)

	var testError int8
	suite.Error(env.NewDecoder(reader("TEST_ERROR=error")).Prefix("TEST_ERROR").Decode(&testError))
	suite.Equal(int8(0), testError)
}

func (suite *DecoderSuite) TestInt16() {
	var test int16
	suite.NoError(env.NewDecoder(reader("TEST_VALID=123")).Prefix("TEST_VALID").Decode(&test))
	suite.Equal(int16(123), test)

	var testNotExist int16
	suite.NoError(env.NewDecoder(reader("")).Prefix("TEST_NOT_EXIST").Decode(&testNotExist))
	suite.Equal(int16(0), testNotExist)

	var testError int16
	suite.Error(env.NewDecoder(reader("TEST_ERROR=error")).Prefix("TEST_ERROR").Decode(&testError))
	suite.Equal(int16(0), testError)
}

func (suite *DecoderSuite) TestInt32() {
	var test int32
	suite.NoError(env.NewDecoder(reader("TEST_VALID=123")).Prefix("TEST_VALID").Decode(&test))
	suite.Equal(int32(123), test)

	var testNotExist int32
	suite.NoError(env.NewDecoder(reader("")).Prefix("TEST_NOT_EXIST").Decode(&testNotExist))
	suite.Equal(int32(0), testNotExist)

	var testError int32
	suite.Error(env.NewDecoder(reader("TEST_ERROR=error")).Prefix("TEST_ERROR").Decode(&testError))
	suite.Equal(int32(0), testError)
}

func (suite *DecoderSuite) TestInt64() {
	var test int64
	suite.NoError(env.NewDecoder(reader("TEST_VALID=123")).Prefix("TEST_VALID").Decode(&test))
	suite.Equal(int64(123), test)

	var testNotExist int64
	suite.NoError(env.NewDecoder(reader("")).Prefix("TEST_NOT_EXIST").Decode(&testNotExist))
	suite.Equal(int64(0), testNotExist)

	var testError int64
	suite.Error(env.NewDecoder(reader("TEST_ERROR=error")).Prefix("TEST_ERROR").Decode(&testError))
	suite.Equal(int64(0), testError)
}

func (suite *DecoderSuite) TestUint() {
	var test uint
	suite.NoError(env.NewDecoder(reader("TEST_VALID=123")).Prefix("TEST_VALID").Decode(&test))
	suite.Equal(uint(123), test)

	var testNotExist uint
	suite.NoError(env.NewDecoder(reader("")).Prefix("TEST_NOT_EXIST").Decode(&testNotExist))
	suite.Equal(uint(0), testNotExist)

	var testError uint
	suite.Error(env.NewDecoder(reader("TEST_ERROR=error")).Prefix("TEST_ERROR").Decode(&testError))
	suite.Equal(uint(0), testError)
}

func (suite *DecoderSuite) TestUint8() {
	var test uint8
	suite.NoError(env.NewDecoder(reader("TEST_VALID=123")).Prefix("TEST_VALID").Decode(&test))
	suite.Equal(uint8(123), test)

	var testNotExist uint8
	suite.NoError(env.NewDecoder(reader("")).Prefix("TEST_NOT_EXIST").Decode(&testNotExist))
	suite.Equal(uint8(0), testNotExist)

	var testError uint8
	suite.Error(env.NewDecoder(reader("TEST_ERROR=error")).Prefix("TEST_ERROR").Decode(&testError))
	suite.Equal(uint8(0), testError)
}

func (suite *DecoderSuite) TestUint16() {
	var test uint16
	suite.NoError(env.NewDecoder(reader("TEST_VALID=123")).Prefix("TEST_VALID").Decode(&test))
	suite.Equal(uint16(123), test)

	var testNotExist uint16
	suite.NoError(env.NewDecoder(reader("")).Prefix("TEST_NOT_EXIST").Decode(&testNotExist))
	suite.Equal(uint16(0), testNotExist)

	var testError uint16
	suite.Error(env.NewDecoder(reader("TEST_ERROR=error")).Prefix("TEST_ERROR").Decode(&testError))
	suite.Equal(uint16(0), testError)
}

func (suite *DecoderSuite) TestUint32() {
	var test uint32
	suite.NoError(env.NewDecoder(reader("TEST_VALID=123")).Prefix("TEST_VALID").Decode(&test))
	suite.Equal(uint32(123), test)

	var testNotExist uint32
	suite.NoError(env.NewDecoder(reader("")).Prefix("TEST_NOT_EXIST").Decode(&testNotExist))
	suite.Equal(uint32(0), testNotExist)

	var testError uint32
	suite.Error(env.NewDecoder(reader("TEST_ERROR=error")).Prefix("TEST_ERROR").Decode(&testError))
	suite.Equal(uint32(0), testError)
}

func (suite *DecoderSuite) TestUint64() {
	var test uint64
	suite.NoError(env.NewDecoder(reader("TEST_VALID=123")).Prefix("TEST_VALID").Decode(&test))
	suite.Equal(uint64(123), test)

	var testNotExist uint64
	suite.NoError(env.NewDecoder(reader("")).Prefix("TEST_NOT_EXIST").Decode(&testNotExist))
	suite.Equal(uint64(0), testNotExist)

	var testError uint64
	suite.Error(env.NewDecoder(reader("TEST_ERROR=error")).Prefix("TEST_ERROR").Decode(&testError))
	suite.Equal(uint64(0), testError)
}

func (suite *DecoderSuite) TestUintPtr() {
	var test uintptr
	suite.NoError(env.NewDecoder(reader("TEST_VALID=123")).Prefix("TEST_VALID").Decode(&test))
	suite.Equal(uintptr(0x123), test)

	var testNotExist uintptr
	suite.NoError(env.NewDecoder(reader("")).Prefix("TEST_NOT_EXIST").Decode(&testNotExist))
	suite.Equal(uintptr(0), testNotExist)

	var testError uintptr
	suite.Error(env.NewDecoder(reader("TEST_ERROR=error")).Prefix("TEST_ERROR").Decode(&testError))
	suite.Equal(uintptr(0), testError)
}

func (suite *DecoderSuite) TestFloat32() {
	var test float32
	suite.NoError(env.NewDecoder(reader("TEST_VALID=1.2")).Prefix("TEST_VALID").Decode(&test))
	suite.Equal(float32(1.2), test)

	var testNotExist float32
	suite.NoError(env.NewDecoder(reader("")).Prefix("TEST_NOT_EXIST").Decode(&testNotExist))
	suite.Equal(float32(0), testNotExist)

	var testError float32
	suite.Error(env.NewDecoder(reader("TEST_ERROR=error")).Prefix("TEST_ERROR").Decode(&testError))
	suite.Equal(float32(0), testError)
}

func (suite *DecoderSuite) TestFloat64() {
	var test float64
	suite.NoError(env.NewDecoder(reader("TEST_VALID=1.2")).Prefix("TEST_VALID").Decode(&test))
	suite.Equal(float64(1.2), test)

	var testNotExist float64
	suite.NoError(env.NewDecoder(reader("")).Prefix("TEST_NOT_EXIST").Decode(&testNotExist))
	suite.Equal(float64(0), testNotExist)

	var testError float64
	suite.Error(env.NewDecoder(reader("TEST_ERROR=error")).Prefix("TEST_ERROR").Decode(&testError))
	suite.Equal(float64(0), testError)
}

func (suite *DecoderSuite) TestComplex64() {
	var test complex64
	suite.NoError(env.NewDecoder(reader("TEST_VALID=1.2")).Prefix("TEST_VALID").Decode(&test))
	suite.Equal(complex64(1.2), test)

	var testNotExist complex64
	suite.NoError(env.NewDecoder(reader("")).Prefix("TEST_NOT_EXIST").Decode(&testNotExist))
	suite.Equal(complex64(0), testNotExist)

	var testError complex64
	suite.Error(env.NewDecoder(reader("TEST_ERROR=error")).Prefix("TEST_ERROR").Decode(&testError))
	suite.Equal(complex64(0), testError)
}

func (suite *DecoderSuite) TestComplex128() {
	var test complex128
	suite.NoError(env.NewDecoder(reader("TEST_VALID=1.2")).Prefix("TEST_VALID").Decode(&test))
	suite.Equal(complex128(1.2), test)

	var testNotExist complex128
	suite.NoError(env.NewDecoder(reader("")).Prefix("TEST_NOT_EXIST").Decode(&testNotExist))
	suite.Equal(complex128(0), testNotExist)

	var testError complex128
	suite.Error(env.NewDecoder(reader("TEST_ERROR=error")).Prefix("TEST_ERROR").Decode(&testError))
	suite.Equal(complex128(0), testError)
}

func (suite *DecoderSuite) TestString() {
	var test string
	suite.NoError(env.NewDecoder(reader("TEST_VALID=value")).Prefix("TEST_VALID").Decode(&test))
	suite.Equal("value", test)

	var testNotExist string
	suite.NoError(env.NewDecoder(reader("")).Prefix("TEST_NOT_EXIST").Decode(&testNotExist))
	suite.Equal("", testNotExist)
}

func (suite *DecoderSuite) TestArray() {
	var test [3]string
	suite.NoError(env.NewDecoder(reader("TEST_0=v1\nTEST_1=v2\nTEST_2=v3")).Prefix("TEST").Decode(&test))
	suite.Equal([3]string{"v1", "v2", "v3"}, test)

	var testNotExist [3]string
	suite.NoError(env.NewDecoder(reader("")).Prefix("TEST_NOT_EXIST").Decode(&testNotExist))
	suite.Equal([3]string{}, testNotExist)

	var testError [3]int
	suite.Error(env.NewDecoder(reader("TEST_ERROR_0=error")).Prefix("TEST_ERROR").Decode(&testError))
	suite.Equal([3]int{}, testError)

	var testKeyError []int
	suite.Error(env.NewDecoder(reader("TEST_ERROR_K0=error")).Prefix("TEST_ERROR").Decode(&testKeyError))
	suite.Equal([3]int{}, testError)
}

func (suite *DecoderSuite) TestSlice() {
	var test []string
	suite.NoError(env.NewDecoder(reader("TEST_0=v1\nTEST_1=v2\nTEST_2=v3")).Prefix("TEST").Decode(&test))
	suite.Equal([]string{"v1", "v2", "v3"}, test)

	var testNotExist []string
	suite.NoError(env.NewDecoder(reader("")).Prefix("TEST_NOT_EXIST").Decode(&testNotExist))
	suite.Equal([]string(nil), testNotExist)

	var testError []int
	suite.Error(env.NewDecoder(reader("TEST_ERROR_0=error")).Prefix("TEST_ERROR").Decode(&testError))
	suite.Equal([]int{}, testError)

	var testKeyError []int
	suite.Error(env.NewDecoder(reader("TEST_ERROR_K0=error")).Prefix("TEST_ERROR").Decode(&testKeyError))
	suite.Equal([]int{}, testError)
}

func (suite *DecoderSuite) TestMap() {
	var test map[string]string
	suite.NoError(env.NewDecoder(reader("TEST_K1=v1\nTEST_K2=v2\nTEST_K3=v3")).Prefix("TEST").Decode(&test))
	suite.Equal(map[string]string{"k1": "v1", "k2": "v2", "k3": "v3"}, test)

	var testNotExist map[string]string
	suite.NoError(env.NewDecoder(reader("")).Prefix("TEST_NOT_EXIST").Decode(&testNotExist))
	suite.Equal(map[string]string{}, testNotExist)

	var testError map[string]int
	suite.Error(env.NewDecoder(reader("TEST_ERROR_K0=error")).Prefix("TEST_ERROR").Decode(&testError))
	suite.Equal(map[string]int{}, testError)
}

func (suite *DecoderSuite) TestStruct() {
	type Test struct {
		Value int
		Slice []string `default:"v1,v2"`
	}

	var test Test
	suite.NoError(env.NewDecoder(reader("TEST_VALUE=1")).Prefix("TEST").Decode(&test))
	suite.Equal(Test{Value: 1, Slice: []string{"v1", "v2"}}, test)

	var testNotExist Test
	suite.NoError(env.NewDecoder(reader("")).Prefix("TEST_NOT_EXIST").Decode(&testNotExist))
	suite.Equal(Test{Value: 0, Slice: []string{"v1", "v2"}}, testNotExist)

	var testError Test
	suite.Error(env.NewDecoder(reader("TEST_ERROR_VALUE=error")).Prefix("TEST_ERROR").Decode(&testError))
	suite.Equal(Test{Value: 0}, testError)

	type TestError struct {
		Value int
		Slice []int `default:"v1"`
	}

	var testDefaultError TestError
	suite.Error(env.NewDecoder(reader("")).Prefix("TEST_ERROR").Decode(&testDefaultError))
	suite.Equal(TestError{Value: 0, Slice: []int{}}, testDefaultError)
}

type validUnmarshaler string

func (u *validUnmarshaler) UnmarshalENV(data []byte) error {
	*u = validUnmarshaler(data)

	return nil
}

type errorUnmarshaler string

func (u *errorUnmarshaler) UnmarshalENV([]byte) error {
	return errors.New("error")
}
