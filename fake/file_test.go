package fake_test

import (
	"errors"
	"fmt"
	"io/fs"
	"io/ioutil"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"gitlab.com/evolves-fr/gommon/fake"
)

var errUnknown = errors.New("unknown")

// Tests

func TestFile(t *testing.T) {
	t.Parallel()

	file := &fake.File{
		FileName:   "hello",
		Contents:   "hello world",
		FileMode:   0o644,
		Offset:     0,
		StatError:  nil,
		ReadError:  nil,
		CloseError: nil,
	}

	data, err := ioutil.ReadAll(file)
	assert.NoError(t, err)
	assert.Equal(t, []byte("hello world"), data)

	stat, err := file.Stat()
	assert.NoError(t, err)
	assert.Equal(t, "hello", stat.Name())
	assert.Equal(t, int64(11), stat.Size())
	assert.Equal(t, fs.FileMode(0o644), stat.Mode())
	assert.Equal(t, time.Time{}, stat.ModTime())
	assert.Equal(t, false, stat.IsDir())
	assert.Nil(t, stat.Sys())
}

func TestFile_errorStat(t *testing.T) {
	t.Parallel()

	file := &fake.File{
		FileName:   "error",
		Contents:   "error",
		FileMode:   0o644,
		Offset:     0,
		StatError:  errUnknown,
		ReadError:  nil,
		CloseError: nil,
	}

	stat, err := file.Stat()
	assert.Error(t, err)
	assert.Nil(t, stat)
}

func TestFile_errorRead(t *testing.T) {
	t.Parallel()

	file := &fake.File{
		FileName:   "error",
		Contents:   "error",
		FileMode:   0o644,
		Offset:     0,
		StatError:  nil,
		ReadError:  errUnknown,
		CloseError: nil,
	}

	data, err := ioutil.ReadAll(file)
	assert.Error(t, err)
	assert.Equal(t, []byte{}, data)
}

func TestFile_errorClose(t *testing.T) {
	t.Parallel()

	file := &fake.File{
		FileName:   "error",
		Contents:   "error",
		FileMode:   0o644,
		Offset:     0,
		StatError:  nil,
		ReadError:  nil,
		CloseError: errUnknown,
	}

	assert.Error(t, file.Close())
}

// Examples

func ExampleFile() {
	file := &fake.File{
		FileName:   "hello",
		Contents:   "hello world",
		FileMode:   0o644,
		Offset:     0,
		StatError:  nil,
		ReadError:  nil,
		CloseError: nil,
	}

	stat, err := file.Stat()
	if err != nil {
		panic(err)
	}

	data, err := ioutil.ReadAll(file)
	if err != nil {
		panic(err)
	}

	if err = file.Close(); err != nil {
		panic(err)
	}

	fmt.Printf("Name    = %v\n", stat.Name())
	fmt.Printf("Size    = %v\n", stat.Size())
	fmt.Printf("Mode    = %v\n", stat.Mode().String())
	fmt.Printf("ModTime = %v\n", stat.ModTime().String())
	fmt.Printf("IsDir   = %v\n", stat.IsDir())
	fmt.Printf("Sys     = %v\n", stat.Sys())
	fmt.Printf("Content = %v\n", string(data))

	// Output:
	// Name    = hello
	// Size    = 11
	// Mode    = -rw-r--r--
	// ModTime = 0001-01-01 00:00:00 +0000 UTC
	// IsDir   = false
	// Sys     = <nil>
	// Content = hello world
}
