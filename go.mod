module gitlab.com/evolves-fr/gommon

go 1.20

require (
	github.com/Masterminds/semver/v3 v3.2.1
	github.com/go-playground/validator/v10 v10.14.1
	github.com/stretchr/testify v1.8.4
	gopkg.in/yaml.v3 v3.0.1
)

require (
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/gabriel-vasile/mimetype v1.4.2 // indirect
	github.com/go-playground/locales v0.14.1 // indirect
	github.com/go-playground/universal-translator v0.18.1 // indirect
	github.com/leodido/go-urn v1.2.4 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	golang.org/x/crypto v0.10.0 // indirect
	golang.org/x/net v0.11.0 // indirect
	golang.org/x/sys v0.9.0 // indirect
	golang.org/x/text v0.10.0 // indirect
)

// See CVE-2022-29526 and CVE-2022-27191.
retract v0.16.0

// Incorrect timestamp timezone in tests.
retract v0.17.0

// Security CVE.
retract v0.17.1

// FIX StringSlice UnmarshalJSON.
retract v0.18.0
