package env_test

import (
	"bytes"
	"errors"
	"testing"

	"github.com/stretchr/testify/suite"
	"gitlab.com/evolves-fr/gommon/env"
	"gitlab.com/evolves-fr/gommon/tests"
)

func TestEncoder(t *testing.T) {
	t.Parallel()

	suite.Run(t, new(EncoderSuite))
}

type EncoderSuite struct {
	suite.Suite
}

func (suite *EncoderSuite) TestWriterError() {
	suite.EqualError(env.NewEncoder(tests.ErrReadWriter(errors.New("writer error"))).Encode(map[string]string{}), "writer error")
}

func (suite *EncoderSuite) TestNilType() {
	suite.EqualError(env.NewEncoder(new(bytes.Buffer)).Encode(nil), "values is nil")
}

func (suite *EncoderSuite) TestEncode() {
	var buffer bytes.Buffer
	envMap := map[string]string{"VAR_STRING": "string", "KEY1": "value1", "KEY2": "value2"}
	envRaw := []byte("KEY1=value1\nKEY2=value2\nVAR_STRING=string\n")
	suite.NoError(env.NewEncoder(&buffer).Encode(envMap))
	suite.Equal(envRaw, buffer.Bytes())
}
