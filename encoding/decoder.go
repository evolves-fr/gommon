package encoding

import (
	"errors"
	"io"
)

func NewDecoder(r io.Reader) *Decoder {
	return &Decoder{r: r}
}

type Decoder struct {
	r io.Reader
}

func (dec *Decoder) Decode(v any) error {
	if dec.r == nil {
		return errors.New("reader is nil")
	}

	data, err := io.ReadAll(dec.r)
	if err != nil {
		return err
	}

	return Unmarshal(data, v)
}
