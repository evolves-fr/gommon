package gommon

import "reflect"

type Value[T comparable] struct {
	Value T
	Valid bool
}

func zeroToV[T comparable]() Value[T] {
	return Value[T]{Value: reflect.Zero(reflect.TypeOf(new(T)).Elem()).Interface().(T), Valid: false}
}

func toV[T comparable](value T) Value[T] {
	return Value[T]{Value: value, Valid: true}
}

func functionToV[T comparable](value T, err error) Value[T] {
	if err != nil {
		return zeroToV[T]()
	}

	return toV[T](value)
}

func convertToV[T comparable](v any) Value[T] {
	vo := reflect.ValueOf(v)
	to := reflect.TypeOf(new(T)).Elem()

	if !vo.CanConvert(to) {
		return zeroToV[T]()
	}

	return toV[T](vo.Convert(to).Interface().(T))
}

func convertFunctionToV[T comparable](value any, err error) Value[T] {
	if err != nil {
		return zeroToV[T]()
	}

	return convertToV[T](value)
}

func numberToBooleanV[T Number](v T) Value[bool] {
	switch v {
	case 0:
		return toV[bool](false)
	case 1:
		return toV[bool](true)
	default:
		return zeroToV[bool]()
	}
}
