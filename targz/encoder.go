package targz

import (
	"archive/tar"
	"compress/gzip"
	"io"
	"io/fs"
	"sync"

	"gitlab.com/evolves-fr/gommon/errors"
)

// Encoder is the interface implemented for write tar.gz archive.
type Encoder interface {
	Add(path string, file fs.File) error
	Close() error
}

// NewEncoder returns a new encoder that writes to writer.
func NewEncoder(writer io.Writer) (Encoder, error) {
	if writer == nil {
		return nil, errors.New("writer is nil")
	}

	mutex := new(sync.Mutex)
	gzipWriter := gzip.NewWriter(writer)
	tarWriter := tar.NewWriter(gzipWriter)

	return &encoder{mutex: mutex, gzipWriter: gzipWriter, tarWriter: tarWriter}, nil
}

// An encoder write fs.Files to tar.gz archive.
type encoder struct {
	mutex      *sync.Mutex
	gzipWriter *gzip.Writer
	tarWriter  *tar.Writer
}

// Add fs.File to path in tar.gz archive.
func (w *encoder) Add(path string, file fs.File) error {
	w.mutex.Lock()
	defer w.mutex.Unlock()

	info, err := file.Stat()
	if err != nil {
		return err
	}

	header, err := tar.FileInfoHeader(info, info.Name())
	if err != nil {
		return err
	}

	if len(path) > 0 {
		header.Name = path
	}

	if err = w.tarWriter.WriteHeader(header); err != nil {
		return err
	}

	if _, err = io.Copy(w.tarWriter, file); err != nil {
		return err
	}

	return nil
}

// Close tar.gz archive writer.
func (w *encoder) Close() error {
	w.mutex.Lock()
	defer w.mutex.Unlock()

	errs := make([]error, 0)

	if err := w.tarWriter.Close(); err != nil {
		errs = append(errs, err)
	}

	if err := w.gzipWriter.Close(); err != nil {
		errs = append(errs, err)
	}

	if len(errs) > 0 {
		return errors.Join(errs...)
	}

	return nil
}
