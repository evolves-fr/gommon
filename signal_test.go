package gommon_test

import (
	"os"
	"syscall"
	"testing"

	"github.com/stretchr/testify/suite"
	"gitlab.com/evolves-fr/gommon"
)

func TestSignal(t *testing.T) {
	suite.Run(t, new(SignalSuite))
}

type SignalSuite struct {
	suite.Suite
}

func (suite *SignalSuite) signal(sig os.Signal) (os.Signal, error) {
	sigs := gommon.Signal(sig)

	proc, err := os.FindProcess(os.Getpid())
	if err != nil {
		return nil, err
	}

	if err = proc.Signal(sig); err != nil {
		return nil, err
	}

	return <-sigs, nil
}

func (suite *SignalSuite) TestSIGINT() {
	res, err := suite.signal(syscall.SIGINT)
	suite.NoError(err)
	suite.Equal(syscall.SIGINT, res)
}

func (suite *SignalSuite) TestSIGQUIT() {
	res, err := suite.signal(syscall.SIGQUIT)
	suite.NoError(err)
	suite.Equal(syscall.SIGQUIT, res)
}

func (suite *SignalSuite) TestSIGTERM() {
	res, err := suite.signal(syscall.SIGTERM)
	suite.NoError(err)
	suite.Equal(syscall.SIGTERM, res)
}

func (suite *SignalSuite) TestSIGTSTP() {
	res, err := suite.signal(syscall.SIGTSTP)
	suite.NoError(err)
	suite.Equal(syscall.SIGTSTP, res)
}
