package store

var std = New()

func StandardStore() Store {
	return std
}

func Has(key any) bool {
	return std.Has(key)
}

func Get(key any) any {
	return std.Get(key)
}

func Set(key, value any) {
	std.Set(key, value)
}

func Remove(key any) {
	std.Remove(key)
}
