package utils

import (
	"strings"

	"gitlab.com/evolves-fr/gommon/validator"
)

func DomainLevel(domain string, level int) string {
	if err := validator.New().Var(domain, "domain"); err != nil {
		return ""
	}

	domain = strings.TrimSuffix(domain, ".")

	if err := validator.New().Var(domain, "ip"); err == nil {
		return ""
	}

	if level < 1 {
		return ""
	}

	values := strings.Split(domain, ".")

	if len(values) < level {
		return ""
	}

	return strings.Join(values[len(values)-level:], ".")
}

func TLD(domain string) string {
	return DomainLevel(domain, 1)
}
