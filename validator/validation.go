package validator

import (
	"reflect"
	"time"

	v10 "github.com/go-playground/validator/v10"
)

func isDuration(fl v10.FieldLevel) bool {
	if fl.Field().Kind() == reflect.String {
		_, err := time.ParseDuration(fl.Field().String())

		return err == nil
	}

	return false
}

func isDomain(fl v10.FieldLevel) bool {
	return domainRegex.MatchString(fl.Field().String())
}
