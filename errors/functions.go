package errors

import "strings"

func Join(errs ...error) error {
	return JoinWithSeparator("\n", errs...)
}

func JoinWithSeparator(separator string, errs ...error) error {
	values := make([]string, 0)

	for _, err := range errs {
		if err != nil {
			values = append(values, err.Error())
		}
	}

	return New(strings.Join(values, separator))
}
