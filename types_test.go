package gommon_test

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/url"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/suite"
	"gitlab.com/evolves-fr/gommon"
	"gitlab.com/evolves-fr/gommon/env"
	"gopkg.in/yaml.v3"
)

func TestParseBase64(t *testing.T) {
	t.Parallel()

	b64, err := gommon.ParseBase64("====")
	assert.Error(t, err)
	assert.Equal(t, gommon.Base64(nil), b64)

	b64, err = gommon.ParseBase64("dmFsdWU=")
	assert.NoError(t, err)
	assert.Equal(t, gommon.Base64("value"), b64)
}

func TestBase64(t *testing.T) {
	suite.Run(t, new(Base64Suite))
}

type Base64Suite struct {
	suite.Suite
}

func (suite *Base64Suite) TestString() {
	suite.Equal("value", gommon.Base64("value").String())
}

func (suite *Base64Suite) TestEncode() {
	suite.Equal("dmFsdWU=", gommon.Base64("value").Encode())
}

func (suite *Base64Suite) TestMarshalText() {
	data, err := gommon.Base64("value").MarshalText()
	suite.NoError(err)
	suite.Equal([]byte("dmFsdWU="), data)
}

func (suite *Base64Suite) TestUnmarshalText() {
	var b64 gommon.Base64

	// Invalid
	suite.Error(b64.UnmarshalText([]byte("====")))
	suite.Equal(gommon.Base64(nil), b64)

	// Valid
	suite.NoError(b64.UnmarshalText([]byte("dmFsdWU=")))
	suite.Equal(gommon.Base64("value"), b64)
}

func (suite *Base64Suite) TestMarshalJSON() {
	data, err := json.Marshal(gommon.Base64("value"))
	suite.NoError(err)
	suite.Equal([]byte("\"dmFsdWU=\""), data)
}

func (suite *Base64Suite) TestUnmarshalJSON() {
	var b64 gommon.Base64

	// Invalid
	suite.Error(json.Unmarshal([]byte("\"====\""), &b64))
	suite.Equal(gommon.Base64(nil), b64)

	//  Valid
	suite.NoError(json.Unmarshal([]byte("\"dmFsdWU=\""), &b64))
	suite.Equal(gommon.Base64("value"), b64)
}

func (suite *Base64Suite) TestMarshalYAML() {
	data, err := yaml.Marshal(gommon.Base64("value"))
	suite.NoError(err)
	suite.Equal([]byte("dmFsdWU=\n"), data)
}

func (suite *Base64Suite) TestUnmarshalYAML() {
	var b64 gommon.Base64

	// Invalid
	suite.Error(yaml.Unmarshal([]byte("====\n"), &b64))
	suite.Equal(gommon.Base64(nil), b64)

	//  Valid
	suite.NoError(yaml.Unmarshal([]byte("dmFsdWU=\n"), &b64))
	suite.Equal(gommon.Base64("value"), b64)
}

func (suite *Base64Suite) TestUnmarshalENV() {
	var b64 gommon.Base64

	// Invalid
	suite.T().Setenv("TEST_BASE64", "====")
	suite.Error(env.UnmarshalWithPrefix(&b64, "TEST_BASE64"))
	suite.Equal(gommon.Base64(nil), b64)

	//  Valid
	suite.T().Setenv("TEST_BASE64", "dmFsdWU=")
	suite.NoError(env.UnmarshalWithPrefix(&b64, "TEST_BASE64"))
	suite.Equal(gommon.Base64("value"), b64)
}

func TestParseDSN(t *testing.T) {
	dsn, err := gommon.ParseDSN("redis://endpoint.local")
	assert.NoError(t, err)
	assert.Equal(t, gommon.DSN{Scheme: "redis", Endpoint: "endpoint.local", Queries: url.Values{}}, dsn)

	dsn, err = gommon.ParseDSN(":/unknown")
	assert.Error(t, err)
	assert.Equal(t, gommon.DSN{}, dsn)
}

func TestDSN(t *testing.T) {
	suite.Run(t, new(DSNSuite))
}

type DSNSuite struct {
	suite.Suite
	tests map[string]gommon.DSN
}

func (suite *DSNSuite) SetupSuite() {
	suite.tests = make(map[string]gommon.DSN)

	for bits := 1 << 0; bits < 1<<8; bits++ {
		var dsn gommon.DSN

		dsn.Scheme = gommon.Ternary(bits&(1<<0) != 0, "scheme", "")
		dsn.Username = gommon.Ternary(bits&(1<<1) != 0, "username", "")
		dsn.Password = gommon.Ternary(bits&(1<<2) != 0, "password", "")
		dsn.Protocol = gommon.Ternary(bits&(1<<3) != 0, "udp6", "")
		dsn.Endpoint = gommon.Ternary(bits&(1<<4) != 0, "endpoint.example.tld", "")
		dsn.Port = gommon.Ternary(bits&(1<<5) != 0, 9876, 0)
		dsn.Path = gommon.Ternary(bits&(1<<6) != 0, "myPath", "")
		dsn.Queries = gommon.Ternary(bits&(1<<7) != 0, url.Values{
			"key":   {"value"},
			"slice": {"value1", "value2"},
		}, url.Values{})

		suite.tests[dsn.String()] = dsn
	}
}

func (suite *DSNSuite) TestString() {
	for value, dsn := range suite.tests {
		suite.Equal(value, dsn.String(), value)
	}
}

func (suite *DSNSuite) TestRedacted() {
	suite.Equal("mysql://username:xxxxx@tcp(127.0.0.1:3306)/mysql", gommon.DSN{
		Scheme:   "mysql",
		Username: "username",
		Password: "password",
		Protocol: "tcp",
		Endpoint: "127.0.0.1",
		Port:     3306,
		Path:     "mysql",
		Queries:  nil,
	}.Redacted())
}

func (suite *DSNSuite) TestMarshalText() {
	for value, dsn := range suite.tests {
		res, err := dsn.MarshalText()
		suite.NoError(err)
		suite.Equal([]byte(value), res)
	}
}

func (suite *DSNSuite) TestUnmarshalText() {
	// Valid
	for value, result := range suite.tests {
		var dsn gommon.DSN

		suite.NoError(dsn.UnmarshalText([]byte(value)))
		suite.Equal(result, dsn, value)
	}

	// Invalid
	suite.EqualError(new(gommon.DSN).UnmarshalText([]byte("invalid:/unknown")), "not valid DSN")
}

func (suite *DSNSuite) TestMarshalJSON() {
	for value, dsn := range suite.tests {
		var buffer bytes.Buffer

		enc := json.NewEncoder(&buffer)
		enc.SetEscapeHTML(false)
		suite.NoError(enc.Encode(dsn))
		suite.Equal("\""+value+"\"\n", buffer.String())
	}
}

func (suite *DSNSuite) TestUnmarshalJSON() {
	// Valid
	for value, result := range suite.tests {
		var dsn gommon.DSN

		suite.NoError(json.Unmarshal([]byte("\""+value+"\"\n"), &dsn))
		suite.Equal(result, dsn, value)
	}

	// Invalid
	suite.EqualError(json.Unmarshal([]byte("\"invalid:unknown\""), new(gommon.DSN)), "not valid DSN")
}

func (suite *DSNSuite) TestMarshalYAML() {
	for value, dsn := range suite.tests {
		var buffer bytes.Buffer

		enc := yaml.NewEncoder(&buffer)
		suite.NoError(enc.Encode(dsn))
		suite.Equal(value+"\n", buffer.String())
	}
}

func (suite *DSNSuite) TestUnmarshalYAML() {
	// Valid
	for value, result := range suite.tests {
		var dsn gommon.DSN

		suite.NoError(yaml.Unmarshal([]byte(value+"\n"), &dsn))
		suite.Equal(result, dsn, value)
	}

	// Invalid
	suite.EqualError(yaml.Unmarshal([]byte("invalid:/unknown\n"), new(gommon.DSN)), "not valid DSN")
}

func (suite *DSNSuite) TestUnmarshalENV() {
	// Valid
	for value, result := range suite.tests {
		var dsn gommon.DSN

		suite.T().Setenv("TEST_DSN", value)

		suite.NoError(env.UnmarshalWithPrefix(&dsn, "TEST_DSN"))
		suite.Equal(result, dsn, value)
	}

	// Invalid
	var dsn gommon.DSN

	suite.T().Setenv("TEST_DSN", "invalid:unknown")
	suite.EqualError(env.UnmarshalWithPrefix(&dsn, "TEST_DSN"), "not valid DSN")
}

func TestStringSlice(t *testing.T) {
	t.Parallel()

	suite.Run(t, new(StringSliceSuite))
}

type StringSliceSuite struct {
	suite.Suite
}

func (suite *StringSliceSuite) TestMarshalText() {
	res, err := gommon.StringSlice{"value1", "", "value2"}.MarshalText()
	suite.NoError(err)
	suite.Equal("value1,,value2", string(res))
}

func (suite *StringSliceSuite) TestUnmarshalText() {
	var slice gommon.StringSlice

	suite.NoError(slice.UnmarshalText([]byte("value1,,value2")))
	suite.Equal(gommon.StringSlice{"value1", "", "value2"}, slice)
}

func (suite *StringSliceSuite) TestMarshalJSON() {
	res, err := json.Marshal(gommon.StringSlice{"value1", "", "value2"})
	suite.NoError(err)
	suite.Equal(`["value1","","value2"]`, string(res))

	res, err = json.MarshalIndent(gommon.StringSlice{"value1", "", "value2"}, "", "  ")
	suite.NoError(err)
	suite.Equal("[\n  \"value1\",\n  \"\",\n  \"value2\"\n]", string(res))
}

func (suite *StringSliceSuite) TestUnmarshalJSON() {
	var jsonSlice gommon.StringSlice

	suite.NoError(json.Unmarshal([]byte(`["value1","","value2"]`), &jsonSlice))
	suite.Equal(gommon.StringSlice{"value1", "", "value2"}, jsonSlice)

	var stringSlice gommon.StringSlice

	suite.NoError(json.Unmarshal([]byte(`"[\"value1\",\"\",\"value2\"]"`), &stringSlice))
	suite.Equal(gommon.StringSlice{"value1", "", "value2"}, stringSlice)

	var stringSubSlice gommon.StringSlice

	suite.NoError(json.Unmarshal([]byte(`["[\"value1\",\"\"]","[\"value2\"]"]`), &stringSubSlice))
	suite.Equal(gommon.StringSlice{"value1", "", "value2"}, stringSubSlice)

	var stringToSlice gommon.StringSlice

	suite.NoError(json.Unmarshal([]byte(`"value1,value2"`), &stringToSlice))
	suite.Equal(gommon.StringSlice{"value1", "value2"}, stringToSlice)

	var errorSubSlice gommon.StringSlice

	suite.Error(errorSubSlice.UnmarshalJSON([]byte(`[value1,,value2]`)))
	suite.Nil(errorSubSlice)

	var jsonObject gommon.StringSlice

	suite.NoError(jsonObject.UnmarshalJSON([]byte(`{ }`)))
	suite.Nil(jsonObject)
}

func (suite *StringSliceSuite) TestMarshalYAML() {
	res, err := yaml.Marshal(gommon.StringSlice{"value1", "", "value2"})
	suite.NoError(err)
	suite.Equal(`[value1, "", value2]`+"\n", string(res))
}

func (suite *StringSliceSuite) TestUnmarshalYAML() {
	var jsonSlice gommon.StringSlice

	suite.NoError(yaml.Unmarshal([]byte("value1,,value2\n"), &jsonSlice))
	suite.Equal(gommon.StringSlice{"value1", "", "value2"}, jsonSlice)
}

func TestParseTimestamp(t *testing.T) {
	ts, err := gommon.ParseTimestamp("1136214245")
	assert.NoError(t, err)
	assert.Equal(t, time.Date(2006, 1, 2, 15, 4, 5, 0, time.UTC), ts.Time().UTC())
}

func TestTimestamp(t *testing.T) {
	suite.Run(t, new(TimestampSuite))
}

type TimestampSuite struct {
	suite.Suite
}

func (suite *TimestampSuite) TestString() {
	suite.Equal("1136214245", gommon.Timestamp(1136214245).String())
}

func (suite *TimestampSuite) TestMarshalBinary() {
	data, err := gommon.Timestamp(1136214245).MarshalBinary()
	suite.NoError(err)
	suite.Equal([]byte{0x0, 0x0, 0x0, 0x0, 0x43, 0xb9, 0x40, 0xe5}, data)
}

func (suite *TimestampSuite) TestUnmarshalBinary() {
	var timestamp gommon.Timestamp

	// Invalid
	suite.Error(timestamp.UnmarshalBinary([]byte{0x22, 0x65, 0x72, 0x72, 0x6f, 0x72, 0x22}))

	// Valid
	suite.NoError(timestamp.UnmarshalBinary([]byte{0x0, 0x0, 0x0, 0x0, 0x43, 0xb9, 0x40, 0xe5}))
	suite.Equal(time.Date(2006, 1, 2, 15, 4, 5, 0, time.UTC), timestamp.Time().UTC())
}

func (suite *TimestampSuite) TestMarshalText() {
	data, err := gommon.Timestamp(1136214245).MarshalText()
	suite.NoError(err)
	suite.Equal([]byte{0x31, 0x31, 0x33, 0x36, 0x32, 0x31, 0x34, 0x32, 0x34, 0x35}, data)
}

func (suite *TimestampSuite) TestUnmarshalText() {
	var timestamp gommon.Timestamp

	// Invalid
	suite.Error(timestamp.UnmarshalText([]byte{0x22, 0x65, 0x72, 0x72, 0x6f, 0x72, 0x22}))

	// Valid
	suite.NoError(timestamp.UnmarshalText([]byte{0x31, 0x31, 0x33, 0x36, 0x32, 0x31, 0x34, 0x32, 0x34, 0x35}))
	suite.Equal(time.Date(2006, 1, 2, 15, 4, 5, 0, time.UTC), timestamp.Time().UTC())
}

func (suite *TimestampSuite) TestMarshalJSON() {
	data, err := json.Marshal(gommon.Timestamp(1136214245))
	suite.NoError(err)
	suite.Equal([]byte{0x31, 0x31, 0x33, 0x36, 0x32, 0x31, 0x34, 0x32, 0x34, 0x35}, data)
}

func (suite *TimestampSuite) TestUnmarshalJSON() {
	var timestamp gommon.Timestamp

	// Invalid
	suite.Error(json.Unmarshal([]byte{0x22, 0x65, 0x72, 0x72, 0x6f, 0x72, 0x22}, &timestamp))

	// Valid
	suite.NoError(json.Unmarshal([]byte{0x31, 0x31, 0x33, 0x36, 0x32, 0x31, 0x34, 0x32, 0x34, 0x35}, &timestamp))
	suite.Equal(time.Date(2006, 1, 2, 15, 4, 5, 0, time.UTC), timestamp.Time().UTC())
}

func (suite *TimestampSuite) TestMarshalYAML() {
	data, err := yaml.Marshal(gommon.Timestamp(1136214245))
	suite.NoError(err)
	suite.Equal([]byte{0x22, 0x31, 0x31, 0x33, 0x36, 0x32, 0x31, 0x34, 0x32, 0x34, 0x35, 0x22, 0xa}, data)
}

func (suite *TimestampSuite) TestUnmarshalYAML() {
	var timestamp gommon.Timestamp

	// Invalid YAML
	suite.Error(yaml.Unmarshal([]byte{0x22, 0x65, 0x72, 0x72, 0x6f, 0x72, 0x22}, &timestamp))

	// Valid YAML
	suite.NoError(yaml.Unmarshal([]byte{0x31, 0x31, 0x33, 0x36, 0x32, 0x31, 0x34, 0x32, 0x34, 0x35}, &timestamp))
	suite.Equal(time.Date(2006, 1, 2, 15, 4, 5, 0, time.UTC), timestamp.Time().UTC())
}

func (suite *TimestampSuite) TestUnmarshalENV() {
	var timestamp gommon.Timestamp

	suite.T().Setenv("TEST_TS_ERROR", "")
	suite.T().Setenv("TEST_TS", "1136214245")

	// Invalid YAML
	suite.Error(env.UnmarshalWithPrefix(&timestamp, "TEST_TS_ERROR"))
	suite.Equal(time.Date(1970, 1, 1, 0, 0, 0, 0, time.UTC).UTC(), timestamp.Time().UTC())

	// Valid YAML
	suite.NoError(env.UnmarshalWithPrefix(&timestamp, "TEST_TS"))
	suite.Equal(time.Date(2006, 1, 2, 15, 4, 5, 0, time.UTC), timestamp.Time().UTC())
}

func ExampleParseTimestamp() {
	ts, err := gommon.ParseTimestamp("1136214245")
	if err != nil {
		panic(err)
	}

	fmt.Println(ts.Time().UTC().String())
	// Output: 2006-01-02 15:04:05 +0000 UTC
}
