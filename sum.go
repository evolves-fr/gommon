package gommon

import (
	"crypto/md5"
	"crypto/sha1"
	"crypto/sha256"
	"crypto/sha512"
	"fmt"
	"hash"
	"strings"
)

func Sum(hashName string, data []byte) (string, error) {
	var h hash.Hash

	switch strings.ToLower(hashName) {
	case "md5":
		h = md5.New()
	case "sha1":
		h = sha1.New()
	case "sha256":
		h = sha256.New()
	case "sha512":
		h = sha512.New()
	default:
		return "", fmt.Errorf("invalid hash '%s'", hashName)
	}

	return SumFn(h, data), nil
}

func SumFn(h hash.Hash, data []byte) string {
	if h == nil {
		return ""
	}

	h.Write(data)

	return fmt.Sprintf("%x", h.Sum(nil))
}
