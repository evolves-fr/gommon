package gommon

import (
	"io/ioutil"
	"os"
)

func PathExist(path string) bool {
	_, err := os.Stat(path)
	return !os.IsNotExist(err)
}

func PathEmpty(path string) bool {
	if dir, err := ioutil.ReadDir(path); err == nil && len(dir) > 0 {
		return false
	}

	return true
}
