package errors

func New(text string) error {
	return &stringError{Value: text}
}

type stringError struct {
	Value string
}

func (e *stringError) Error() string {
	return e.Value
}
