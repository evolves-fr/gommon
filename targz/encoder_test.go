package targz_test

import (
	"bytes"
	"errors"
	"io/fs"
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/evolves-fr/gommon/fake"
	"gitlab.com/evolves-fr/gommon/targz"
)

// Tests

func TestNewEncoder(t *testing.T) {
	t.Parallel()

	expected := []byte{
		0x1f, 0x8b, 0x08, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xff, 0xec, 0xd4, 0x41, 0x0a, 0xc2, 0x30,
		0x10, 0x05, 0xd0, 0x1c, 0x25, 0x27, 0xd0, 0x3f, 0x6d, 0xb0, 0xe7, 0x51, 0x9a, 0xa2, 0x32, 0x38,
		0x90, 0x4e, 0xd0, 0xe3, 0x78, 0x17, 0x2f, 0x26, 0x75, 0xa1, 0x90, 0x8d, 0x0b, 0x49, 0x83, 0x30,
		0x6f, 0x33, 0x21, 0x9b, 0xfc, 0x2c, 0xfe, 0x4c, 0x27, 0x8e, 0xb4, 0xd1, 0x9b, 0xba, 0x7a, 0x00,
		0x60, 0x17, 0xc2, 0x6b, 0x02, 0x28, 0x27, 0x40, 0xfd, 0xe7, 0xbc, 0xdc, 0x13, 0x86, 0x00, 0xe7,
		0x51, 0x31, 0xd3, 0x5b, 0x9e, 0x75, 0x9f, 0x1c, 0x7e, 0x7e, 0xab, 0xfc, 0xdc, 0x9f, 0x38, 0x46,
		0x66, 0xf1, 0x57, 0x49, 0x3c, 0xb6, 0x8e, 0x62, 0x1a, 0x98, 0x84, 0xc7, 0x98, 0x68, 0xbb, 0xec,
		0x81, 0xae, 0xd2, 0x1e, 0xf8, 0xde, 0xff, 0xa1, 0xe8, 0x7f, 0xd7, 0x83, 0xac, 0xff, 0x6b, 0x38,
		0xc8, 0xe5, 0x2c, 0x39, 0xf9, 0xc7, 0xdd, 0xab, 0xe4, 0xb9, 0x75, 0x1c, 0x63, 0x8c, 0x31, 0x2b,
		0x79, 0x06, 0x00, 0x00, 0xff, 0xff, 0x68, 0x19, 0xc0, 0x9a, 0x00, 0x0c, 0x00, 0x00,
	}

	//nolint:exhaustruct
	hello := &fake.File{FileName: "hello", Contents: "hello world", FileMode: 0o644}
	//nolint:exhaustruct
	bonjour := &fake.File{FileName: "bonjour", Contents: "bonjour à tous", FileMode: 0o644}

	// Valid encoder
	buf := new(bytes.Buffer)
	enc, err := targz.NewEncoder(buf)
	assert.NoError(t, err)
	assert.NoError(t, enc.Add("file1.txt", hello))
	assert.NoError(t, enc.Add("folder1/file2.txt", bonjour))
	assert.NoError(t, enc.Close())
	assert.Equal(t, expected, buf.Bytes())
}

func TestNewEncoder_invalidWriter(t *testing.T) {
	t.Parallel()

	enc, err := targz.NewEncoder(nil)
	assert.Error(t, err)
	assert.Nil(t, enc)
}

func TestNewEncoder_invalidFileStat(t *testing.T) {
	t.Parallel()

	//nolint:exhaustruct,goerr113
	file := &fake.File{FileName: "error", Contents: "error", FileMode: 0o644, StatError: errors.New("unknown")}

	enc, err := targz.NewEncoder(bytes.NewBuffer([]byte{}))
	assert.NoError(t, err)
	assert.Error(t, enc.Add("error.txt", file))
	assert.NoError(t, enc.Close())
}

func TestNewEncoder_invalidFileReader(t *testing.T) {
	t.Parallel()

	//nolint:exhaustruct,goerr113
	file := &fake.File{FileName: "", Contents: "", FileMode: 0o644, ReadError: errors.New("unknown")}

	enc, err := targz.NewEncoder(bytes.NewBuffer([]byte{}))
	assert.NoError(t, err)
	assert.Error(t, enc.Add("error.txt", file))
	assert.NoError(t, enc.Close())
}

func TestNewEncoder_invalidFilePerm(t *testing.T) {
	t.Parallel()

	//nolint:exhaustruct
	file := &fake.File{FileName: "socket", Contents: "socket", FileMode: fs.ModeSocket}

	enc, err := targz.NewEncoder(bytes.NewBuffer([]byte{}))
	assert.NoError(t, err)
	assert.Error(t, enc.Add("socket.txt", file))
	assert.NoError(t, enc.Close())
}

func TestNewEncoder_writerClosed(t *testing.T) {
	t.Parallel()

	//nolint:exhaustruct
	file := &fake.File{FileName: "hello", Contents: "hello world", FileMode: 0o644}

	enc, err := targz.NewEncoder(bytes.NewBuffer([]byte{}))
	assert.NoError(t, err)
	assert.NoError(t, enc.Close())
	assert.Error(t, enc.Add("hello.txt", file))
}

func TestNewEncoder_bufferOverflow(t *testing.T) {
	t.Parallel()

	//nolint:exhaustruct
	file := &fake.File{FileName: "hello", Contents: "hello world", FileMode: 0o644}

	enc, err := targz.NewEncoder(newBufferSized(make([]byte, 0, 1)))
	assert.NoError(t, err)
	assert.Error(t, enc.Add("hello.txt", file))
	assert.Error(t, enc.Close())
}

// Examples

func ExampleNewEncoder() {
	// Open destination buffer writer
	destination := new(bytes.Buffer)

	// Open tar.gz writer
	encoder, err := targz.NewEncoder(destination)
	if err != nil {
		panic(err)
	}
	defer encoder.Close()

	// Open file
	file, err := os.OpenFile("test.txt", os.O_RDWR|os.O_CREATE, 0o755)
	if err != nil {
		panic(err)
	}
	defer file.Close()
	defer os.Remove(file.Name()) // clean up

	// Add file to root archive
	if err = encoder.Add("test.txt", file); err != nil {
		panic(err)
	}

	// Or add file to sub folder archive
	//if err = encoder.Add("subFolder/test.txt", file); err != nil {
	//	panic(err)
	//}

	// Close archive writer
	if err = encoder.Close(); err != nil {
		panic(err)
	}

	// Output:
}

// Utils

func newBufferSized(buf []byte) *bufferSized {
	return &bufferSized{Buffer: bytes.NewBuffer(buf), cap: cap(buf)}
}

type bufferSized struct {
	*bytes.Buffer
	cap int
}

func (b *bufferSized) Write(p []byte) (int, error) {
	if len(p)+b.Len() > b.Cap() {
		return 0, errors.New("buffer overflow") //nolint:goerr113
	}

	return b.Buffer.Write(p)
}
