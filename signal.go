package gommon

import (
	"os"
	"os/signal"
)

// Signal is channel of system signal
func Signal(sigs ...os.Signal) <-chan os.Signal {
	var s = make(chan os.Signal, 1)
	signal.Notify(s, sigs...)
	return s
}
