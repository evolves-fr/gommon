package gommon

import (
	"math/rand"
	"time"
	"unsafe"
)

const (
	letterIdxBits = 6                    // 6 bits to represent a letter index
	letterIdxMask = 1<<letterIdxBits - 1 // All 1-bits, as many as letterIdxBits
	letterIdxMax  = 63 / letterIdxBits   // # of letter indices fitting in 63 bits

	LowerTable   = `abcdefghijklmnopqrstuvwxyz`
	UpperTable   = `ABCDEFGHIJKLMNOPQRSTUVWXYZ`
	NumericTable = `0123456789`
	SymbolsTable = `!"#$%&'()*+,-./:;<=>?@[\]^_{|}~` + "`"
)

func Random(generator RandomGenerator, length int) string {
	if generator == nil {
		generator = NewStandardRandom()
	}

	return generator.Generate(length)
}

type RandomGenerator interface {
	Generate(length int) string
}

func NewCustomRandom(chars string) RandomGenerator {
	return randomGenerator(chars)
}

func NewLowerLettersRandom() RandomGenerator {
	return randomGenerator(LowerTable)
}

func NewUpperLettersRandom() RandomGenerator {
	return randomGenerator(UpperTable)
}

func NewLettersRandom() RandomGenerator {
	return randomGenerator(LowerTable + UpperTable)
}

func NewNumberRandom() RandomGenerator {
	return randomGenerator(NumericTable)
}

func NewAlphanumericRandom() RandomGenerator {
	return randomGenerator(LowerTable + UpperTable + NumericTable)
}

func NewStandardRandom() RandomGenerator {
	return randomGenerator(LowerTable + UpperTable + NumericTable + SymbolsTable)
}

type randomGenerator string

func (g randomGenerator) Generate(length int) string {
	src := rand.NewSource(time.Now().UnixNano())

	value := make([]byte, length)

	// A src.Int63() generates 63 random bits, enough for letterIdxMax characters!
	for increment, cache, remain := length-1, src.Int63(), letterIdxMax; increment >= 0; {
		if remain == 0 {
			cache, remain = src.Int63(), letterIdxMax
		}

		if idx := int(cache & letterIdxMask); idx < len(g) {
			value[increment] = g[idx]
			increment--
		}

		cache >>= letterIdxBits

		remain--
	}

	return *(*string)(unsafe.Pointer(&value))
}
