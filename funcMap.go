package gommon

import (
	"net/url"
	"os"
	"path"
	"regexp"
	"strings"
	"time"
)

var funcMap = map[string]any{
	"bool":          Parse[bool],
	"int":           Parse[int],
	"int8":          Parse[int8],
	"int16":         Parse[int16],
	"int32":         Parse[int32],
	"int64":         Parse[int64],
	"uint":          Parse[uint],
	"uint8":         Parse[uint8],
	"uint16":        Parse[uint16],
	"uint32":        Parse[uint32],
	"uint64":        Parse[uint64],
	"uintPtr":       Parse[uintptr],
	"float32":       Parse[float32],
	"float64":       Parse[float64],
	"complex64":     Parse[complex64],
	"complex128":    Parse[complex128],
	"duration":      Parse[time.Duration],
	"time":          Parse[time.Time],
	"timestamp":     Parse[Timestamp],
	"dsn":           Parse[DSN],
	"base64":        Parse[Base64],
	"url":           Parse[*url.URL],
	"values":        Parse[url.Values],
	"default":       fmDefault[any],
	"ternary":       Ternary[any],
	"empty":         Empty[any],
	"compact":       Compact[string],
	"env":           os.Getenv,
	"indent":        fmIndent,
	"nindent":       fmNIndent,
	"regexMatch":    fmRegexpMatchStringMatchString,
	"contains":      fmStringsContains,
	"hasPrefix":     fmStringsHasPrefix,
	"hasSuffix":     fmStringsHasSuffix,
	"trimSpace":     strings.TrimSpace,
	"upper":         strings.ToUpper,
	"lower":         strings.ToLower,
	"title":         strings.Title,
	"split":         fmStringsSplit,
	"now":           time.Now,
	"pathBase":      path.Base,
	"pathDir":       path.Dir,
	"pathClean":     path.Clean,
	"pathExt":       path.Ext,
	"pathIsAbs":     path.IsAbs,
	"pathExist":     PathExist,
	"pathEmpty":     PathEmpty,
	"semverCompare": SemverCompare,
}

// FuncMap returns list of functions
func FuncMap() map[string]any {
	fm := make(map[string]interface{}, len(funcMap))
	for k, v := range funcMap {
		fm[k] = v
	}
	return fm
}

func fmDefault[T any](v2, v1 T) T {
	return Default[T](v1, v2)
}

func fmRegexpMatchStringMatchString(str, pattern string) (matched bool, err error) {
	return regexp.MatchString(pattern, str)
}

func fmStringsContains(substr, str string) bool {
	return strings.Contains(str, substr)
}

func fmStringsHasPrefix(prefix, str string) bool {
	return strings.HasPrefix(str, prefix)
}

func fmStringsHasSuffix(suffix, str string) bool {
	return strings.HasSuffix(str, suffix)
}

func fmStringsSplit(sep, str string) []string {
	return strings.Split(str, sep)
}

func fmIndent(spaces int, v string) string {
	pad := strings.Repeat(" ", spaces)
	return pad + strings.Replace(v, "\n", "\n"+pad, -1)
}

func fmNIndent(spaces int, v string) string {
	return "\n" + fmIndent(spaces, v)
}
