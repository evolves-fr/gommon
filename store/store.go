package store

import "sync"

type Store interface {
	Has(key any) bool
	Get(key any) any
	Set(key, value any)
	Remove(key any)
}

func New() Store {
	return &store{
		data:  make(map[any]any),
		mutex: new(sync.RWMutex),
	}
}

type store struct {
	data  map[any]any
	mutex *sync.RWMutex
}

func (s *store) Has(key any) bool {
	s.mutex.RLock()
	defer s.mutex.RUnlock()

	_, ok := s.data[key]

	return ok
}

func (s *store) Get(key any) any {
	s.mutex.RLock()
	defer s.mutex.RUnlock()

	return s.data[key]
}

func (s *store) Set(key, value any) {
	s.mutex.Lock()

	s.data[key] = value

	s.mutex.Unlock()
}

func (s *store) Remove(key any) {
	s.mutex.Lock()

	delete(s.data, key)

	s.mutex.Unlock()
}
