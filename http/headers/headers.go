package headers

// Source: https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers

const (
	// Accept Informs the server about the types of data that can be sent back.
	Accept = "Accept"

	// AcceptCH Servers can advertise support for Client Hints using the Accept-CH header field or an equivalent
	// HTML <meta> element with http-equiv attribute.
	AcceptCH = "Accept-CH"

	// AcceptCharset Advertised a client's supported character encodings. It is no longer widely used.
	AcceptCharset = "Accept-Charset"

	// AcceptEncoding The encoding algorithm, usually a compression algorithm, that can be used on the resource
	// sent back.
	AcceptEncoding = "Accept-Encoding"

	// AcceptLanguage Informs the server about the human language the server is expected to send back. This is a
	// hint and is not necessarily under the full control of the user: the server should always pay attention not to
	// override an explicit user choice (like selecting a language from a dropdown).
	AcceptLanguage = "Accept-Language"

	// AcceptPatch Advertises which media-type the server is able to understand in a PATCH request.
	AcceptPatch = "Accept-Patch"

	// AcceptPost Advertises which media types are accepted by the server for HTTP post requests.
	AcceptPost = "Accept-Post"

	// AcceptRanges Indicates if the server supports range requests, and if so in which unit the range can be
	// expressed.
	AcceptRanges = "Accept-Ranges"

	// AccessControlAllowCredentials Indicates whether the response to the request can be exposed when the
	// credentials flag is true.
	AccessControlAllowCredentials = "Access-Control-Allow-Credentials"

	// AccessControlAllowHeaders Used in response to a preflight request to indicate which HTTP headers can be
	// used when making the actual request.
	AccessControlAllowHeaders = "Access-Control-Allow-Headers"

	// AccessControlAllowMethods Specifies the methods allowed when accessing the resource in response to a
	// preflight request.
	AccessControlAllowMethods = "Access-Control-Allow-Methods"

	// AccessControlAllowOrigin Indicates whether the response can be shared.
	AccessControlAllowOrigin = "Access-Control-Allow-Origin"

	// AccessControlExposeHeaders Indicates which headers can be exposed as part of the response by listing
	// their names.
	AccessControlExposeHeaders = "Access-Control-Expose-Headers"

	// AccessControlMaxAge Indicates how long the results of a preflight request can be cached.
	AccessControlMaxAge = "Access-Control-Max-Age"

	// AccessControlRequestHeaders Used when issuing a preflight request to let the server know which HTTP
	// headers will be used when the actual request is made.
	AccessControlRequestHeaders = "Access-Control-Request-Headers"

	// AccessControlRequestMethod Used when issuing a preflight request to let the server know which HTTP
	// method will be used when the actual request is made.
	AccessControlRequestMethod = "Access-Control-Request-Method"

	// Age The time, in seconds, that the object has been in a proxy cache.
	Age = "Age"

	// Allow Lists the set of HTTP request methods supported by a resource.
	Allow = "Allow"

	// AltSvc Used to list alternate ways to reach this service.
	AltSvc = "Alt-Svc"

	// Authorization Contains the credentials to authenticate a user-agent with a server.
	Authorization = "Authorization"

	// CacheControl Directives for caching mechanisms in both requests and responses.
	CacheControl = "Cache-Control"

	// ClearSiteData Clears browsing data (e.g. cookies, storage, cache) associated with the requesting website.
	ClearSiteData = "Clear-Site-Data"

	// Connection Controls whether the network connection stays open after the current transaction finishes.
	Connection = "Connection"

	// ContentDisposition Indicates if the resource transmitted should be displayed inline
	// (default behavior without the header), or if it should be handled like a download and the browser should
	// present a "Save As" dialog.
	ContentDisposition = "Content-Disposition"

	// ContentEncoding Used to specify the compression algorithm.
	ContentEncoding = "Content-Encoding"

	// ContentLanguage Describes the human language(s) intended for the audience, so that it allows a user to
	// differentiate according to the users' own preferred language.
	ContentLanguage = "Content-Language"

	// ContentLength The size of the resource, in decimal number of bytes.
	ContentLength = "Content-Length"

	// ContentLocation Indicates an alternate location for the returned data.
	ContentLocation = "Content-Location"

	// ContentRange Indicates where in a full body message a partial message belongs.
	ContentRange = "Content-Range"

	// ContentSecurityPolicy Controls resources the user agent is allowed to load for a given page.
	ContentSecurityPolicy = "Content-Security-Policy"

	// ContentSecurityPolicyReportOnly Allows web developers to experiment with policies by monitoring, but not
	// enforcing, their effects. These violation reports consist of JSON documents sent via an HTTP POST request to
	// the specified URI.
	ContentSecurityPolicyReportOnly = "Content-Security-Policy-Report-Only"

	// ContentType Indicates the media type of the resource.
	ContentType = "Content-Type"

	// Cookie Contains stored HTTP cookies previously sent by the server with the Set-Cookie header.
	Cookie = "Cookie"

	// CrossOriginEmbedderPolicy Allows a server to declare an embedder policy for a given document.
	CrossOriginEmbedderPolicy = "Cross-Origin-Embedder-Policy"

	// CrossOriginOpenerPolicy Prevents other domains from opening/controlling a window.
	CrossOriginOpenerPolicy = "Cross-Origin-Opener-Policy"

	// CrossOriginResourcePolicy Prevents other domains from reading the response of the resources to which this
	// header is applied.
	CrossOriginResourcePolicy = "Cross-Origin-Resource-Policy"

	// Date Contains the date and time at which the message was originated.
	Date = "Date"

	// Digest Provides a digest of the selected representation of the requested resource.
	Digest = "Digest"

	// DNT (Do Not Track) request header indicates the user's tracking preference. It lets users indicate
	// whether they would prefer privacy rather than personalized content.
	DNT = "DNT"

	// ETag A unique string identifying the version of the resource. Conditional requests using If-Match and
	// If-None-Match use this value to change the behavior of the request.
	ETag = "ETag"

	// Expect Indicates expectations that need to be fulfilled by the server to properly handle the request.
	Expect = "Expect"

	// ExpectCT Allows sites to opt in to reporting and/or enforcement of Certificate Transparency requirements,
	// which prevents the use of misissued certificates for that site from going unnoticed. When a site enables the
	// Expect-CT header, they are requesting that Chrome check that any certificate for that site appears in public
	// CT logs.
	ExpectCT = "Expect-CT"

	// Expires The date/time after which the response is considered stale.
	Expires = "Expires"

	// Forwarded Contains information from the client-facing side of proxy servers that is altered or lost when
	// a proxy is involved in the path of the request.
	Forwarded = "Forwarded"

	// From Contains an Internet email address for a human user who controls the requesting user agent.
	From = "From"

	// Host Specifies the domain name of the server (for virtual hosting), and (optionally) the TCP port number
	// on which the server is listening.
	Host = "Host"

	// IfMatch Makes the request conditional, and applies the method only if the stored resource matches one of
	// the given ETags.
	IfMatch = "If-Match"

	// IfModifiedSince Makes the request conditional, and expects the resource to be transmitted only if it has
	// been modified after the given date. This is used to transmit data only when the cache is out of date.
	IfModifiedSince = "If-Modified-Since"

	// IfNoneMatch Makes the request conditional, and applies the method only if the stored resource doesn't
	// match any of the given ETags. This is used to update caches (for safe requests), or to prevent uploading a new
	// resource when one already exists.
	IfNoneMatch = "If-None-Match"

	// IfRange Creates a conditional range request that is only fulfilled if the given etag or date matches
	// the remote resource. Used to prevent downloading two ranges from incompatible version of the resource.
	IfRange = "If-Range"

	// IfUnmodifiedSince Makes the request conditional, and expects the resource to be transmitted only if it has
	// not been modified after the given date. This ensures the coherence of a new fragment of a specific range with
	// previous ones, or to implement an optimistic concurrency control system when modifying existing documents.
	IfUnmodifiedSince = "If-Unmodified-Since"

	// KeepAlive Controls how long a persistent connection should stay open.
	KeepAlive = "Keep-Alive"

	// LargeAllocation Tells the browser that the page being loaded is going to want to perform a large
	// allocation.
	// Deprecated: Not for use in new websites.
	LargeAllocation = "Large-Allocation"

	// LastModified The last modification date of the resource, used to compare several versions of the same
	// resource. It is less accurate than ETag, but easier to calculate in some environments. Conditional requests
	// using If-Modified-Since and If-Unmodified-Since use this value to change the behavior of the request.
	LastModified = "Last-Modified"

	// Link The Link entity-header field provides a means for serializing one or more links in HTTP headers.
	// It is semantically equivalent to the HTML <link> element.
	Link = "Link"

	// Location Indicates the URL to redirect a page to.
	Location = "Location"

	// MaxForwards When using TRACE, indicates the maximum number of hops the request can do before being
	// reflected to the sender.
	MaxForwards = "Max-Forwards"

	// NEL Defines a mechanism that enables developers to declare a network error reporting policy.
	NEL = "NEL"

	// Origin Indicates where a fetch originates from.
	Origin = "Origin"

	// Pragma Implementation-specific header that may have various effects anywhere along the request-response
	// chain. Used for backwards compatibility with HTTP/1.0 caches where the Cache-Control header is not yet present.
	// Deprecated: Not for use in new websites.
	Pragma = "Pragma"

	// ProxyAuthenticate Defines the authentication method that should be used to access a resource behind a
	// proxy server.
	ProxyAuthenticate = "Proxy-Authenticate"

	// ProxyAuthorization Contains the credentials to authenticate a user agent with a proxy server.
	ProxyAuthorization = "Proxy-Authorization"

	// Range Indicates the part of a document that the server should return.
	Range = "Range"

	// Referer The address of the previous web page from which a link to the currently requested page was
	// followed.
	Referer = "Referer"

	// ReferrerPolicy Governs which referrer information sent in the Referer header should be included with
	// requests made.
	ReferrerPolicy = "Referrer-Policy"

	// RetryAfter Indicates how long the user agent should wait before making a follow-up request.
	RetryAfter = "Retry-After"

	// SaveData A boolean that indicates the user agent's preference for reduced data usage.
	SaveData = "Save-Data"

	// SecFetchDest It is a request header that indicates the request's destination to a server. It is a
	// Structured Header whose value is a token with possible values audio, audioworklet, document, embed, empty,
	// font, image, manifest, object, paintworklet, report, script, serviceworker, sharedworker, style, track, video,
	// worker, and xslt.
	SecFetchDest = "Sec-Fetch-Dest"

	// SecFetchMode It is a request header that indicates the request's mode to a server. It is a Structured
	// Header whose value is a token with possible values cors, navigate, no-cors, same-origin, and websocket.
	SecFetchMode = "Sec-Fetch-Mode"

	// SecFetchSite It is a request header that indicates the relationship between a request initiator's origin
	// and its target's origin. It is a Structured Header whose value is a token with possible values cross-site,
	// same-origin, same-site, and none.
	SecFetchSite = "Sec-Fetch-Site"

	// SecFetchUser It is a request header that indicates whether or not a navigation request was triggered by
	// user activation. It is a Structured Header whose value is a boolean so possible values are ?0 for false and ?1
	// for true.
	SecFetchUser = "Sec-Fetch-User"

	// SecWebSocketAccept Used in the websocket opening handshake. It would appear in the response headers.
	// That is, this is header is sent from server to client to inform that server is willing to initiate a websocket
	// connection.
	SecWebSocketAccept = "Sec-WebSocket-Accept"

	// Server Contains information about the software used by the origin server to handle the request.
	Server = "Server"

	// ServerTiming Communicates one or more metrics and descriptions for the given request-response cycle.
	ServerTiming = "Server-Timing"

	// ServiceWorkerNavigationPreload A request header sent in preemptive request to fetch() a resource during
	// service worker boot. The value, which is set with NavigationPreloadManager.setHeaderValue(), can be used to
	// inform a server that a different resource should be returned than in a normal fetch() operation.
	ServiceWorkerNavigationPreload = "Service-Worker-Navigation-Preload"

	// SetCookie Send cookies from the server to the user-agent.
	SetCookie = "Set-Cookie"

	// SourceMap Links generated code to a source map.
	SourceMap = "SourceMap"

	// StrictTransportSecurity Force communication using HTTPS instead of HTTP.
	StrictTransportSecurity = "Strict-Transport-Security"

	// TE Specifies the transfer encodings the user agent is willing to accept.
	TE = "TE"

	// TimingAllowOrigin Specifies origins that are allowed to see values of attributes retrieved via features
	// of the Resource Timing API, which would otherwise be reported as zero due to cross-origin restrictions.
	TimingAllowOrigin = "Timing-Allow-Origin"

	// Tk Indicates the tracking status that applied to the corresponding request.
	Tk = "Tk"

	// Trailer Allows the sender to include additional fields at the end of chunked message.
	Trailer = "Trailer"

	// TransferEncoding Specifies the form of encoding used to safely transfer the resource to the user.
	TransferEncoding = "Transfer-Encoding"

	// Upgrade The relevant RFC document for the Upgrade header field is RFC 7230, section 6.7. The standard
	// establishes rules for upgrading or changing to a different protocol on the current client, server, transport
	// protocol connection. For example, this header standard allows a client to change from HTTP 1.1 to HTTP 2.0,
	// assuming the server decides to acknowledge and implement the Upgrade header field. Neither party is required
	// to accept the terms specified in the Upgrade header field. It can be used in both client and server headers.
	// If the Upgrade header field is specified, then the sender MUST also send the Connection header field with the
	// upgrade option specified. For details on the Connection header field please see section 6.1 of the
	// aforementioned RFC.
	Upgrade = "Upgrade"

	// UpgradeInsecureRequests Sends a signal to the server expressing the client's preference for an encrypted
	// and authenticated response, and that it can successfully handle the upgrade-insecure-requests directive.
	UpgradeInsecureRequests = "Upgrade-Insecure-Requests"

	// UserAgent Contains a characteristic string that allows the network protocol peers to identify the
	// application type, operating system, software vendor or software version of the requesting software user agent.
	// See also the Firefox user agent string reference.
	UserAgent = "User-Agent"

	// Vary Determines how to match request headers to decide whether a cached response can be used rather than
	// requesting a fresh one from the origin server.
	Vary = "Vary"

	// Via Added by proxies, both forward and reverse proxies, and can appear in the request headers and the
	// response headers.
	Via = "Via"

	// WantDigest Is primarily used in a request, to ask the server to provide a digest of the requested
	// resource using the Digest response header.
	WantDigest = "Want-Digest"

	// Warning General warning information about possible problems.
	// Deprecated: Not for use in new websites.
	Warning = "Warning"

	// WWWAuthenticate Defines the authentication method that should be used to access a resource.
	WWWAuthenticate = "WWW-Authenticate"

	// XContentTypeOptions Disables MIME sniffing and forces browser to use the type given in Content-Type.
	XContentTypeOptions = "X-Content-Type-Options"

	// XDNSPrefetchControl Controls DNS prefetching, a feature by which browsers proactively perform domain
	// name resolution on both links that the user may choose to follow as well as URLs for items referenced by the
	// document, including images, CSS, JavaScript, and so forth.
	XDNSPrefetchControl = "X-DNS-Prefetch-Control"

	// XForwardedFor Identifies the originating IP addresses of a client connecting to a web server through an
	// HTTP proxy or a load balancer.
	XForwardedFor = "X-Forwarded-For"

	// XForwardedHost Identifies the original host requested that a client used to connect to your proxy or
	// load balancer.
	XForwardedHost = "X-Forwarded-Host"

	// XForwardedProto Identifies the protocol (HTTP or HTTPS) that a client used to connect to your proxy or
	// load balancer.
	XForwardedProto = "X-Forwarded-Proto"

	// XFrameOptions Indicates whether a browser should be allowed to render a page in a <frame>, <iframe>,
	// <embed> or <object>.
	XFrameOptions = "X-Frame-Options"

	// XXSSProtection Enables cross-site scripting filtering.
	XXSSProtection = "X-XSS-Protection"
)
