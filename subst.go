package gommon

import (
	"bytes"
	"errors"
	"io"
	"text/template"
)

func Subst(input []byte) ([]byte, error) {
	// Initialize buffer
	var buffer bytes.Buffer

	// Execute
	if err := SubstIO(bytes.NewReader(input), &buffer); err != nil {
		return nil, err
	}

	// Return result
	return buffer.Bytes(), nil

}

func SubstIO(input io.Reader, output io.Writer) error {
	return NewSubstitution(input).Funcs(FuncMap()).Execute(output)
}

func NewSubstitution(input io.Reader) *Substitution {
	return &Substitution{input: input}
}

type Substitution struct {
	input     io.Reader
	functions map[string]any
}

func (s *Substitution) Func(name string, fn any) *Substitution {
	if s.functions == nil {
		s.functions = make(map[string]any)
	}

	s.functions[name] = fn

	return s
}

func (s *Substitution) Funcs(funcMap map[string]any) *Substitution {
	for k, v := range funcMap {
		s.Func(k, v)
	}

	return s
}

func (s *Substitution) Execute(output io.Writer) error {
	// Verify if input reader is not set
	if s.input == nil {
		return errors.New("input reader is nil")
	}

	// Verify if output writer is not set
	if output == nil {
		return errors.New("output writer is nil")
	}

	// Read input data
	data, err := io.ReadAll(s.input)
	if err != nil {
		return err
	}

	// Initialize template renderer
	tpl, err := template.New("").Funcs(s.functions).Parse(string(data))
	if err != nil {
		return err
	}

	// Execute template
	return tpl.Execute(output, nil)
}
