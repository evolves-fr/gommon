package env

import (
	"bytes"
	"io"
	"os"
	"regexp"
	"sort"
	"strings"

	"gitlab.com/evolves-fr/gommon"
)

func Environ() io.Reader {
	return strings.NewReader(strings.Join(os.Environ(), "\n"))
}

type Env interface {
	Keys() []string
	KeysWithRegex(regex string) []string
	Has(key string) bool
	HasWithRegex(regex string) bool
	Get(key string) string
	Clone() Env
	Encode() []byte
}

func LoadDotEnv(r io.Reader) (Env, error) {
	// Read contents of reader
	data, err := io.ReadAll(r)
	if err != nil {
		return nil, err
	}

	// Prepare object
	environ := make(Environment)

	// Split lines
	for _, line := range gommon.Split[string](string(data), "\n") {
		// Split key value
		keyValue := strings.SplitN(line, "=", 2)
		if len(keyValue) != 2 {
			continue
		}

		environ[keyValue[0]] = keyValue[1]
	}

	return environ, nil
}

func LoadEnviron() (Env, error) {
	return LoadDotEnv(Environ())
}

type Environment map[string]string

func (e Environment) Keys() []string {
	// Prepare slice results
	keys := make([]string, 0)

	// Process
	for key := range e {
		keys = append(keys, key)
	}

	// Sort
	sort.Slice(keys, func(i, j int) bool {
		return keys[i] < keys[j]
	})

	return keys
}

func (e Environment) KeysWithRegex(regex string) []string {
	// Prepare regex processor
	rxp := regexp.MustCompile(regex)

	// Prepare slice results
	keys := make([]string, 0)

	// Process
	for key := range e {
		if rxp.MatchString(key) {
			keys = append(keys, key)
		}
	}

	// Sort
	sort.Slice(keys, func(i, j int) bool {
		return keys[i] < keys[j]
	})

	return keys
}

func (e Environment) Has(key string) bool {
	_, ok := e[key]

	return ok
}

func (e Environment) HasWithRegex(regex string) bool {
	// Prepare regex processor
	rxp := regexp.MustCompile(regex)

	// Process
	for key := range e {
		if rxp.MatchString(key) {
			return true
		}
	}

	return false
}

func (e Environment) Get(key string) string {
	if v, ok := e[key]; ok {
		return v
	}

	return ""
}

func (e Environment) Clone() Env {
	e2 := make(Environment)
	for key, value := range e {
		e2[key] = value
	}

	return e2
}

func (e Environment) Encode() []byte {
	var buffer bytes.Buffer

	for _, key := range e.Keys() {
		buffer.WriteString(key + "=" + e[key] + "\n")
	}

	return buffer.Bytes()
}
