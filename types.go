package gommon

import (
	"bytes"
	"encoding/base64"
	"encoding/binary"
	"encoding/json"
	"errors"
	"net/url"
	"regexp"
	"strconv"
	"strings"
	"time"

	"gopkg.in/yaml.v3"
)

// Number generic type for int, int8, int16, int32, int64, uint, uint8, uint16, uint32 and uint64
type Number interface {
	~int | ~int8 | ~int16 | ~int32 | ~int64 | ~uint | ~uint8 | ~uint16 | ~uint32 | ~uint64
}

// Float generic type for float32 and float64
type Float interface {
	~float32 | ~float64
}

// Complex generic type for complex64 and complex128
type Complex interface {
	~complex64 | ~complex128
}

// ParseBase64 decode to Base64.
func ParseBase64(str string) (Base64, error) {
	value, err := base64.StdEncoding.Strict().DecodeString(str)
	if err != nil {
		return nil, err
	}

	return value, nil
}

// Base64 standard type
type Base64 []byte

// String implements the fmt.Stringer interface.
func (v Base64) String() string {
	return string(v)
}

// Encode return Base64 encoded
func (v Base64) Encode() string {
	encoded := make([]byte, base64.StdEncoding.Strict().EncodedLen(len(v)))
	base64.StdEncoding.Strict().Encode(encoded, v)
	return string(encoded)
}

// MarshalText implements the encoding.TextMarshaler interface.
func (v Base64) MarshalText() ([]byte, error) {
	return []byte(v.Encode()), nil
}

// UnmarshalText implements the encoding.TextUnmarshaler interface.
func (v *Base64) UnmarshalText(data []byte) error {
	decoded := make([]byte, base64.StdEncoding.Strict().DecodedLen(len(data)))

	n, err := base64.StdEncoding.Strict().Decode(decoded, data)
	if err != nil {
		return err
	}

	*v = decoded[:n]

	return nil
}

// MarshalJSON implements the json.Marshaler interface.
func (v Base64) MarshalJSON() ([]byte, error) {
	return []byte(`"` + v.Encode() + `"`), nil
}

// UnmarshalJSON implements the json.Unmarshaler interface.
func (v *Base64) UnmarshalJSON(data []byte) error {
	return v.UnmarshalText(bytes.TrimRight(bytes.TrimLeft(data, `"`), `"`))
}

// MarshalYAML implements the yaml.Marshaler interface.
func (v Base64) MarshalYAML() (interface{}, error) {
	value := v.Encode()

	return yaml.Node{ //nolint:exhaustruct
		Kind:  yaml.ScalarNode,
		Style: Ternary(Empty(value), yaml.DoubleQuotedStyle, 0),
		Value: value,
	}, nil
}

// UnmarshalYAML implements the yaml.Unmarshaler interface.
func (v *Base64) UnmarshalYAML(value *yaml.Node) error {
	return v.UnmarshalText(bytes.TrimSuffix([]byte(value.Value), []byte("\n")))
}

// UnmarshalENV implements the env.Unmarshaler interface.
func (v *Base64) UnmarshalENV(data []byte) error {
	return v.UnmarshalText(data)
}

// ParseDSN parses a raw dsn into a DSN structure.
func ParseDSN(str string) (DSN, error) {
	// Prepare regex [scheme://][username[:password]@][protocol(endpoint[:port])][/path][?param1=value1&paramN=valueN]
	regex := regexp.MustCompile(`^(?:(?P<scheme>[^://]+)://)?` +
		`(?:(?P<username>[^:]+)?(?:\:(?P<password>[^@]+))?@)?` +
		`(?:(?P<protocol>tcp6?|udp6?)\()?(?P<endpoint>[^:)/?]+)?(?:\:(?P<port>\d[^)/?]+))?\)?` +
		`(?:/(?P<path>[^\?]*))?` +
		`(?:\?(?P<queries>.+))?$`)

	// Check if valid DSN
	if !regex.MatchString(str) {
		return DSN{}, errors.New("not valid DSN")
	}

	// Match regex with raw value
	match := regex.FindStringSubmatch(str)

	// Set values
	dsn := DSN{
		Scheme:   match[regex.SubexpIndex("scheme")],
		Username: match[regex.SubexpIndex("username")],
		Password: match[regex.SubexpIndex("password")],
		Protocol: match[regex.SubexpIndex("protocol")],
		Endpoint: match[regex.SubexpIndex("endpoint")],
		Port:     MustParse[int](match[regex.SubexpIndex("port")]),
		Path:     match[regex.SubexpIndex("path")],
		Queries:  MustParse[url.Values](match[regex.SubexpIndex("queries")]),
	}

	return dsn, nil
}

// DSN (Data Source Name) is advanced structure for describe to connection a data source.
type DSN struct {
	Scheme   string
	Username string
	Password string
	Protocol string
	Endpoint string
	Port     int
	Path     string
	Queries  url.Values
}

// String reassembles the DSN into a valid DSN string.
func (v DSN) String() string {
	var buffer bytes.Buffer

	buffer.WriteString(Ternary(!Empty(v.Scheme), v.Scheme+"://", ""))
	buffer.WriteString(Ternary(!Empty(v.Username), v.Username, ""))
	buffer.WriteString(Ternary(!Empty(v.Password), ":"+v.Password, ""))
	buffer.WriteString(Ternary(!Empty(v.Username) || !Empty(v.Password), "@", ""))
	buffer.WriteString(Ternary(!Empty(v.Protocol), v.Protocol+"(", ""))
	buffer.WriteString(Ternary(!Empty(v.Endpoint), v.Endpoint, ""))
	buffer.WriteString(Ternary(!Empty(v.Port), ":"+strconv.Itoa(v.Port), ""))
	buffer.WriteString(Ternary(!Empty(v.Protocol), ")", ""))
	buffer.WriteString(Ternary(!Empty(v.Path), "/"+v.Path, ""))
	buffer.WriteString(Ternary(v.Queries != nil && v.Queries.Encode() != "", "?"+v.Queries.Encode(), ""))

	return buffer.String()
}

// Redacted is like String but replaces any password with "xxxxx".
func (v DSN) Redacted() string {
	dsn := v
	if !Empty(dsn.Password) {
		dsn.Password = "xxxxx"
	}

	return dsn.String()
}

// MarshalText implements the encoding.TextMarshaler interface.
func (v DSN) MarshalText() ([]byte, error) {
	return []byte(v.String()), nil
}

// UnmarshalText implements the encoding.TextUnmarshaler interface.
func (v *DSN) UnmarshalText(data []byte) error {
	var err error

	*v, err = ParseDSN(string(data))

	return err
}

// MarshalJSON implements the json.Marshaler interface.
func (v DSN) MarshalJSON() ([]byte, error) {
	raw, _ := v.MarshalText()

	return append(append([]byte(`"`), raw...), []byte(`"`)...), nil
}

// UnmarshalJSON implements the json.Unmarshaler interface.
func (v *DSN) UnmarshalJSON(data []byte) error {
	return v.UnmarshalText(bytes.TrimRight(bytes.TrimLeft(data, `"`), `"`))
}

// MarshalYAML implements the yaml.Marshaler interface.
func (v DSN) MarshalYAML() (interface{}, error) {
	raw, _ := v.MarshalText()

	return string(raw), nil
}

// UnmarshalYAML implements the yaml.Unmarshaler interface.
func (v *DSN) UnmarshalYAML(value *yaml.Node) error {
	return v.UnmarshalText([]byte(value.Value))
}

// UnmarshalENV implements the env.Unmarshaler interface.
func (v *DSN) UnmarshalENV(data []byte) error {
	return v.UnmarshalText(data)
}

// StringSlice implement Marshaler and Unmarshaler interfaces.
type StringSlice []string

// MarshalText implements the encoding.TextMarshaler interface.
func (v StringSlice) MarshalText() ([]byte, error) {
	return []byte(strings.Join(v, ",")), nil
}

// UnmarshalText implements the encoding.TextUnmarshaler interface.
func (v *StringSlice) UnmarshalText(data []byte) error {
	*v = Split[string](string(data), ",")

	return nil
}

// MarshalJSON implements the json.Marshaler interface.
func (v StringSlice) MarshalJSON() ([]byte, error) {
	var buffer bytes.Buffer

	buffer.WriteString(`[`)

	for i := 0; i < len(v); i++ {
		buffer.WriteString(`"` + v[i] + `"`)

		if i != len(v)-1 {
			buffer.WriteString(`,`)
		}
	}

	buffer.WriteString(`]`)

	return buffer.Bytes(), nil
}

// UnmarshalJSON implements the json.Unmarshaler interface.
func (v *StringSlice) UnmarshalJSON(data []byte) error {
	if d := strings.TrimSpace(string(data)); strings.HasPrefix(d, "{") && strings.HasSuffix(d, "}") {
		return nil
	}

	if bytes.HasPrefix(data, []byte(`"`)) && bytes.HasSuffix(data, []byte(`"`)) {
		data = bytes.TrimPrefix(data, []byte(`"`))
		data = bytes.TrimSuffix(data, []byte(`"`))
		data = bytes.ReplaceAll(data, []byte(`\"`), []byte(`"`))
	}

	var values []string
	if json.Valid(data) || (bytes.HasPrefix(data, []byte("[")) && bytes.HasSuffix(data, []byte("]"))) {
		if err := json.Unmarshal(data, &values); err != nil {
			return err
		}
	} else {
		values = Split[string](string(data), ",")
	}

	for _, value := range values {
		if json.Valid([]byte(value)) {
			_ = v.UnmarshalJSON([]byte(value))
		} else {
			*v = append(*v, Split[string](strings.Trim(value, `"`), ",")...)
		}
	}

	return nil
}

// MarshalYAML implements the yaml.Marshaler interface.
func (v StringSlice) MarshalYAML() (interface{}, error) {
	items := make([]*yaml.Node, 0)

	for _, value := range v {
		items = append(items, &yaml.Node{ //nolint:exhaustruct
			Kind:  yaml.ScalarNode,
			Style: Ternary(Empty(value), yaml.DoubleQuotedStyle, 0),
			Value: value,
		})
	}

	//nolint:exhaustruct
	return yaml.Node{Kind: yaml.SequenceNode, Style: yaml.FlowStyle, Content: items}, nil
}

// UnmarshalYAML implements the yaml.Unmarshaler interface.
func (v *StringSlice) UnmarshalYAML(value *yaml.Node) error {
	return v.UnmarshalText(bytes.TrimSuffix([]byte(value.Value), []byte("\n")))
}

// ParseTimestamp decode to Timestamp.
func ParseTimestamp(value string) (Timestamp, error) {
	ts, err := Parse[int64](value)
	if err != nil {
		return Timestamp(0), err
	}

	return Timestamp(ts), nil
}

// Timestamp is alias of time.Time with Marshaler and Unmarshaler interfaces.
type Timestamp int64

// Time return standard time.Time object.
func (t Timestamp) Time() time.Time {
	return time.Unix(int64(t), 0)
}

// String implements the fmt.Stringer interface.
func (t Timestamp) String() string {
	return strconv.FormatInt(int64(t), 10)
}

// MarshalBinary implements the encoding.BinaryMarshaler interface.
func (t Timestamp) MarshalBinary() ([]byte, error) {
	buffer := new(bytes.Buffer)
	_ = binary.Write(buffer, binary.BigEndian, t)

	return buffer.Bytes(), nil
}

// UnmarshalBinary implements the encoding.BinaryUnmarshaler interface.
func (t *Timestamp) UnmarshalBinary(data []byte) error {
	var value int64
	if err := binary.Read(bytes.NewReader(data), binary.BigEndian, &value); err != nil {
		return err
	}

	*t = Timestamp(value)

	return nil
}

// MarshalText implements the encoding.TextMarshaler interface.
func (t Timestamp) MarshalText() ([]byte, error) {
	return []byte(strconv.FormatInt(int64(t), 10)), nil
}

// UnmarshalText implements the encoding.TextUnmarshaler interface.
func (t *Timestamp) UnmarshalText(data []byte) error {
	ts, err := ParseTimestamp(string(data))
	if err != nil {
		return err
	}

	*t = ts

	return nil
}

// MarshalJSON implements the json.Marshaler interface.
func (t Timestamp) MarshalJSON() ([]byte, error) {
	return t.MarshalText()
}

// UnmarshalJSON implements the json.Unmarshaler interface.
func (t *Timestamp) UnmarshalJSON(data []byte) error {
	return t.UnmarshalText(bytes.Trim(data, "\""))
}

// MarshalYAML implements the yaml.Marshaler interface.
func (t Timestamp) MarshalYAML() (interface{}, error) {
	return strconv.FormatInt(t.Time().Unix(), 10), nil
}

// UnmarshalYAML implements the yaml.Unmarshaler interface.
func (t *Timestamp) UnmarshalYAML(value *yaml.Node) error {
	return t.UnmarshalText([]byte(value.Value))
}

// UnmarshalENV implements the env.Unmarshaler interface.
func (t *Timestamp) UnmarshalENV(data []byte) error {
	return t.UnmarshalText(data)
}
