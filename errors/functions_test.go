package errors_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/evolves-fr/gommon/errors"
)

func TestJoin(t *testing.T) {
	t.Parallel()

	for _, test := range []struct {
		Expected string
		Errors   []error
	}{
		{
			Expected: "start\ncontent\nend",
			Errors:   []error{errors.New("start"), errors.New("content"), errors.New("end")},
		},
		{
			Expected: "start\nend",
			Errors:   []error{errors.New("start"), nil, errors.New("end")},
		},
	} {
		test := test

		t.Run(test.Expected, func(t *testing.T) {
			t.Parallel()

			assert.EqualError(t, errors.Join(test.Errors...), test.Expected)
		})
	}
}

func TestJoinWithSeparator(t *testing.T) {
	t.Parallel()

	for _, test := range []struct {
		Expected  string
		Separator string
		Errors    []error
	}{
		{
			Expected:  "start;content;end",
			Separator: ";",
			Errors:    []error{errors.New("start"), errors.New("content"), errors.New("end")},
		},
		{
			Expected:  "start|end",
			Separator: "|",
			Errors:    []error{errors.New("start"), nil, errors.New("end")},
		},
	} {
		test := test

		t.Run(test.Expected, func(t *testing.T) {
			t.Parallel()

			assert.EqualError(t, errors.JoinWithSeparator(test.Separator, test.Errors...), test.Expected)
		})
	}
}
