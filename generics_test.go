package gommon_test

import (
	"fmt"
	"net/url"
	"testing"
	"time"

	"github.com/stretchr/testify/suite"
	"gitlab.com/evolves-fr/gommon"
)

func TestCompact(t *testing.T) {
	t.Parallel()

	suite.Run(t, new(CompactSuite))
}

type CompactSuite struct {
	suite.Suite
}

func (suite *CompactSuite) TestBytes() {
	suite.Equal([]byte{0x61, 0x62, 0x63, 0x64}, gommon.Compact([]byte{0x00, 0x61, 0x62, 0x63, 0x64}))
	suite.Equal([]byte{0x61, 0x62, 0x63, 0x64}, gommon.Compact([]byte{0x61, 0x62, 0x63, 0x64, 0x00}))
	suite.Equal([]byte{0x61, 0x62, 0x63, 0x64}, gommon.Compact([]byte{0x61, 0x62, 0x00, 0x63, 0x64}))
	suite.Nil(gommon.Compact[byte](nil))
}

func (suite *CompactSuite) TestComplex64() {
	suite.Equal([]complex64{1.9, 2.8, 3.7, 0.5}, gommon.Compact([]complex64{0, 1.9, 2.8, 3.7, 0.5}))
	suite.Equal([]complex64{0.5, 3.7, 2.8, 1.9}, gommon.Compact([]complex64{0.5, 3.7, 2.8, 1.9, 0}))
	suite.Equal([]complex64{0.5, 3.7, 1.9, 2.8}, gommon.Compact([]complex64{0.5, 3.7, 0, 1.9, 2.8}))
	suite.Nil(gommon.Compact[complex64](nil))
}

func (suite *CompactSuite) TestComplex128() {
	suite.Equal([]complex128{1.9, 2.8, 3.7, 0.5}, gommon.Compact([]complex128{0, 1.9, 2.8, 3.7, 0.5}))
	suite.Equal([]complex128{0.5, 3.7, 2.8, 1.9}, gommon.Compact([]complex128{0.5, 3.7, 2.8, 1.9, 0}))
	suite.Equal([]complex128{0.5, 3.7, 1.9, 2.8}, gommon.Compact([]complex128{0.5, 3.7, 0, 1.9, 2.8}))
	suite.Nil(gommon.Compact[complex128](nil))
}

func (suite *CompactSuite) TestFloat32() {
	suite.Equal([]float32{1.9, 2.8, 3.7, 0.5}, gommon.Compact([]float32{0, 1.9, 2.8, 3.7, 0.5}))
	suite.Equal([]float32{0.5, 3.7, 2.8, 1.9}, gommon.Compact([]float32{0.5, 3.7, 2.8, 1.9, 0}))
	suite.Equal([]float32{0.5, 3.7, 1.9, 2.8}, gommon.Compact([]float32{0.5, 3.7, 0, 1.9, 2.8}))
	suite.Nil(gommon.Compact[float32](nil))
}

func (suite *CompactSuite) TestFloat64() {
	suite.Equal([]float64{1.9, 2.8, 3.7, 0.5}, gommon.Compact([]float64{0, 1.9, 2.8, 3.7, 0.5}))
	suite.Equal([]float64{0.5, 3.7, 2.8, 1.9}, gommon.Compact([]float64{0.5, 3.7, 2.8, 1.9, 0}))
	suite.Equal([]float64{0.5, 3.7, 1.9, 2.8}, gommon.Compact([]float64{0.5, 3.7, 0, 1.9, 2.8}))
	suite.Nil(gommon.Compact[float64](nil))
}

func (suite *CompactSuite) TestInt() {
	suite.Equal([]int{1, 2, 3}, gommon.Compact([]int{0, 1, 2, 3}))
	suite.Equal([]int{3, 2, 1}, gommon.Compact([]int{3, 2, 1, 0}))
	suite.Equal([]int{3, 2, 1, 1, 2, 3}, gommon.Compact([]int{3, 2, 1, 0, 1, 2, 3}))
	suite.Nil(gommon.Compact[int](nil))
}

func (suite *CompactSuite) TestInt8() {
	suite.Equal([]int8{1, 2, 3}, gommon.Compact([]int8{0, 1, 2, 3}))
	suite.Equal([]int8{3, 2, 1}, gommon.Compact([]int8{3, 2, 1, 0}))
	suite.Equal([]int8{3, 2, 1, 1, 2, 3}, gommon.Compact([]int8{3, 2, 1, 0, 1, 2, 3}))
	suite.Nil(gommon.Compact[int8](nil))
}

func (suite *CompactSuite) TestInt16() {
	suite.Equal([]int16{1, 2, 3}, gommon.Compact([]int16{0, 1, 2, 3}))
	suite.Equal([]int16{3, 2, 1}, gommon.Compact([]int16{3, 2, 1, 0}))
	suite.Equal([]int16{3, 2, 1, 1, 2, 3}, gommon.Compact([]int16{3, 2, 1, 0, 1, 2, 3}))
	suite.Nil(gommon.Compact[int16](nil))
}

func (suite *CompactSuite) TestInt32() {
	suite.Equal([]int32{1, 2, 3}, gommon.Compact([]int32{0, 1, 2, 3}))
	suite.Equal([]int32{3, 2, 1}, gommon.Compact([]int32{3, 2, 1, 0}))
	suite.Equal([]int32{3, 2, 1, 1, 2, 3}, gommon.Compact([]int32{3, 2, 1, 0, 1, 2, 3}))
	suite.Nil(gommon.Compact[int32](nil))

}

func (suite *CompactSuite) TestInt64() {
	suite.Equal([]int64{1, 2, 3}, gommon.Compact([]int64{0, 1, 2, 3}))
	suite.Equal([]int64{3, 2, 1}, gommon.Compact([]int64{3, 2, 1, 0}))
	suite.Equal([]int64{3, 2, 1, 1, 2, 3}, gommon.Compact([]int64{3, 2, 1, 0, 1, 2, 3}))
	suite.Nil(gommon.Compact[int64](nil))
}

func (suite *CompactSuite) TestRunes() {
	suite.Equal([]rune{0x61, 0x62, 0x63, 0x64}, gommon.Compact([]rune{0x00, 0x61, 0x62, 0x63, 0x64}))
	suite.Equal([]rune{0x61, 0x62, 0x63, 0x64}, gommon.Compact([]rune{0x61, 0x62, 0x63, 0x64, 0x00}))
	suite.Equal([]rune{0x61, 0x62, 0x63, 0x64}, gommon.Compact([]rune{0x61, 0x62, 0x00, 0x63, 0x64}))
	suite.Nil(gommon.Compact[rune](nil))
}

func (suite *CompactSuite) TestStrings() {
	suite.Equal([]string{"foo", "bar"}, gommon.Compact([]string{"", "foo", "bar"}))
	suite.Equal([]string{"foo", "bar"}, gommon.Compact([]string{"foo", "bar", ""}))
	suite.Equal([]string{"foo", "bar"}, gommon.Compact([]string{"foo", "", "bar"}))
	suite.Nil(gommon.Compact[string](nil))
}

func (suite *CompactSuite) TestUint() {
	suite.Equal([]uint{1, 2, 3}, gommon.Compact([]uint{0, 1, 2, 3}))
	suite.Equal([]uint{3, 2, 1}, gommon.Compact([]uint{3, 2, 1, 0}))
	suite.Equal([]uint{3, 2, 1, 1, 2, 3}, gommon.Compact([]uint{3, 2, 1, 0, 1, 2, 3}))
	suite.Nil(gommon.Compact[uint](nil))
}

func (suite *CompactSuite) TestUint8() {
	suite.Equal([]uint8{1, 2, 3}, gommon.Compact([]uint8{0, 1, 2, 3}))
	suite.Equal([]uint8{3, 2, 1}, gommon.Compact([]uint8{3, 2, 1, 0}))
	suite.Equal([]uint8{3, 2, 1, 1, 2, 3}, gommon.Compact([]uint8{3, 2, 1, 0, 1, 2, 3}))
	suite.Nil(gommon.Compact[uint8](nil))
}

func (suite *CompactSuite) TestUint16() {
	suite.Equal([]uint16{1, 2, 3}, gommon.Compact([]uint16{0, 1, 2, 3}))
	suite.Equal([]uint16{3, 2, 1}, gommon.Compact([]uint16{3, 2, 1, 0}))
	suite.Equal([]uint16{3, 2, 1, 1, 2, 3}, gommon.Compact([]uint16{3, 2, 1, 0, 1, 2, 3}))
	suite.Nil(gommon.Compact[uint16](nil))
}

func (suite *CompactSuite) TestUint32() {
	suite.Equal([]uint32{1, 2, 3}, gommon.Compact([]uint32{0, 1, 2, 3}))
	suite.Equal([]uint32{3, 2, 1}, gommon.Compact([]uint32{3, 2, 1, 0}))
	suite.Equal([]uint32{3, 2, 1, 1, 2, 3}, gommon.Compact([]uint32{3, 2, 1, 0, 1, 2, 3}))
	suite.Nil(gommon.Compact[uint32](nil))
}

func (suite *CompactSuite) TestUint64() {
	suite.Equal([]uint64{1, 2, 3}, gommon.Compact([]uint64{0, 1, 2, 3}))
	suite.Equal([]uint64{3, 2, 1}, gommon.Compact([]uint64{3, 2, 1, 0}))
	suite.Equal([]uint64{3, 2, 1, 1, 2, 3}, gommon.Compact([]uint64{3, 2, 1, 0, 1, 2, 3}))
	suite.Nil(gommon.Compact[uint64](nil))
}

func (suite *CompactSuite) TestUintPtr() {
	suite.Equal([]uintptr{1, 2, 3}, gommon.Compact([]uintptr{0, 1, 2, 3}))
	suite.Equal([]uintptr{3, 2, 1}, gommon.Compact([]uintptr{3, 2, 1, 0}))
	suite.Equal([]uintptr{3, 2, 1, 1, 2, 3}, gommon.Compact([]uintptr{3, 2, 1, 0, 1, 2, 3}))
	suite.Nil(gommon.Compact[uintptr](nil))
}

func TestDefault(t *testing.T) {
	t.Parallel()

	suite.Run(t, new(DefaultSuite))
}

type DefaultSuite struct {
	suite.Suite
}

func (suite *DefaultSuite) TestBytes() {
	suite.Equal(byte(0x79), gommon.Default(byte(0x79), byte(0x6e)))
	suite.Equal(byte(0x6e), gommon.Default(byte(0x00), byte(0x6e)))
}

func (suite *DefaultSuite) TestComplex64() {
	suite.Equal(complex64(1.9), gommon.Default(complex64(1.9), complex64(2.8)))
	suite.Equal(complex64(2.8), gommon.Default(complex64(0.0), complex64(2.8)))
}

func (suite *DefaultSuite) TestComplex128() {
	suite.Equal(complex128(1.9), gommon.Default(complex128(1.9), complex128(2.8)))
	suite.Equal(complex128(2.8), gommon.Default(complex128(0.0), complex128(2.8)))
}

func (suite *DefaultSuite) TestFloat32() {
	suite.Equal(float32(1.9), gommon.Default(float32(1.9), float32(2.8)))
	suite.Equal(float32(2.8), gommon.Default(float32(0.0), float32(2.8)))
}

func (suite *DefaultSuite) TestFloat64() {
	suite.Equal(1.9, gommon.Default(1.9, 2.8))
	suite.Equal(2.8, gommon.Default(0.0, 2.8))
}

func (suite *DefaultSuite) TestInt() {
	suite.Equal(1, gommon.Default(1, 2))
	suite.Equal(2, gommon.Default(0, 2))
}

func (suite *DefaultSuite) TestInt8() {
	suite.Equal(int8(1), gommon.Default(int8(1), int8(2)))
	suite.Equal(int8(2), gommon.Default(int8(0), int8(2)))
}

func (suite *DefaultSuite) TestInt16() {
	suite.Equal(int16(1), gommon.Default(int16(1), int16(2)))
	suite.Equal(int16(2), gommon.Default(int16(0), int16(2)))
}

func (suite *DefaultSuite) TestInt32() {
	suite.Equal(int32(1), gommon.Default(int32(1), int32(2)))
	suite.Equal(int32(2), gommon.Default(int32(0), int32(2)))
}

func (suite *DefaultSuite) TestInt64() {
	suite.Equal(int64(1), gommon.Default(int64(1), int64(2)))
	suite.Equal(int64(2), gommon.Default(int64(0), int64(2)))
}

func (suite *DefaultSuite) TestRunes() {
	suite.Equal(rune(0x79), gommon.Default(rune(0x79), rune(0x6e)))
	suite.Equal(rune(0x6e), gommon.Default(rune(0x00), rune(0x6e)))
}

func (suite *DefaultSuite) TestStrings() {
	suite.Equal("value1", gommon.Default("value1", "value2"))
	suite.Equal("value2", gommon.Default("", "value2"))
}

func (suite *DefaultSuite) TestUint() {
	suite.Equal(uint(1), gommon.Default(uint(1), uint(2)))
	suite.Equal(uint(2), gommon.Default(uint(0), uint(2)))
}

func (suite *DefaultSuite) TestUint8() {
	suite.Equal(uint8(1), gommon.Default(uint8(1), uint8(2)))
	suite.Equal(uint8(2), gommon.Default(uint8(0), uint8(2)))
}

func (suite *DefaultSuite) TestUint16() {
	suite.Equal(uint16(1), gommon.Default(uint16(1), uint16(2)))
	suite.Equal(uint16(2), gommon.Default(uint16(0), uint16(2)))
}

func (suite *DefaultSuite) TestUint32() {
	suite.Equal(uint32(1), gommon.Default(uint32(1), uint32(2)))
	suite.Equal(uint32(2), gommon.Default(uint32(0), uint32(2)))
}

func (suite *DefaultSuite) TestUint64() {
	suite.Equal(uint64(1), gommon.Default(uint64(1), uint64(2)))
	suite.Equal(uint64(2), gommon.Default(uint64(0), uint64(2)))
}

func (suite *DefaultSuite) TestUintPtr() {
	suite.Equal(uintptr(1), gommon.Default(uintptr(1), uintptr(2)))
	suite.Equal(uintptr(2), gommon.Default(uintptr(0), uintptr(2)))
}

func TestEmpty(t *testing.T) {
	t.Parallel()

	suite.Run(t, new(EmptySuite))
}

type EmptySuite struct {
	suite.Suite
}

func (suite *EmptySuite) TestEmpty_bytes() {
	suite.Equal(true, gommon.Empty(byte(0x00)))
	suite.Equal(false, gommon.Empty(byte(0x61)))
}

func (suite *EmptySuite) TestEmpty_complex64() {
	suite.Equal(true, gommon.Empty(complex64(0.0)))
	suite.Equal(false, gommon.Empty(complex64(1.9)))
}

func (suite *EmptySuite) TestEmpty_complex128() {
	suite.Equal(true, gommon.Empty(complex128(0.0)))
	suite.Equal(false, gommon.Empty(complex128(1.9)))
}

func (suite *EmptySuite) TestEmpty_float32() {
	suite.Equal(true, gommon.Empty(float32(0.0)))
	suite.Equal(false, gommon.Empty(float32(1.9)))
}

func (suite *EmptySuite) TestEmpty_float64() {
	suite.Equal(true, gommon.Empty(0.0))
	suite.Equal(false, gommon.Empty(1.9))
}

func (suite *EmptySuite) TestEmpty_int() {
	suite.Equal(true, gommon.Empty(0))
	suite.Equal(false, gommon.Empty(1))
}

func (suite *EmptySuite) TestEmpty_int8() {
	suite.Equal(true, gommon.Empty(int8(0)))
	suite.Equal(false, gommon.Empty(int8(1)))
}

func (suite *EmptySuite) TestEmpty_int16() {
	suite.Equal(true, gommon.Empty(int16(0)))
	suite.Equal(false, gommon.Empty(int16(1)))
}

func (suite *EmptySuite) TestEmpty_int32() {
	suite.Equal(true, gommon.Empty(int32(0)))
	suite.Equal(false, gommon.Empty(int32(1)))
}

func (suite *EmptySuite) TestEmpty_int64() {
	suite.Equal(true, gommon.Empty(int64(0)))
	suite.Equal(false, gommon.Empty(int64(1)))
}

func (suite *EmptySuite) TestEmpty_runes() {
	suite.Equal(true, gommon.Empty(rune(0x00)))
	suite.Equal(false, gommon.Empty(rune(0x61)))
}

func (suite *EmptySuite) TestEmpty_strings() {
	suite.Equal(true, gommon.Empty(""))
	suite.Equal(false, gommon.Empty("value"))
}

func (suite *EmptySuite) TestEmpty_uint() {
	suite.Equal(true, gommon.Empty(uint(0)))
	suite.Equal(false, gommon.Empty(uint(1)))
}

func (suite *EmptySuite) TestEmpty_uint8() {
	suite.Equal(true, gommon.Empty(uint8(0)))
	suite.Equal(false, gommon.Empty(uint8(1)))
}

func (suite *EmptySuite) TestEmpty_uint16() {
	suite.Equal(true, gommon.Empty(uint16(0)))
	suite.Equal(false, gommon.Empty(uint16(1)))
}

func (suite *EmptySuite) TestEmpty_uint32() {
	suite.Equal(true, gommon.Empty(uint32(0)))
	suite.Equal(false, gommon.Empty(uint32(1)))
}

func (suite *EmptySuite) TestEmpty_uint64() {
	suite.Equal(true, gommon.Empty(uint64(0)))
	suite.Equal(false, gommon.Empty(uint64(1)))
}

func (suite *EmptySuite) TestEmpty_uintPtr() {
	suite.Equal(true, gommon.Empty(uintptr(0)))
	suite.Equal(false, gommon.Empty(uintptr(1)))
}

func TestFirst(t *testing.T) {
	t.Parallel()

	suite.Run(t, new(FirstSuite))
}

type FirstSuite struct {
	suite.Suite
}

func (suite *FirstSuite) TestString() {
	suite.Equal("one", gommon.First([]string{"one", "two"}))
	suite.Equal("", gommon.First([]string{}))
	suite.Nil(gommon.First([]*string{}))
}

func TestHas(t *testing.T) {
	t.Parallel()

	suite.Run(t, new(HasSuite))
}

type HasSuite struct {
	suite.Suite
}

func (suite *HasSuite) TestBytes() {
	suite.True(gommon.Has(byte(0x62), byte(0x61), byte(0x62), byte(0x63), byte(0x64)))
	suite.False(gommon.Has(byte(0x65), byte(0x61), byte(0x62), byte(0x63), byte(0x64)))
	suite.False(gommon.Has(byte(0)))
}

func (suite *HasSuite) TestComplex64() {
	suite.True(gommon.Has(complex64(2.8), complex64(1.9), complex64(2.8), complex64(3.7)))
	suite.False(gommon.Has(complex64(5), complex64(1.9), complex64(2.8), complex64(3.7)))
	suite.False(gommon.Has(complex64(0)))
}

func (suite *HasSuite) TestComplex128() {
	suite.True(gommon.Has(complex128(2.8), complex128(1.9), complex128(2.8), complex128(3.7)))
	suite.False(gommon.Has(complex128(5), complex128(1.9), complex128(2.8), complex128(3.7)))
	suite.False(gommon.Has(complex128(0)))
}

func (suite *HasSuite) TestFloat32() {
	suite.True(gommon.Has(float32(2.8), float32(1.9), float32(2.8), float32(3.7)))
	suite.False(gommon.Has(float32(5), float32(1.9), float32(2.8), float32(3.7)))
	suite.False(gommon.Has(float32(0)))
}

func (suite *HasSuite) TestFloat64() {
	suite.True(gommon.Has(2.8, 1.9, 2.8, 3.7))
	suite.False(gommon.Has(float64(5), 1.9, 2.8, 3.7))
	suite.False(gommon.Has(float64(0)))
}

func (suite *HasSuite) TestInt() {
	suite.True(gommon.Has(2, 1, 2, 3))
	suite.False(gommon.Has(5, 1, 2, 3))
	suite.False(gommon.Has(0))
}

func (suite *HasSuite) TestInt8() {
	suite.True(gommon.Has(int8(2), int8(1), int8(2), int8(3)))
	suite.False(gommon.Has(int8(5), int8(1), int8(2), int8(3)))
	suite.False(gommon.Has(int8(0)))
}

func (suite *HasSuite) TestInt16() {
	suite.True(gommon.Has(int16(2), int16(1), int16(2), int16(3)))
	suite.False(gommon.Has(int16(5), int16(1), int16(2), int16(3)))
	suite.False(gommon.Has(int16(0)))
}

func (suite *HasSuite) TestInt32() {
	suite.True(gommon.Has(int32(2), int32(1), int32(2), int32(3)))
	suite.False(gommon.Has(int32(5), int32(1), int32(2), int32(3)))
	suite.False(gommon.Has(int32(0)))
}

func (suite *HasSuite) TestInt64() {
	suite.True(gommon.Has(int64(2), int64(1), int64(2), int64(3)))
	suite.False(gommon.Has(int64(5), int64(1), int64(2), int64(3)))
	suite.False(gommon.Has(int64(0)))
}

func (suite *HasSuite) TestRunes() {
	suite.True(gommon.Has(rune(0x62), rune(0x61), rune(0x62), rune(0x63), rune(0x64)))
	suite.False(gommon.Has(rune(0x65), rune(0x61), rune(0x62), rune(0x63), rune(0x64)))
	suite.False(gommon.Has(rune(0)))
}

func (suite *HasSuite) TestStrings() {
	suite.True(gommon.Has("value2", "value1", "value2", "value3"))
	suite.False(gommon.Has("value5", "value1", "value2", "value3"))
	suite.False(gommon.Has("value"))
}

func (suite *HasSuite) TestUint() {
	suite.True(gommon.Has(uint(2), uint(1), uint(2), uint(3)))
	suite.False(gommon.Has(uint(5), uint(1), uint(2), uint(3)))
	suite.False(gommon.Has(uint(0)))
}

func (suite *HasSuite) TestUint8() {
	suite.True(gommon.Has(uint8(2), uint8(1), uint8(2), uint8(3)))
	suite.False(gommon.Has(uint8(5), uint8(1), uint8(2), uint8(3)))
	suite.False(gommon.Has(uint8(0)))
}

func (suite *HasSuite) TestUint16() {
	suite.True(gommon.Has(uint16(2), uint16(1), uint16(2), uint16(3)))
	suite.False(gommon.Has(uint16(5), uint16(1), uint16(2), uint16(3)))
	suite.False(gommon.Has(uint16(0)))
}

func (suite *HasSuite) TestUint32() {
	suite.True(gommon.Has(uint32(2), uint32(1), uint32(2), uint32(3)))
	suite.False(gommon.Has(uint32(5), uint32(1), uint32(2), uint32(3)))
	suite.False(gommon.Has(uint32(0)))
}

func (suite *HasSuite) TestUint64() {
	suite.True(gommon.Has(uint64(2), uint64(1), uint64(2), uint64(3)))
	suite.False(gommon.Has(uint64(5), uint64(1), uint64(2), uint64(3)))
	suite.False(gommon.Has(uint64(0)))
}

func (suite *HasSuite) TestUintPtr() {
	suite.True(gommon.Has(uintptr(2), uintptr(1), uintptr(2), uintptr(3)))
	suite.False(gommon.Has(uintptr(5), uintptr(1), uintptr(2), uintptr(3)))
	suite.False(gommon.Has(uintptr(0)))
}

func TestMustParse(t *testing.T) {
	t.Parallel()

	suite.Run(t, new(MustParseSuite))
}

type MustParseSuite struct {
	suite.Suite
}

func (suite *MustParseSuite) TestUnknown() {
	type unknown struct{}

	suite.Equal(unknown{}, gommon.MustParse[unknown]("error"))
}

func (suite *MustParseSuite) TestBool() {
	suite.Equal(false, gommon.MustParse[bool]("error"))
	suite.Equal(true, gommon.MustParse[bool]("true"))
	suite.Equal(false, gommon.MustParse[bool]("false"))
}

func (suite *MustParseSuite) TestComplex64() {
	suite.Equal(complex64(0), gommon.MustParse[complex64]("error"))
	suite.Equal(complex64(123.987), gommon.MustParse[complex64]("123.987"))
	suite.Equal(complex64(-987.123), gommon.MustParse[complex64]("-987.123"))
}

func (suite *MustParseSuite) TestComplex128() {
	suite.Equal(complex128(0), gommon.MustParse[complex128]("error"))
	suite.Equal(complex128(123.987), gommon.MustParse[complex128]("123.987"))
	suite.Equal(complex128(-987.123), gommon.MustParse[complex128]("-987.123"))
}

func (suite *MustParseSuite) TestFloat32() {
	suite.Equal(float32(0), gommon.MustParse[float32]("error"))
	suite.Equal(float32(123.987), gommon.MustParse[float32]("123.987"))
	suite.Equal(float32(-987.123), gommon.MustParse[float32]("-987.123"))
}

func (suite *MustParseSuite) TestFloat64() {
	suite.Equal(float64(0), gommon.MustParse[float64]("error"))
	suite.Equal(123.987, gommon.MustParse[float64]("123.987"))
	suite.Equal(-987.123, gommon.MustParse[float64]("-987.123"))
}

func (suite *MustParseSuite) TestInt() {
	suite.Equal(0, gommon.MustParse[int]("error"))
	suite.Equal(9223372036854775807, gommon.MustParse[int]("9223372036854775807"))
	suite.Equal(-9223372036854775808, gommon.MustParse[int]("-9223372036854775808"))
}

func (suite *MustParseSuite) TestInt8() {
	suite.Equal(int8(0), gommon.MustParse[int8]("error"))
	suite.Equal(int8(127), gommon.MustParse[int8]("127"))
	suite.Equal(int8(-128), gommon.MustParse[int8]("-128"))
}

func (suite *MustParseSuite) TestInt16() {
	suite.Equal(int16(0), gommon.MustParse[int16]("error"))
	suite.Equal(int16(32767), gommon.MustParse[int16]("32767"))
	suite.Equal(int16(-32768), gommon.MustParse[int16]("-32768"))
}

func (suite *MustParseSuite) TestInt32() {
	suite.Equal(int32(0), gommon.MustParse[int32]("error"))
	suite.Equal(int32(2147483647), gommon.MustParse[int32]("2147483647"))
	suite.Equal(int32(-2147483648), gommon.MustParse[int32]("-2147483648"))
}

func (suite *MustParseSuite) TestInt64() {
	suite.Equal(int64(0), gommon.MustParse[int64]("error"))
	suite.Equal(int64(9223372036854775807), gommon.MustParse[int64]("9223372036854775807"))
	suite.Equal(int64(-9223372036854775808), gommon.MustParse[int64]("-9223372036854775808"))
}

func (suite *MustParseSuite) TestStrings() {
	suite.Equal("ok", gommon.MustParse[string]("ok"))
}

func (suite *MustParseSuite) TestUint() {
	suite.Equal(uint(0), gommon.MustParse[uint]("error"))
	suite.Equal(uint(4294967295), gommon.MustParse[uint]("4294967295"))
	suite.Equal(uint(0), gommon.MustParse[uint]("0"))
}

func (suite *MustParseSuite) TestUint8() {
	suite.Equal(uint8(0), gommon.MustParse[uint8]("error"))
	suite.Equal(uint8(255), gommon.MustParse[uint8]("255"))
	suite.Equal(uint8(0), gommon.MustParse[uint8]("0"))
}

func (suite *MustParseSuite) TestUint16() {
	suite.Equal(uint16(0), gommon.MustParse[uint16]("error"))
	suite.Equal(uint16(65535), gommon.MustParse[uint16]("65535"))
	suite.Equal(uint16(0), gommon.MustParse[uint16]("0"))
}

func (suite *MustParseSuite) TestUint32() {
	suite.Equal(uint32(0), gommon.MustParse[uint32]("error"))
	suite.Equal(uint32(4294967295), gommon.MustParse[uint32]("4294967295"))
	suite.Equal(uint32(0), gommon.MustParse[uint32]("0"))
}

func (suite *MustParseSuite) TestUint64() {
	suite.Equal(uint64(0), gommon.MustParse[uint64]("error"))
	suite.Equal(uint64(18446744073709551615), gommon.MustParse[uint64]("18446744073709551615"))
	suite.Equal(uint64(0), gommon.MustParse[uint64]("0"))
}

func (suite *MustParseSuite) TestUintPtr() {
	suite.Equal(uintptr(0x0), gommon.MustParse[uintptr]("error"))
	suite.Equal(uintptr(0x1a), gommon.MustParse[uintptr]("1a"))
	suite.Equal(uintptr(0x0), gommon.MustParse[uintptr]("0"))
}

func (suite *MustParseSuite) TestDuration() {
	suite.Equal(time.Duration(0), gommon.MustParse[time.Duration]("error"))
	suite.Equal(time.Second*30, gommon.MustParse[time.Duration]("30s"))
	suite.Equal(time.Minute*5+time.Second*45, gommon.MustParse[time.Duration]("5m45s"))
}

func (suite *MustParseSuite) TestTime() {
	suite.Equal(time.Time{}, gommon.MustParse[time.Time]("error"))
	suite.Equal(time.Date(2006, 1, 2, 15, 4, 5, 0, time.UTC), gommon.MustParse[time.Time]("2006-01-02T15:04:05Z"))
}

func (suite *MustParseSuite) TestURL() {
	suite.Nil(gommon.MustParse[*url.URL]("://user:abc"))

	v := gommon.MustParse[*url.URL]("http://example.domain.tld:8080/path?query=value")
	suite.Equal("http", v.Scheme)
	suite.Equal("example.domain.tld:8080", v.Host)
	suite.Equal("/path", v.Path)
	suite.Equal("query=value", v.RawQuery)
}

func (suite *MustParseSuite) TestValues() {
	suite.Equal(url.Values(nil), gommon.MustParse[url.Values]("key;value"))
	suite.Equal(url.Values{"key": {"value"}}, gommon.MustParse[url.Values]("key=value"))
}

func (suite *MustParseSuite) TestBase64() {
	suite.Equal(gommon.Base64(nil), gommon.MustParse[gommon.Base64]("SGVsbG8"))
	suite.Equal(gommon.Base64("Hello"), gommon.MustParse[gommon.Base64]("SGVsbG8="))
}

func (suite *MustParseSuite) TestDSN() {
	suite.Equal(gommon.DSN{}, gommon.MustParse[gommon.DSN]("invalid:/unknown"))

	v := gommon.MustParse[gommon.DSN]("https://user:pass@tcp(domain.tld:1234)/path?key=value")
	suite.Equal("https", v.Scheme)
	suite.Equal("user", v.Username)
	suite.Equal("pass", v.Password)
	suite.Equal("tcp", v.Protocol)
	suite.Equal("domain.tld", v.Endpoint)
	suite.Equal(1234, v.Port)
	suite.Equal("path", v.Path)
	suite.Equal(url.Values{"key": {"value"}}, v.Queries)
}

func (suite *MustParseSuite) TestTimestamp() {
	v1 := gommon.MustParse[gommon.Timestamp]("error")
	suite.Equal(gommon.Timestamp(0), v1)
	suite.Equal(time.Date(1970, 1, 1, 0, 0, 0, 0, time.UTC), v1.Time().UTC())

	v2 := gommon.MustParse[gommon.Timestamp]("1136214305")
	suite.Equal(gommon.Timestamp(1136214305), v2)
	suite.Equal(time.Date(2006, 1, 2, 15, 5, 5, 0, time.UTC), v2.Time().UTC())
}

func TestParse(t *testing.T) {
	t.Parallel()

	suite.Run(t, new(ParseSuite))
}

type ParseSuite struct {
	suite.Suite
}

func (suite *ParseSuite) TestUnknown() {
	type unknown struct{}

	v1, err := gommon.Parse[unknown]("error")
	suite.Error(err)
	suite.Equal(unknown{}, v1)
}

func (suite *ParseSuite) TestBool() {
	v1, err := gommon.Parse[bool]("error")
	suite.Error(err)
	suite.Equal(false, v1)

	v2, err := gommon.Parse[bool]("true")
	suite.NoError(err)
	suite.Equal(true, v2)

	v3, err := gommon.Parse[bool]("false")
	suite.NoError(err)
	suite.Equal(false, v3)
}

func (suite *ParseSuite) TestComplex64() {
	v1, err := gommon.Parse[complex64]("error")
	suite.Error(err)
	suite.Equal(complex64(0), v1)

	v2, err := gommon.Parse[complex64]("123.987")
	suite.NoError(err)
	suite.Equal(complex64(123.987), v2)

	v3, err := gommon.Parse[complex64]("-987.123")
	suite.NoError(err)
	suite.Equal(complex64(-987.123), v3)
}

func (suite *ParseSuite) TestComplex128() {
	v1, err := gommon.Parse[complex128]("error")
	suite.Error(err)
	suite.Equal(complex128(0), v1)

	v2, err := gommon.Parse[complex128]("123.987")
	suite.NoError(err)
	suite.Equal(complex128(123.987), v2)

	v3, err := gommon.Parse[complex128]("-987.123")
	suite.NoError(err)
	suite.Equal(complex128(-987.123), v3)
}

func (suite *ParseSuite) TestFloat32() {
	v1, err := gommon.Parse[float32]("error")
	suite.Error(err)
	suite.Equal(float32(0), v1)

	v2, err := gommon.Parse[float32]("123.987")
	suite.NoError(err)
	suite.Equal(float32(123.987), v2)

	v3, err := gommon.Parse[float32]("-987.123")
	suite.NoError(err)
	suite.Equal(float32(-987.123), v3)
}

func (suite *ParseSuite) TestFloat64() {
	v1, err := gommon.Parse[float64]("error")
	suite.Error(err)
	suite.Equal(float64(0), v1)

	v2, err := gommon.Parse[float64]("123.987")
	suite.NoError(err)
	suite.Equal(123.987, v2)

	v3, err := gommon.Parse[float64]("-987.123")
	suite.NoError(err)
	suite.Equal(-987.123, v3)
}

func (suite *ParseSuite) TestInt() {
	v1, err := gommon.Parse[int]("error")
	suite.Error(err)
	suite.Equal(0, v1)

	v2, err := gommon.Parse[int]("9223372036854775807")
	suite.NoError(err)
	suite.Equal(9223372036854775807, v2)

	v3, err := gommon.Parse[int]("-9223372036854775808")
	suite.NoError(err)
	suite.Equal(-9223372036854775808, v3)
}

func (suite *ParseSuite) TestInt8() {
	v1, err := gommon.Parse[int8]("error")
	suite.Error(err)
	suite.Equal(int8(0), v1)

	v2, err := gommon.Parse[int8]("127")
	suite.NoError(err)
	suite.Equal(int8(127), v2)

	v3, err := gommon.Parse[int8]("-128")
	suite.NoError(err)
	suite.Equal(int8(-128), v3)
}

func (suite *ParseSuite) TestInt16() {
	v1, err := gommon.Parse[int16]("error")
	suite.Error(err)
	suite.Equal(int16(0), v1)

	v2, err := gommon.Parse[int16]("32767")
	suite.NoError(err)
	suite.Equal(int16(32767), v2)

	v3, err := gommon.Parse[int16]("-32768")
	suite.NoError(err)
	suite.Equal(int16(-32768), v3)
}

func (suite *ParseSuite) TestInt32() {
	v1, err := gommon.Parse[int32]("error")
	suite.Error(err)
	suite.Equal(int32(0), v1)

	v2, err := gommon.Parse[int32]("2147483647")
	suite.NoError(err)
	suite.Equal(int32(2147483647), v2)

	v3, err := gommon.Parse[int32]("-2147483648")
	suite.NoError(err)
	suite.Equal(int32(-2147483648), v3)
}

func (suite *ParseSuite) TestInt64() {
	v1, err := gommon.Parse[int64]("error")
	suite.Error(err)
	suite.Equal(int64(0), v1)

	v2, err := gommon.Parse[int64]("9223372036854775807")
	suite.NoError(err)
	suite.Equal(int64(9223372036854775807), v2)

	v3, err := gommon.Parse[int64]("-9223372036854775808")
	suite.NoError(err)
	suite.Equal(int64(-9223372036854775808), v3)
}

func (suite *ParseSuite) TestStrings() {
	v1, err := gommon.Parse[string]("ok")
	suite.NoError(err)
	suite.Equal("ok", v1)
}

func (suite *ParseSuite) TestUint() {
	v1, err := gommon.Parse[uint]("error")
	suite.Error(err)
	suite.Equal(uint(0), v1)

	v2, err := gommon.Parse[uint]("4294967295")
	suite.NoError(err)
	suite.Equal(uint(4294967295), v2)

	v3, err := gommon.Parse[uint]("0")
	suite.NoError(err)
	suite.Equal(uint(0), v3)
}

func (suite *ParseSuite) TestUint8() {
	v1, err := gommon.Parse[uint8]("error")
	suite.Error(err)
	suite.Equal(uint8(0), v1)

	v2, err := gommon.Parse[uint8]("255")
	suite.NoError(err)
	suite.Equal(uint8(255), v2)

	v3, err := gommon.Parse[uint8]("0")
	suite.NoError(err)
	suite.Equal(uint8(0), v3)
}

func (suite *ParseSuite) TestUint16() {
	v1, err := gommon.Parse[uint16]("error")
	suite.Error(err)
	suite.Equal(uint16(0), v1)

	v2, err := gommon.Parse[uint16]("65535")
	suite.NoError(err)
	suite.Equal(uint16(65535), v2)

	v3, err := gommon.Parse[uint16]("0")
	suite.NoError(err)
	suite.Equal(uint16(0), v3)
}

func (suite *ParseSuite) TestUint32() {
	v1, err := gommon.Parse[uint32]("error")
	suite.Error(err)
	suite.Equal(uint32(0), v1)

	v2, err := gommon.Parse[uint32]("4294967295")
	suite.NoError(err)
	suite.Equal(uint32(4294967295), v2)

	v3, err := gommon.Parse[uint32]("0")
	suite.NoError(err)
	suite.Equal(uint32(0), v3)
}

func (suite *ParseSuite) TestUint64() {
	v1, err := gommon.Parse[uint64]("error")
	suite.Error(err)
	suite.Equal(uint64(0), v1)

	v2, err := gommon.Parse[uint64]("18446744073709551615")
	suite.NoError(err)
	suite.Equal(uint64(18446744073709551615), v2)

	v3, err := gommon.Parse[uint64]("0")
	suite.NoError(err)
	suite.Equal(uint64(0), v3)
}

func (suite *ParseSuite) TestUintPtr() {
	v1, err := gommon.Parse[uintptr]("error")
	suite.Error(err)
	suite.Equal(uintptr(0x0), v1)

	v2, err := gommon.Parse[uintptr]("1a")
	suite.NoError(err)
	suite.Equal(uintptr(0x1a), v2)

	v3, err := gommon.Parse[uintptr]("0")
	suite.NoError(err)
	suite.Equal(uintptr(0x0), v3)
}

func (suite *ParseSuite) TestDuration() {
	v1, err := gommon.Parse[time.Duration]("error")
	suite.Error(err)
	suite.Equal(time.Duration(0), v1)

	v2, err := gommon.Parse[time.Duration]("30s")
	suite.NoError(err)
	suite.Equal(time.Second*30, v2)

	v3, err := gommon.Parse[time.Duration]("5m45s")
	suite.NoError(err)
	suite.Equal(time.Minute*5+time.Second*45, v3)
}

func (suite *ParseSuite) TestTime() {
	v1, err := gommon.Parse[time.Time]("error")
	suite.Error(err)
	suite.Equal(time.Time{}, v1)

	v2, err := gommon.Parse[time.Time]("2006-01-02T15:04:05Z")
	suite.NoError(err)
	suite.Equal(time.Date(2006, 1, 2, 15, 4, 5, 0, time.UTC), v2)
}

func (suite *ParseSuite) TestTimestamp() {
	v1, err := gommon.Parse[gommon.Timestamp]("error")
	suite.Error(err)
	suite.Equal(gommon.Timestamp(0), v1)
	suite.Equal(time.Date(1970, 1, 1, 0, 0, 0, 0, time.UTC), v1.Time().UTC())

	v2, err := gommon.Parse[gommon.Timestamp]("1136214305")
	suite.NoError(err)
	suite.Equal(gommon.Timestamp(1136214305), v2)
	suite.Equal(time.Date(2006, 1, 2, 15, 5, 5, 0, time.UTC), v2.Time().UTC())
}

func TestPtr(t *testing.T) {
	t.Parallel()

	suite.Run(t, new(PtrSuite))
}

type PtrSuite struct {
	suite.Suite
}

func (suite *PtrSuite) TestBool() {
	test1 := gommon.Ptr(true)
	suite.IsType((*bool)(nil), test1)
	suite.Equal(true, *test1)

	test2 := gommon.Ptr(false)
	suite.IsType((*bool)(nil), test2)
	suite.Equal(false, *test2)
}

func (suite *PtrSuite) TestByte() {
	test1 := gommon.Ptr(byte(0x61))
	suite.IsType((*uint8)(nil), test1)
	suite.Equal(uint8(97), *test1)

	test2 := gommon.Ptr(byte(0))
	suite.IsType((*uint8)(nil), test2)
	suite.Equal(uint8(0), *test2)
}

func (suite *PtrSuite) TestComplex64() {
	test1 := gommon.Ptr(complex64(123.987))
	suite.IsType((*complex64)(nil), test1)
	suite.Equal(complex64(123.987), *test1)

	test2 := gommon.Ptr(complex64(0.123))
	suite.IsType((*complex64)(nil), test2)
	suite.Equal(complex64(0.123), *test2)
}

func (suite *PtrSuite) TestComplex128() {
	test1 := gommon.Ptr(complex128(123.987))
	suite.IsType((*complex128)(nil), test1)
	suite.Equal(complex128(123.987), *test1)

	test2 := gommon.Ptr(complex128(0.123))
	suite.IsType((*complex128)(nil), test2)
	suite.Equal(complex128(0.123), *test2)
}

func (suite *PtrSuite) TestFloat32() {
	test1 := gommon.Ptr(float32(123.987))
	suite.IsType((*float32)(nil), test1)
	suite.Equal(float32(123.987), *test1)

	test2 := gommon.Ptr(float32(0.123))
	suite.IsType((*float32)(nil), test2)
	suite.Equal(float32(0.123), *test2)
}

func (suite *PtrSuite) TestFloat64() {
	test1 := gommon.Ptr(123.987)
	suite.IsType((*float64)(nil), test1)
	suite.Equal(123.987, *test1)

	test2 := gommon.Ptr(0.123)
	suite.IsType((*float64)(nil), test2)
	suite.Equal(0.123, *test2)
}

func (suite *PtrSuite) TestInt() {
	test1 := gommon.Ptr(1)
	suite.IsType((*int)(nil), test1)
	suite.Equal(1, *test1)

	test2 := gommon.Ptr(0)
	suite.IsType((*int)(nil), test2)
	suite.Equal(0, *test2)
}

func (suite *PtrSuite) TestInt8() {
	test1 := gommon.Ptr(int8(1))
	suite.IsType((*int8)(nil), test1)
	suite.Equal(int8(1), *test1)

	test2 := gommon.Ptr(int8(0))
	suite.IsType((*int8)(nil), test2)
	suite.Equal(int8(0), *test2)
}

func (suite *PtrSuite) TestInt16() {
	test1 := gommon.Ptr(int16(1))
	suite.IsType((*int16)(nil), test1)
	suite.Equal(int16(1), *test1)

	test2 := gommon.Ptr(int16(0))
	suite.IsType((*int16)(nil), test2)
	suite.Equal(int16(0), *test2)
}

func (suite *PtrSuite) TestInt32() {
	test1 := gommon.Ptr(int32(1))
	suite.IsType((*int32)(nil), test1)
	suite.Equal(int32(1), *test1)

	test2 := gommon.Ptr(int32(0))
	suite.IsType((*int32)(nil), test2)
	suite.Equal(int32(0), *test2)
}

func (suite *PtrSuite) TestInt64() {
	test1 := gommon.Ptr(int64(1))
	suite.IsType((*int64)(nil), test1)
	suite.Equal(int64(1), *test1)

	test2 := gommon.Ptr(int64(0))
	suite.IsType((*int64)(nil), test2)
	suite.Equal(int64(0), *test2)
}

func (suite *PtrSuite) TestRune() {
	test1 := gommon.Ptr(rune(0x61))
	suite.IsType((*int32)(nil), test1)
	suite.Equal(int32(97), *test1)

	test2 := gommon.Ptr(rune(0))
	suite.IsType((*int32)(nil), test2)
	suite.Equal(int32(0), *test2)
}

func (suite *PtrSuite) TestString() {
	test1 := gommon.Ptr("test")
	suite.IsType((*string)(nil), test1)
	suite.Equal("test", *test1)

	test2 := gommon.Ptr("")
	suite.IsType((*string)(nil), test2)
	suite.Equal("", *test2)
}

func (suite *PtrSuite) TestUint() {
	test1 := gommon.Ptr(uint(1))
	suite.IsType((*uint)(nil), test1)
	suite.Equal(uint(1), *test1)

	test2 := gommon.Ptr(uint(0))
	suite.IsType((*uint)(nil), test2)
	suite.Equal(uint(0), *test2)
}

func (suite *PtrSuite) TestUint8() {
	test1 := gommon.Ptr(uint8(1))
	suite.IsType((*uint8)(nil), test1)
	suite.Equal(uint8(1), *test1)

	test2 := gommon.Ptr(uint8(0))
	suite.IsType((*uint8)(nil), test2)
	suite.Equal(uint8(0), *test2)
}

func (suite *PtrSuite) TestUint16() {
	test1 := gommon.Ptr(uint16(1))
	suite.IsType((*uint16)(nil), test1)
	suite.Equal(uint16(1), *test1)

	test2 := gommon.Ptr(uint16(0))
	suite.IsType((*uint16)(nil), test2)
	suite.Equal(uint16(0), *test2)
}

func (suite *PtrSuite) TestUint32() {
	test1 := gommon.Ptr(uint32(1))
	suite.IsType((*uint32)(nil), test1)
	suite.Equal(uint32(1), *test1)

	test2 := gommon.Ptr(uint32(0))
	suite.IsType((*uint32)(nil), test2)
	suite.Equal(uint32(0), *test2)
}

func (suite *PtrSuite) TestUint64() {
	test1 := gommon.Ptr(uint64(1))
	suite.IsType((*uint64)(nil), test1)
	suite.Equal(uint64(1), *test1)

	test2 := gommon.Ptr(uint64(0))
	suite.IsType((*uint64)(nil), test2)
	suite.Equal(uint64(0), *test2)
}

func (suite *PtrSuite) TestUintPtr() {
	test1 := gommon.Ptr(uintptr(1))
	suite.IsType((*uintptr)(nil), test1)
	suite.Equal(uintptr(1), *test1)

	test2 := gommon.Ptr(uintptr(0))
	suite.IsType((*uintptr)(nil), test2)
	suite.Equal(uintptr(0), *test2)
}

func TestUnPtr(t *testing.T) {
	t.Parallel()

	suite.Run(t, new(UnPtrSuite))
}

type UnPtrSuite struct {
	suite.Suite
}

func (suite *UnPtrSuite) TestBool() {
	test1 := gommon.UnPtr(gommon.Ptr(true))
	suite.Equal(true, test1)

	test2 := gommon.UnPtr(gommon.Ptr(false))
	suite.Equal(false, test2)
}

func (suite *UnPtrSuite) TestByte() {
	test1 := gommon.UnPtr(gommon.Ptr(byte(0x61)))
	suite.Equal(uint8(97), test1)

	test2 := gommon.UnPtr(gommon.Ptr(byte(0)))
	suite.Equal(uint8(0), test2)
}

func (suite *UnPtrSuite) TestComplex64() {
	test1 := gommon.UnPtr(gommon.Ptr(complex64(123.987)))
	suite.Equal(complex64(123.987), test1)

	test2 := gommon.UnPtr(gommon.Ptr(complex64(0.123)))
	suite.Equal(complex64(0.123), test2)
}

func (suite *UnPtrSuite) TestComplex128() {
	test1 := gommon.UnPtr(gommon.Ptr(complex128(123.987)))
	suite.Equal(complex128(123.987), test1)

	test2 := gommon.UnPtr(gommon.Ptr(complex128(0.123)))
	suite.Equal(complex128(0.123), test2)
}

func (suite *UnPtrSuite) TestFloat32() {
	test1 := gommon.UnPtr(gommon.Ptr(float32(123.987)))
	suite.Equal(float32(123.987), test1)

	test2 := gommon.UnPtr(gommon.Ptr(float32(0.123)))
	suite.Equal(float32(0.123), test2)
}

func (suite *UnPtrSuite) TestFloat64() {
	test1 := gommon.UnPtr(gommon.Ptr(123.987))
	suite.Equal(123.987, test1)

	test2 := gommon.UnPtr(gommon.Ptr(0.123))
	suite.Equal(0.123, test2)
}

func (suite *UnPtrSuite) TestInt() {
	test1 := gommon.UnPtr(gommon.Ptr(1))
	suite.Equal(1, test1)

	test2 := gommon.UnPtr(gommon.Ptr(0))
	suite.Equal(0, test2)
}

func (suite *UnPtrSuite) TestInt8() {
	test1 := gommon.UnPtr(gommon.Ptr(int8(1)))
	suite.Equal(int8(1), test1)

	test2 := gommon.UnPtr(gommon.Ptr(int8(0)))
	suite.Equal(int8(0), test2)
}

func (suite *UnPtrSuite) TestInt16() {
	test1 := gommon.UnPtr(gommon.Ptr(int16(1)))
	suite.Equal(int16(1), test1)

	test2 := gommon.UnPtr(gommon.Ptr(int16(0)))
	suite.Equal(int16(0), test2)
}

func (suite *UnPtrSuite) TestInt32() {
	test1 := gommon.UnPtr(gommon.Ptr(int32(1)))
	suite.Equal(int32(1), test1)

	test2 := gommon.UnPtr(gommon.Ptr(int32(0)))
	suite.Equal(int32(0), test2)
}

func (suite *UnPtrSuite) TestInt64() {
	test1 := gommon.UnPtr(gommon.Ptr(int64(1)))
	suite.Equal(int64(1), test1)

	test2 := gommon.UnPtr(gommon.Ptr(int64(0)))
	suite.Equal(int64(0), test2)
}

func (suite *UnPtrSuite) TestRune() {
	test1 := gommon.UnPtr(gommon.Ptr(rune(0x61)))
	suite.Equal(int32(97), test1)

	test2 := gommon.UnPtr(gommon.Ptr(rune(0)))
	suite.Equal(int32(0), test2)
}

func (suite *UnPtrSuite) TestString() {
	test1 := gommon.UnPtr(gommon.Ptr("test"))
	suite.Equal("test", test1)

	test2 := gommon.UnPtr(gommon.Ptr(""))
	suite.Equal("", test2)
}

func (suite *UnPtrSuite) TestUint() {
	test1 := gommon.UnPtr(gommon.Ptr(uint(1)))
	suite.Equal(uint(1), test1)

	test2 := gommon.UnPtr(gommon.Ptr(uint(0)))
	suite.Equal(uint(0), test2)
}

func (suite *UnPtrSuite) TestUint8() {
	test1 := gommon.UnPtr(gommon.Ptr(uint8(1)))
	suite.Equal(uint8(1), test1)

	test2 := gommon.UnPtr(gommon.Ptr(uint8(0)))
	suite.Equal(uint8(0), test2)
}

func (suite *UnPtrSuite) TestUint16() {
	test1 := gommon.UnPtr(gommon.Ptr(uint16(1)))
	suite.Equal(uint16(1), test1)

	test2 := gommon.UnPtr(gommon.Ptr(uint16(0)))
	suite.Equal(uint16(0), test2)
}

func (suite *UnPtrSuite) TestUint32() {
	test1 := gommon.UnPtr(gommon.Ptr(uint32(1)))
	suite.Equal(uint32(1), test1)

	test2 := gommon.UnPtr(gommon.Ptr(uint32(0)))
	suite.Equal(uint32(0), test2)
}

func (suite *UnPtrSuite) TestUint64() {
	test1 := gommon.UnPtr(gommon.Ptr(uint64(1)))
	suite.Equal(uint64(1), test1)

	test2 := gommon.UnPtr(gommon.Ptr(uint64(0)))
	suite.Equal(uint64(0), test2)
}

func (suite *UnPtrSuite) TestUintPtr() {
	test1 := gommon.UnPtr(gommon.Ptr(uintptr(1)))
	suite.Equal(uintptr(1), test1)

	test2 := gommon.UnPtr(gommon.Ptr(uintptr(0)))
	suite.Equal(uintptr(0), test2)
}

func TestSplit(t *testing.T) {
	t.Parallel()

	suite.Run(t, new(SplitSuite))
}

type SplitSuite struct {
	suite.Suite
}

func (suite *SplitSuite) TestUnknown() {
	type unknown struct{}

	suite.Equal([]unknown{}, gommon.Split[unknown]("error,unknown", ","))
}

func (suite *SplitSuite) TestBool() {
	suite.Equal([]bool{true, false}, gommon.Split[bool]("true,,false", ","))
}

func (suite *SplitSuite) TestComplex64() {
	// Valid
	suite.Equal([]complex64{123.987, 0, -987.123}, gommon.Split[complex64]("123.987,0,-987.123", ","))

	// Invalid
	suite.Equal([]complex64{}, gommon.Split[complex64]("error,invalid", ","))
}

func (suite *SplitSuite) TestComplex128() {
	// Valid
	suite.Equal([]complex128{123.987, 0, -987.123}, gommon.Split[complex128]("123.987,0,-987.123", ","))

	// Invalid
	suite.Equal([]complex128{}, gommon.Split[complex128]("error,invalid", ","))
}

func (suite *SplitSuite) TestFloat32() {
	// Valid
	suite.Equal([]float32{123.987, 0, -987.123}, gommon.Split[float32]("123.987,0,-987.123", ","))

	// Invalid
	suite.Equal([]float32{}, gommon.Split[float32]("error,invalid", ","))
}

func (suite *SplitSuite) TestFloat64() {
	// Valid
	suite.Equal([]float64{123.987, 0, -987.123}, gommon.Split[float64]("123.987,0,-987.123", ","))

	// Invalid
	suite.Equal([]float64{}, gommon.Split[float64]("error,invalid", ","))
}

func (suite *SplitSuite) TestInt() {
	// Valid
	suite.Equal([]int{9223372036854775807, 0, -9223372036854775808}, gommon.Split[int]("9223372036854775807,0,-9223372036854775808", ","))

	// Invalid
	suite.Equal([]int{}, gommon.Split[int]("error,invalid", ","))
}

func (suite *SplitSuite) TestInt8() {
	// Valid
	suite.Equal([]int8{127, 0, -128}, gommon.Split[int8]("127,0,-128", ","))

	// Invalid
	suite.Equal([]int8{}, gommon.Split[int8]("error,invalid", ","))
}

func (suite *SplitSuite) TestInt16() {
	// Valid
	suite.Equal([]int16{32767, 0, -32768}, gommon.Split[int16]("32767,0,-32768", ","))

	// Invalid
	suite.Equal([]int16{}, gommon.Split[int16]("error,invalid", ","))
}

func (suite *SplitSuite) TestInt32() {
	// Valid
	suite.Equal([]int32{2147483647, 0, -2147483648}, gommon.Split[int32]("2147483647,0,-2147483648", ","))

	// Invalid
	suite.Equal([]int32{}, gommon.Split[int32]("error,invalid", ","))
}

func (suite *SplitSuite) TestInt64() {
	// Valid
	suite.Equal([]int64{9223372036854775807, 0, -9223372036854775808}, gommon.Split[int64]("9223372036854775807,0,-9223372036854775808", ","))

	// Invalid
	suite.Equal([]int64{}, gommon.Split[int64]("error,invalid", ","))
}

func (suite *SplitSuite) TestStrings() {
	suite.Equal([]string{"value1", "value2", "", "value3"}, gommon.Split[string]("value1,value2,,value3", ","))
}

func (suite *SplitSuite) TestUint() {
	// Valid
	suite.Equal([]uint{4294967295, 0}, gommon.Split[uint]("4294967295,0", ","))

	// Invalid
	suite.Equal([]uint{}, gommon.Split[uint]("error,invalid", ","))
}

func (suite *SplitSuite) TestUint8() {
	// Valid
	suite.Equal([]uint8{255, 0}, gommon.Split[uint8]("255,0", ","))

	// Invalid
	suite.Equal([]uint8{}, gommon.Split[uint8]("error,invalid", ","))
}

func (suite *SplitSuite) TestUint16() {
	// Valid
	suite.Equal([]uint16{65535, 0}, gommon.Split[uint16]("65535,0", ","))

	// Invalid
	suite.Equal([]uint16{}, gommon.Split[uint16]("error,invalid", ","))
}

func (suite *SplitSuite) TestUint32() {
	// Valid
	suite.Equal([]uint32{4294967295, 0}, gommon.Split[uint32]("4294967295,0", ","))

	// Invalid
	suite.Equal([]uint32{}, gommon.Split[uint32]("error,invalid", ","))
}

func (suite *SplitSuite) TestUint64() {
	// Valid
	suite.Equal([]uint64{18446744073709551615, 0}, gommon.Split[uint64]("18446744073709551615,0", ","))

	// Invalid
	suite.Equal([]uint64{}, gommon.Split[uint64]("error,invalid", ","))
}

func TestSplitSlice(t *testing.T) {
	t.Parallel()

	suite.Run(t, new(SplitSliceSuite))
}

type SplitSliceSuite struct {
	suite.Suite
}

func (suite *SplitSliceSuite) TestUnknown() {
	type unknown struct{}

	suite.Equal([]unknown{}, gommon.SplitSlice[unknown]([]string{"error,unknown"}, ","))
}

func (suite *SplitSliceSuite) TestBool() {
	suite.Equal([]bool{true, false, false, true}, gommon.SplitSlice[bool]([]string{"true,,false", "false,true"}, ","))
}

func (suite *SplitSliceSuite) TestComplex64() {
	// Valid
	suite.Equal([]complex64{123.98, 0, -987.12}, gommon.SplitSlice[complex64]([]string{"123.98,0", "-987.12"}, ","))

	// Invalid
	suite.Equal([]complex64{}, gommon.SplitSlice[complex64]([]string{"error,", "invalid"}, ","))
}

func (suite *SplitSliceSuite) TestComplex128() {
	// Valid
	suite.Equal([]complex128{123.98, 0, -987.12}, gommon.SplitSlice[complex128]([]string{"123.98,0", "-987.12"}, ","))

	// Invalid
	suite.Equal([]complex128{}, gommon.SplitSlice[complex128]([]string{"error,", "invalid"}, ","))
}

func (suite *SplitSliceSuite) TestFloat32() {
	// Valid
	suite.Equal([]float32{123.987, 0, -987.123}, gommon.SplitSlice[float32]([]string{"123.987,0", "-987.123"}, ","))

	// Invalid
	suite.Equal([]float32{}, gommon.SplitSlice[float32]([]string{"error,", "invalid"}, ","))
}

func (suite *SplitSliceSuite) TestFloat64() {
	// Valid
	suite.Equal([]float64{123.987, 0, -987.123}, gommon.SplitSlice[float64]([]string{"123.987,0", "-987.123"}, ","))

	// Invalid
	suite.Equal([]float64{}, gommon.SplitSlice[float64]([]string{"error,", "invalid"}, ","))
}

func (suite *SplitSliceSuite) TestInt() {
	// Valid
	suite.Equal([]int{9223372036854775807, 0, -9223372036854775808}, gommon.SplitSlice[int]([]string{"9223372036854775807,0", "-9223372036854775808"}, ","))

	// Invalid
	suite.Equal([]int{}, gommon.SplitSlice[int]([]string{"error,", "invalid"}, ","))
}

func (suite *SplitSliceSuite) TestInt8() {
	// Valid
	suite.Equal([]int8{127, 0, -128}, gommon.SplitSlice[int8]([]string{"127,0", "-128"}, ","))

	// Invalid
	suite.Equal([]int8{}, gommon.SplitSlice[int8]([]string{"error,", "invalid"}, ","))
}

func (suite *SplitSliceSuite) TestInt16() {
	// Valid
	suite.Equal([]int16{32767, 0, -32768}, gommon.SplitSlice[int16]([]string{"32767,0", "-32768"}, ","))

	// Invalid
	suite.Equal([]int16{}, gommon.SplitSlice[int16]([]string{"error,", "invalid"}, ","))
}

func (suite *SplitSliceSuite) TestInt32() {
	// Valid
	suite.Equal([]int32{2147483647, 0, -2147483648}, gommon.SplitSlice[int32]([]string{"2147483647,0", "-2147483648"}, ","))

	// Invalid
	suite.Equal([]int32{}, gommon.SplitSlice[int32]([]string{"error,", "invalid"}, ","))
}

func (suite *SplitSliceSuite) TestInt64() {
	// Valid
	suite.Equal([]int64{9223372036854775807, 0, -9223372036854775808}, gommon.SplitSlice[int64]([]string{"9223372036854775807,0", "-9223372036854775808"}, ","))

	// Invalid
	suite.Equal([]int64{}, gommon.SplitSlice[int64]([]string{"error,", "invalid"}, ","))
}

func (suite *SplitSliceSuite) TestStrings() {
	suite.Equal([]string{"value1", "value2", "", "value3"}, gommon.SplitSlice[string]([]string{"value1,value2,", "value3"}, ","))
}

func (suite *SplitSliceSuite) TestUint() {
	// Valid
	suite.Equal([]uint{4294967295, 0, 1}, gommon.SplitSlice[uint]([]string{"4294967295,0", "1"}, ","))

	// Invalid
	suite.Equal([]uint{}, gommon.SplitSlice[uint]([]string{"error,", "invalid"}, ","))
}

func (suite *SplitSliceSuite) TestUint8() {
	// Valid
	suite.Equal([]uint8{255, 0, 1}, gommon.SplitSlice[uint8]([]string{"255,0", "1"}, ","))

	// Invalid
	suite.Equal([]uint8{}, gommon.SplitSlice[uint8]([]string{"error,", "invalid"}, ","))
}

func (suite *SplitSliceSuite) TestUint16() {
	// Valid
	suite.Equal([]uint16{65535, 0, 1}, gommon.SplitSlice[uint16]([]string{"65535,0", "1"}, ","))

	// Invalid
	suite.Equal([]uint16{}, gommon.SplitSlice[uint16]([]string{"error,", "invalid"}, ","))
}

func (suite *SplitSliceSuite) TestUint32() {
	// Valid
	suite.Equal([]uint32{4294967295, 0, 1}, gommon.SplitSlice[uint32]([]string{"4294967295,0", "1"}, ","))

	// Invalid
	suite.Equal([]uint32{}, gommon.SplitSlice[uint32]([]string{"error,", "invalid"}, ","))
}

func (suite *SplitSliceSuite) TestUint64() {
	// Valid
	suite.Equal([]uint64{18446744073709551615, 0, 1}, gommon.SplitSlice[uint64]([]string{"18446744073709551615,0", "1"}, ","))

	// Invalid
	suite.Equal([]uint64{}, gommon.SplitSlice[uint64]([]string{"error,", "invalid"}, ","))
}

func TestTernary(t *testing.T) {
	t.Parallel()

	suite.Run(t, new(TernarySuite))
}

type TernarySuite struct {
	suite.Suite
}

func (suite *TernarySuite) TestBytes() {
	suite.Equal(byte(0x79), gommon.Ternary(true, byte(0x79), byte(0x6e)))
	suite.Equal(byte(0x6e), gommon.Ternary(false, byte(0x79), byte(0x6e)))
}

func (suite *TernarySuite) TestComplex64() {
	suite.Equal(complex64(1.9), gommon.Ternary(true, complex64(1.9), complex64(2.8)))
	suite.Equal(complex64(2.8), gommon.Ternary(false, complex64(1.9), complex64(2.8)))
}

func (suite *TernarySuite) TestComplex128() {
	suite.Equal(complex128(1.9), gommon.Ternary(true, complex128(1.9), complex128(2.8)))
	suite.Equal(complex128(2.8), gommon.Ternary(false, complex128(1.9), complex128(2.8)))
}

func (suite *TernarySuite) TestFloat32() {
	suite.Equal(float32(1.9), gommon.Ternary(true, float32(1.9), float32(2.8)))
	suite.Equal(float32(2.8), gommon.Ternary(false, float32(1.9), float32(2.8)))
}

func (suite *TernarySuite) TestFloat64() {
	suite.Equal(float64(1.9), gommon.Ternary(true, float64(1.9), float64(2.8)))
	suite.Equal(float64(2.8), gommon.Ternary(false, float64(1.9), float64(2.8)))
}

func (suite *TernarySuite) TestInt() {
	suite.Equal(1, gommon.Ternary(true, 1, 2))
	suite.Equal(2, gommon.Ternary(false, 1, 2))
}

func (suite *TernarySuite) TestInt8() {
	suite.Equal(int8(1), gommon.Ternary(true, int8(1), int8(2)))
	suite.Equal(int8(2), gommon.Ternary(false, int8(1), int8(2)))
}

func (suite *TernarySuite) TestInt16() {
	suite.Equal(int16(1), gommon.Ternary(true, int16(1), int16(2)))
	suite.Equal(int16(2), gommon.Ternary(false, int16(1), int16(2)))
}

func (suite *TernarySuite) TestInt32() {
	suite.Equal(int32(1), gommon.Ternary(true, int32(1), int32(2)))
	suite.Equal(int32(2), gommon.Ternary(false, int32(1), int32(2)))
}

func (suite *TernarySuite) TestInt64() {
	suite.Equal(int64(1), gommon.Ternary(true, int64(1), int64(2)))
	suite.Equal(int64(2), gommon.Ternary(false, int64(1), int64(2)))
}

func (suite *TernarySuite) TestRunes() {
	suite.Equal(rune(0x79), gommon.Ternary(true, rune(0x79), rune(0x6e)))
	suite.Equal(rune(0x6e), gommon.Ternary(false, rune(0x79), rune(0x6e)))
}

func (suite *TernarySuite) TestStrings() {
	suite.Equal("True", gommon.Ternary(true, "True", "False"))
	suite.Equal("False", gommon.Ternary(false, "True", "False"))
}

func (suite *TernarySuite) TestUint() {
	suite.Equal(uint(1), gommon.Ternary(true, uint(1), uint(2)))
	suite.Equal(uint(2), gommon.Ternary(false, uint(1), uint(2)))
}

func (suite *TernarySuite) TestUint8() {
	suite.Equal(uint8(1), gommon.Ternary(true, uint8(1), uint8(2)))
	suite.Equal(uint8(2), gommon.Ternary(false, uint8(1), uint8(2)))
}

func (suite *TernarySuite) TestUint16() {
	suite.Equal(uint16(1), gommon.Ternary(true, uint16(1), uint16(2)))
	suite.Equal(uint16(2), gommon.Ternary(false, uint16(1), uint16(2)))
}

func (suite *TernarySuite) TestUint32() {
	suite.Equal(uint32(1), gommon.Ternary(true, uint32(1), uint32(2)))
	suite.Equal(uint32(2), gommon.Ternary(false, uint32(1), uint32(2)))
}

func (suite *TernarySuite) TestUint64() {
	suite.Equal(uint64(1), gommon.Ternary(true, uint64(1), uint64(2)))
	suite.Equal(uint64(2), gommon.Ternary(false, uint64(1), uint64(2)))
}

func (suite *TernarySuite) TestUintPtr() {
	suite.Equal(uintptr(1), gommon.Ternary(true, uintptr(1), uintptr(2)))
	suite.Equal(uintptr(2), gommon.Ternary(false, uintptr(1), uintptr(2)))
}

func TestUniq(t *testing.T) {
	t.Parallel()

	suite.Run(t, new(UniqSuite))
}

type UniqSuite struct {
	suite.Suite
}

func (suite *UniqSuite) TestBytes() {
	suite.Equal([]byte{0x61, 0x62, 0x63}, gommon.Uniq([]byte{0x61, 0x62, 0x61, 0x63}))
}

func (suite *UniqSuite) TestComplex64() {
	suite.Equal([]complex64{1.9, 2.8, 3.7}, gommon.Uniq([]complex64{1.9, 2.8, 1.9, 3.7}))
}

func (suite *UniqSuite) TestComplex128() {
	suite.Equal([]complex128{1.9, 2.8, 3.7}, gommon.Uniq([]complex128{1.9, 2.8, 1.9, 3.7}))
}

func (suite *UniqSuite) TestFloat32() {
	suite.Equal([]float32{1.9, 2.8, 3.7}, gommon.Uniq([]float32{1.9, 2.8, 1.9, 3.7}))
}

func (suite *UniqSuite) TestFloat64() {
	suite.Equal([]float64{1.9, 2.8, 3.7}, gommon.Uniq([]float64{1.9, 2.8, 1.9, 3.7}))
}

func (suite *UniqSuite) TestInt() {
	suite.Equal([]int{1, 2, 3}, gommon.Uniq([]int{1, 2, 1, 3}))
}

func (suite *UniqSuite) TestInt8() {
	suite.Equal([]int8{1, 2, 3}, gommon.Uniq([]int8{1, 2, 1, 3}))
}

func (suite *UniqSuite) TestInt16() {
	suite.Equal([]int16{1, 2, 3}, gommon.Uniq([]int16{1, 2, 1, 3}))
}

func (suite *UniqSuite) TestInt32() {
	suite.Equal([]int32{1, 2, 3}, gommon.Uniq([]int32{1, 2, 1, 3}))
}

func (suite *UniqSuite) TestInt64() {
	suite.Equal([]int64{1, 2, 3}, gommon.Uniq([]int64{1, 2, 1, 3}))
}

func (suite *UniqSuite) TestRunes() {
	suite.Equal([]rune{0x61, 0x62, 0x63}, gommon.Uniq([]rune{0x61, 0x62, 0x61, 0x63}))
}

func (suite *UniqSuite) TestStrings() {
	suite.Equal([]string{"v1", "v2", "v3"}, gommon.Uniq([]string{"v1", "v2", "v1", "v3"}))
}

func (suite *UniqSuite) TestUint() {
	suite.Equal([]uint{1, 2, 3}, gommon.Uniq([]uint{1, 2, 1, 3}))
}

func (suite *UniqSuite) TestUint8() {
	suite.Equal([]uint8{1, 2, 3}, gommon.Uniq([]uint8{1, 2, 1, 3}))
}

func (suite *UniqSuite) TestUint16() {
	suite.Equal([]uint16{1, 2, 3}, gommon.Uniq([]uint16{1, 2, 1, 3}))
}

func (suite *UniqSuite) TestUint32() {
	suite.Equal([]uint32{1, 2, 3}, gommon.Uniq([]uint32{1, 2, 1, 3}))
}

func (suite *UniqSuite) TestUint64() {
	suite.Equal([]uint64{1, 2, 3}, gommon.Uniq([]uint64{1, 2, 1, 3}))
}

func (suite *UniqSuite) TestUintPtr() {
	suite.Equal([]uintptr{1, 2, 3}, gommon.Uniq([]uintptr{1, 2, 1, 3}))
}

func TestWrapParse(t *testing.T) {
	t.Parallel()

	suite.Run(t, new(WrapParseSuite))
}

type WrapParseSuite struct {
	suite.Suite
}

func (suite *WrapParseSuite) TestUnknown() {
	type unknown struct{}

	v1, err := gommon.WrapParse[unknown]("error")
	suite.Error(err)
	suite.Equal(unknown{}, v1)
}

func (suite *WrapParseSuite) TestBool() {
	v1, err := gommon.WrapParse[bool]("error")
	suite.Error(err)
	suite.Equal(false, v1)

	v2, err := gommon.WrapParse[bool]("true")
	suite.NoError(err)
	suite.Equal(true, v2)

	v3, err := gommon.WrapParse[bool]("false")
	suite.NoError(err)
	suite.Equal(false, v3)
}

func (suite *WrapParseSuite) TestComplex64() {
	v1, err := gommon.WrapParse[complex64]("error")
	suite.Error(err)
	suite.Equal(complex64(0), v1)

	v2, err := gommon.WrapParse[complex64]("123.987")
	suite.NoError(err)
	suite.Equal(complex64(123.987), v2)

	v3, err := gommon.WrapParse[complex64]("-987.123")
	suite.NoError(err)
	suite.Equal(complex64(-987.123), v3)
}

func (suite *WrapParseSuite) TestComplex128() {
	v1, err := gommon.WrapParse[complex128]("error")
	suite.Error(err)
	suite.Equal(complex128(0), v1)

	v2, err := gommon.WrapParse[complex128]("123.987")
	suite.NoError(err)
	suite.Equal(complex128(123.987), v2)

	v3, err := gommon.WrapParse[complex128]("-987.123")
	suite.NoError(err)
	suite.Equal(complex128(-987.123), v3)
}

func (suite *WrapParseSuite) TestFloat32() {
	v1, err := gommon.WrapParse[float32]("error")
	suite.Error(err)
	suite.Equal(float32(0), v1)

	v2, err := gommon.WrapParse[float32]("123.987")
	suite.NoError(err)
	suite.Equal(float32(123.987), v2)

	v3, err := gommon.WrapParse[float32]("-987.123")
	suite.NoError(err)
	suite.Equal(float32(-987.123), v3)
}

func (suite *WrapParseSuite) TestFloat64() {
	v1, err := gommon.WrapParse[float64]("error")
	suite.Error(err)
	suite.Equal(float64(0), v1)

	v2, err := gommon.WrapParse[float64]("123.987")
	suite.NoError(err)
	suite.Equal(123.987, v2)

	v3, err := gommon.WrapParse[float64]("-987.123")
	suite.NoError(err)
	suite.Equal(-987.123, v3)
}

func (suite *WrapParseSuite) TestInt() {
	v1, err := gommon.WrapParse[int]("error")
	suite.Error(err)
	suite.Equal(0, v1)

	v2, err := gommon.WrapParse[int]("9223372036854775807")
	suite.NoError(err)
	suite.Equal(9223372036854775807, v2)

	v3, err := gommon.WrapParse[int]("-9223372036854775808")
	suite.NoError(err)
	suite.Equal(-9223372036854775808, v3)
}

func (suite *WrapParseSuite) TestInt8() {
	v1, err := gommon.WrapParse[int8]("error")
	suite.Error(err)
	suite.Equal(int8(0), v1)

	v2, err := gommon.WrapParse[int8]("127")
	suite.NoError(err)
	suite.Equal(int8(127), v2)

	v3, err := gommon.WrapParse[int8]("-128")
	suite.NoError(err)
	suite.Equal(int8(-128), v3)
}

func (suite *WrapParseSuite) TestInt16() {
	v1, err := gommon.WrapParse[int16]("error")
	suite.Error(err)
	suite.Equal(int16(0), v1)

	v2, err := gommon.WrapParse[int16]("32767")
	suite.NoError(err)
	suite.Equal(int16(32767), v2)

	v3, err := gommon.WrapParse[int16]("-32768")
	suite.NoError(err)
	suite.Equal(int16(-32768), v3)
}

func (suite *WrapParseSuite) TestInt32() {
	v1, err := gommon.WrapParse[int32]("error")
	suite.Error(err)
	suite.Equal(int32(0), v1)

	v2, err := gommon.WrapParse[int32]("2147483647")
	suite.NoError(err)
	suite.Equal(int32(2147483647), v2)

	v3, err := gommon.WrapParse[int32]("-2147483648")
	suite.NoError(err)
	suite.Equal(int32(-2147483648), v3)
}

func (suite *WrapParseSuite) TestInt64() {
	v1, err := gommon.WrapParse[int64]("error")
	suite.Error(err)
	suite.Equal(int64(0), v1)

	v2, err := gommon.WrapParse[int64]("9223372036854775807")
	suite.NoError(err)
	suite.Equal(int64(9223372036854775807), v2)

	v3, err := gommon.WrapParse[int64]("-9223372036854775808")
	suite.NoError(err)
	suite.Equal(int64(-9223372036854775808), v3)
}

func (suite *WrapParseSuite) TestStrings() {
	v1, err := gommon.WrapParse[string]("ok")
	suite.NoError(err)
	suite.Equal("ok", v1)
}

func (suite *WrapParseSuite) TestUint() {
	v1, err := gommon.WrapParse[uint]("error")
	suite.Error(err)
	suite.Equal(uint(0), v1)

	v2, err := gommon.WrapParse[uint]("4294967295")
	suite.NoError(err)
	suite.Equal(uint(4294967295), v2)

	v3, err := gommon.WrapParse[uint]("0")
	suite.NoError(err)
	suite.Equal(uint(0), v3)
}

func (suite *WrapParseSuite) TestUint8() {
	v1, err := gommon.WrapParse[uint8]("error")
	suite.Error(err)
	suite.Equal(uint8(0), v1)

	v2, err := gommon.WrapParse[uint8]("255")
	suite.NoError(err)
	suite.Equal(uint8(255), v2)

	v3, err := gommon.WrapParse[uint8]("0")
	suite.NoError(err)
	suite.Equal(uint8(0), v3)
}

func (suite *WrapParseSuite) TestUint16() {
	v1, err := gommon.WrapParse[uint16]("error")
	suite.Error(err)
	suite.Equal(uint16(0), v1)

	v2, err := gommon.WrapParse[uint16]("65535")
	suite.NoError(err)
	suite.Equal(uint16(65535), v2)

	v3, err := gommon.WrapParse[uint16]("0")
	suite.NoError(err)
	suite.Equal(uint16(0), v3)
}

func (suite *WrapParseSuite) TestUint32() {
	v1, err := gommon.WrapParse[uint32]("error")
	suite.Error(err)
	suite.Equal(uint32(0), v1)

	v2, err := gommon.WrapParse[uint32]("4294967295")
	suite.NoError(err)
	suite.Equal(uint32(4294967295), v2)

	v3, err := gommon.WrapParse[uint32]("0")
	suite.NoError(err)
	suite.Equal(uint32(0), v3)
}

func (suite *WrapParseSuite) TestUint64() {
	v1, err := gommon.WrapParse[uint64]("error")
	suite.Error(err)
	suite.Equal(uint64(0), v1)

	v2, err := gommon.WrapParse[uint64]("18446744073709551615")
	suite.NoError(err)
	suite.Equal(uint64(18446744073709551615), v2)

	v3, err := gommon.WrapParse[uint64]("0")
	suite.NoError(err)
	suite.Equal(uint64(0), v3)
}

func (suite *WrapParseSuite) TestUintPtr() {
	v1, err := gommon.WrapParse[uintptr]("error")
	suite.Error(err)
	suite.Equal(uintptr(0x0), v1)

	v2, err := gommon.WrapParse[uintptr]("1a")
	suite.NoError(err)
	suite.Equal(uintptr(0x1a), v2)

	v3, err := gommon.WrapParse[uintptr]("0")
	suite.NoError(err)
	suite.Equal(uintptr(0x0), v3)
}

func (suite *WrapParseSuite) TestDuration() {
	v1, err := gommon.WrapParse[time.Duration]("error")
	suite.Error(err)
	suite.Equal(time.Duration(0), v1)

	v2, err := gommon.WrapParse[time.Duration]("30s")
	suite.NoError(err)
	suite.Equal(time.Second*30, v2)

	v3, err := gommon.WrapParse[time.Duration]("5m45s")
	suite.NoError(err)
	suite.Equal(time.Minute*5+time.Second*45, v3)
}

func (suite *WrapParseSuite) TestTime() {
	v1, err := gommon.WrapParse[time.Time]("error")
	suite.Error(err)
	suite.Equal(time.Time{}, v1)

	v2, err := gommon.WrapParse[time.Time]("2006-01-02T15:04:05Z")
	suite.NoError(err)
	suite.Equal(time.Date(2006, 1, 2, 15, 4, 5, 0, time.UTC), v2)
}

func (suite *WrapParseSuite) TestTimestamp() {
	v1, err := gommon.WrapParse[gommon.Timestamp]("error")
	suite.Error(err)
	suite.Equal(gommon.Timestamp(0), v1)
	suite.Equal(time.Date(1970, 1, 1, 0, 0, 0, 0, time.UTC), v1.(gommon.Timestamp).Time().UTC())

	v2, err := gommon.WrapParse[gommon.Timestamp]("1136214305")
	suite.NoError(err)
	suite.Equal(gommon.Timestamp(1136214305), v2)
	suite.Equal(time.Date(2006, 1, 2, 15, 5, 5, 0, time.UTC), v2.(gommon.Timestamp).Time().UTC())
}

func TestZero(t *testing.T) {
	t.Parallel()

	suite.Run(t, new(ZeroSuite))
}

type ZeroSuite struct {
	suite.Suite
}

func (suite *ZeroSuite) TestString() {
	suite.Equal("", gommon.Zero[string]())
	suite.Nil(gommon.Zero[*string]())
}

func TestConvert(t *testing.T) {
	t.Parallel()

	suite.Run(t, new(ConvertSuite))
}

type ConvertSuite struct{ suite.Suite }

func (suite *ConvertSuite) TestAny() {
	for _, value := range []any{
		struct{}{},
		"test",
		"1",
		false,
		true,
		-1,
		0,
		1,
		int8(-1),
		int8(0),
		int8(1),
		int16(-1),
		int16(0),
		int16(1),
		int32(-1),
		int32(0),
		int32(1),
		int64(-1),
		int64(0),
		int64(1),
		uint(0),
		uint(1),
		uint8(0),
		uint8(1),
		uint16(0),
		uint16(1),
		uint32(0),
		uint32(1),
		uint64(0),
		uint64(1),
		uintptr(0),
		uintptr(1),
		float32(0),
		float32(1),
		float64(0),
		float64(1),
		complex64(0),
		complex64(1),
		complex128(0),
		complex128(1),
	} {
		suite.Run(fmt.Sprintf("%T(%v)", value, value), func() {
			suite.Equalf(value, gommon.Convert(value).Interface(), "value=%#v", value)
		})
	}
}

func (suite *ConvertSuite) TestToString() {
	type StringType string

	var (
		stringAny  any        = "test"
		stringType StringType = "test"
	)

	for value, res := range map[any]gommon.Value[string]{
		stringAny:     {Value: "test", Valid: true},
		stringType:    {Value: "test", Valid: true},
		struct{}{}:    {Value: "", Valid: false},
		"test":        {Value: "test", Valid: true},
		"1":           {Value: "1", Valid: true},
		false:         {Value: "false", Valid: true},
		true:          {Value: "true", Valid: true},
		-1:            {Value: "-1", Valid: true},
		0:             {Value: "0", Valid: true},
		1:             {Value: "1", Valid: true},
		int8(-1):      {Value: "-1", Valid: true},
		int8(0):       {Value: "0", Valid: true},
		int8(1):       {Value: "1", Valid: true},
		int16(-1):     {Value: "-1", Valid: true},
		int16(0):      {Value: "0", Valid: true},
		int16(1):      {Value: "1", Valid: true},
		int32(-1):     {Value: "-1", Valid: true},
		int32(0):      {Value: "0", Valid: true},
		int32(1):      {Value: "1", Valid: true},
		int64(-1):     {Value: "-1", Valid: true},
		int64(0):      {Value: "0", Valid: true},
		int64(1):      {Value: "1", Valid: true},
		uint(0):       {Value: "0", Valid: true},
		uint(1):       {Value: "1", Valid: true},
		uint8(0):      {Value: "0", Valid: true},
		uint8(1):      {Value: "1", Valid: true},
		uint16(0):     {Value: "0", Valid: true},
		uint16(1):     {Value: "1", Valid: true},
		uint32(0):     {Value: "0", Valid: true},
		uint32(1):     {Value: "1", Valid: true},
		uint64(0):     {Value: "0", Valid: true},
		uint64(1):     {Value: "1", Valid: true},
		uintptr(0):    {Value: "0", Valid: true},
		uintptr(1):    {Value: "1", Valid: true},
		float32(0):    {Value: "0", Valid: true},
		float32(1):    {Value: "1", Valid: true},
		float64(0):    {Value: "0", Valid: true},
		float64(1):    {Value: "1", Valid: true},
		complex64(0):  {Value: "(0+0i)", Valid: true},
		complex64(1):  {Value: "(1+0i)", Valid: true},
		complex128(0): {Value: "(0+0i)", Valid: true},
		complex128(1): {Value: "(1+0i)", Valid: true},
	} {
		suite.Run(fmt.Sprintf("%T(%v)", value, value), func() {
			test := gommon.Convert(value).ToString()
			suite.Equalf(res.Valid, test.Valid, "value=%#v res=%#v", value, res)
			suite.Equalf(res.Value, test.Value, "value=%#v res=%#v", value, res)
		})
	}
}

func (suite *ConvertSuite) TestToBool() {
	for value, res := range map[any]gommon.Value[bool]{
		struct{}{}:    {Value: false, Valid: false},
		"test":        {Value: false, Valid: false},
		"1":           {Value: true, Valid: true},
		false:         {Value: false, Valid: true},
		true:          {Value: true, Valid: true},
		-1:            {Value: false, Valid: false},
		0:             {Value: false, Valid: true},
		1:             {Value: true, Valid: true},
		int8(-1):      {Value: false, Valid: false},
		int8(0):       {Value: false, Valid: true},
		int8(1):       {Value: true, Valid: true},
		int16(-1):     {Value: false, Valid: false},
		int16(0):      {Value: false, Valid: true},
		int16(1):      {Value: true, Valid: true},
		int32(-1):     {Value: false, Valid: false},
		int32(0):      {Value: false, Valid: true},
		int32(1):      {Value: true, Valid: true},
		int64(-1):     {Value: false, Valid: false},
		int64(0):      {Value: false, Valid: true},
		int64(1):      {Value: true, Valid: true},
		uint(0):       {Value: false, Valid: true},
		uint(1):       {Value: true, Valid: true},
		uint8(0):      {Value: false, Valid: true},
		uint8(1):      {Value: true, Valid: true},
		uint16(0):     {Value: false, Valid: true},
		uint16(1):     {Value: true, Valid: true},
		uint32(0):     {Value: false, Valid: true},
		uint32(1):     {Value: true, Valid: true},
		uint64(0):     {Value: false, Valid: true},
		uint64(1):     {Value: true, Valid: true},
		uintptr(0):    {Value: false, Valid: false},
		uintptr(1):    {Value: false, Valid: false},
		float32(0):    {Value: false, Valid: false},
		float32(1):    {Value: false, Valid: false},
		float64(0):    {Value: false, Valid: false},
		float64(1):    {Value: false, Valid: false},
		complex64(0):  {Value: false, Valid: false},
		complex64(1):  {Value: false, Valid: false},
		complex128(0): {Value: false, Valid: false},
		complex128(1): {Value: false, Valid: false},
	} {
		suite.Run(fmt.Sprintf("%T(%v)", value, value), func() {
			test := gommon.Convert(value).ToBool()
			suite.Equalf(res.Valid, test.Valid, "value=%#v res=%#v", value, res)
			suite.Equalf(res.Value, test.Value, "value=%#v res=%#v", value, res)
		})
	}
}

func (suite *ConvertSuite) TestToInt() {
	for value, res := range map[any]gommon.Value[int]{
		struct{}{}:    {Value: 0, Valid: false},
		"test":        {Value: 0, Valid: false},
		"1":           {Value: 1, Valid: true},
		false:         {Value: 0, Valid: true},
		true:          {Value: 1, Valid: true},
		-1:            {Value: -1, Valid: true},
		0:             {Value: 0, Valid: true},
		1:             {Value: 1, Valid: true},
		int8(-1):      {Value: -1, Valid: true},
		int8(0):       {Value: 0, Valid: true},
		int8(1):       {Value: 1, Valid: true},
		int16(-1):     {Value: -1, Valid: true},
		int16(0):      {Value: 0, Valid: true},
		int16(1):      {Value: 1, Valid: true},
		int32(-1):     {Value: -1, Valid: true},
		int32(0):      {Value: 0, Valid: true},
		int32(1):      {Value: 1, Valid: true},
		int64(-1):     {Value: -1, Valid: true},
		int64(0):      {Value: 0, Valid: true},
		int64(1):      {Value: 1, Valid: true},
		uint(0):       {Value: 0, Valid: true},
		uint(1):       {Value: 1, Valid: true},
		uint8(0):      {Value: 0, Valid: true},
		uint8(1):      {Value: 1, Valid: true},
		uint16(0):     {Value: 0, Valid: true},
		uint16(1):     {Value: 1, Valid: true},
		uint32(0):     {Value: 0, Valid: true},
		uint32(1):     {Value: 1, Valid: true},
		uint64(0):     {Value: 0, Valid: true},
		uint64(1):     {Value: 1, Valid: true},
		uintptr(0):    {Value: 0, Valid: true},
		uintptr(1):    {Value: 1, Valid: true},
		float32(0):    {Value: 0, Valid: true},
		float32(1):    {Value: 1, Valid: true},
		float64(0):    {Value: 0, Valid: true},
		float64(1):    {Value: 1, Valid: true},
		complex64(0):  {Value: 0, Valid: false},
		complex64(1):  {Value: 0, Valid: false},
		complex128(0): {Value: 0, Valid: false},
		complex128(1): {Value: 0, Valid: false},
	} {
		suite.Run(fmt.Sprintf("%T(%v)", value, value), func() {
			test := gommon.Convert(value).ToInt()
			suite.Equalf(res.Valid, test.Valid, "value=%#v res=%#v", value, res)
			suite.Equalf(res.Value, test.Value, "value=%#v res=%#v", value, res)
		})
	}
}

func (suite *ConvertSuite) TestToInt8() {
	for value, res := range map[any]gommon.Value[int8]{
		struct{}{}:    {Value: 0, Valid: false},
		"test":        {Value: 0, Valid: false},
		"1":           {Value: 1, Valid: true},
		false:         {Value: 0, Valid: true},
		true:          {Value: 1, Valid: true},
		-1:            {Value: -1, Valid: true},
		0:             {Value: 0, Valid: true},
		1:             {Value: 1, Valid: true},
		int8(-1):      {Value: -1, Valid: true},
		int8(0):       {Value: 0, Valid: true},
		int8(1):       {Value: 1, Valid: true},
		int16(-1):     {Value: -1, Valid: true},
		int16(0):      {Value: 0, Valid: true},
		int16(1):      {Value: 1, Valid: true},
		int32(-1):     {Value: -1, Valid: true},
		int32(0):      {Value: 0, Valid: true},
		int32(1):      {Value: 1, Valid: true},
		int64(-1):     {Value: -1, Valid: true},
		int64(0):      {Value: 0, Valid: true},
		int64(1):      {Value: 1, Valid: true},
		uint(0):       {Value: 0, Valid: true},
		uint(1):       {Value: 1, Valid: true},
		uint8(0):      {Value: 0, Valid: true},
		uint8(1):      {Value: 1, Valid: true},
		uint16(0):     {Value: 0, Valid: true},
		uint16(1):     {Value: 1, Valid: true},
		uint32(0):     {Value: 0, Valid: true},
		uint32(1):     {Value: 1, Valid: true},
		uint64(0):     {Value: 0, Valid: true},
		uint64(1):     {Value: 1, Valid: true},
		uintptr(0):    {Value: 0, Valid: true},
		uintptr(1):    {Value: 1, Valid: true},
		float32(0):    {Value: 0, Valid: true},
		float32(1):    {Value: 1, Valid: true},
		float64(0):    {Value: 0, Valid: true},
		float64(1):    {Value: 1, Valid: true},
		complex64(0):  {Value: 0, Valid: false},
		complex64(1):  {Value: 0, Valid: false},
		complex128(0): {Value: 0, Valid: false},
		complex128(1): {Value: 0, Valid: false},
	} {
		suite.Run(fmt.Sprintf("%T(%v)", value, value), func() {
			test := gommon.Convert(value).ToInt8()
			suite.Equalf(res.Valid, test.Valid, "value=%#v res=%#v", value, res)
			suite.Equalf(res.Value, test.Value, "value=%#v res=%#v", value, res)
		})
	}
}

func (suite *ConvertSuite) TestToInt16() {
	for value, res := range map[any]gommon.Value[int16]{
		struct{}{}:    {Value: 0, Valid: false},
		"test":        {Value: 0, Valid: false},
		"1":           {Value: 1, Valid: true},
		false:         {Value: 0, Valid: true},
		true:          {Value: 1, Valid: true},
		-1:            {Value: -1, Valid: true},
		0:             {Value: 0, Valid: true},
		1:             {Value: 1, Valid: true},
		int8(-1):      {Value: -1, Valid: true},
		int8(0):       {Value: 0, Valid: true},
		int8(1):       {Value: 1, Valid: true},
		int16(-1):     {Value: -1, Valid: true},
		int16(0):      {Value: 0, Valid: true},
		int16(1):      {Value: 1, Valid: true},
		int32(-1):     {Value: -1, Valid: true},
		int32(0):      {Value: 0, Valid: true},
		int32(1):      {Value: 1, Valid: true},
		int64(-1):     {Value: -1, Valid: true},
		int64(0):      {Value: 0, Valid: true},
		int64(1):      {Value: 1, Valid: true},
		uint(0):       {Value: 0, Valid: true},
		uint(1):       {Value: 1, Valid: true},
		uint8(0):      {Value: 0, Valid: true},
		uint8(1):      {Value: 1, Valid: true},
		uint16(0):     {Value: 0, Valid: true},
		uint16(1):     {Value: 1, Valid: true},
		uint32(0):     {Value: 0, Valid: true},
		uint32(1):     {Value: 1, Valid: true},
		uint64(0):     {Value: 0, Valid: true},
		uint64(1):     {Value: 1, Valid: true},
		uintptr(0):    {Value: 0, Valid: true},
		uintptr(1):    {Value: 1, Valid: true},
		float32(0):    {Value: 0, Valid: true},
		float32(1):    {Value: 1, Valid: true},
		float64(0):    {Value: 0, Valid: true},
		float64(1):    {Value: 1, Valid: true},
		complex64(0):  {Value: 0, Valid: false},
		complex64(1):  {Value: 0, Valid: false},
		complex128(0): {Value: 0, Valid: false},
		complex128(1): {Value: 0, Valid: false},
	} {
		suite.Run(fmt.Sprintf("%T(%v)", value, value), func() {
			test := gommon.Convert(value).ToInt16()
			suite.Equalf(res.Valid, test.Valid, "value=%#v res=%#v", value, res)
			suite.Equalf(res.Value, test.Value, "value=%#v res=%#v", value, res)
		})
	}
}

func (suite *ConvertSuite) TestToInt32() {
	for value, res := range map[any]gommon.Value[int32]{
		struct{}{}:    {Value: 0, Valid: false},
		"test":        {Value: 0, Valid: false},
		"1":           {Value: 1, Valid: true},
		false:         {Value: 0, Valid: true},
		true:          {Value: 1, Valid: true},
		-1:            {Value: -1, Valid: true},
		0:             {Value: 0, Valid: true},
		1:             {Value: 1, Valid: true},
		int8(-1):      {Value: -1, Valid: true},
		int8(0):       {Value: 0, Valid: true},
		int8(1):       {Value: 1, Valid: true},
		int16(-1):     {Value: -1, Valid: true},
		int16(0):      {Value: 0, Valid: true},
		int16(1):      {Value: 1, Valid: true},
		int32(-1):     {Value: -1, Valid: true},
		int32(0):      {Value: 0, Valid: true},
		int32(1):      {Value: 1, Valid: true},
		int64(-1):     {Value: -1, Valid: true},
		int64(0):      {Value: 0, Valid: true},
		int64(1):      {Value: 1, Valid: true},
		uint(0):       {Value: 0, Valid: true},
		uint(1):       {Value: 1, Valid: true},
		uint8(0):      {Value: 0, Valid: true},
		uint8(1):      {Value: 1, Valid: true},
		uint16(0):     {Value: 0, Valid: true},
		uint16(1):     {Value: 1, Valid: true},
		uint32(0):     {Value: 0, Valid: true},
		uint32(1):     {Value: 1, Valid: true},
		uint64(0):     {Value: 0, Valid: true},
		uint64(1):     {Value: 1, Valid: true},
		uintptr(0):    {Value: 0, Valid: true},
		uintptr(1):    {Value: 1, Valid: true},
		float32(0):    {Value: 0, Valid: true},
		float32(1):    {Value: 1, Valid: true},
		float64(0):    {Value: 0, Valid: true},
		float64(1):    {Value: 1, Valid: true},
		complex64(0):  {Value: 0, Valid: false},
		complex64(1):  {Value: 0, Valid: false},
		complex128(0): {Value: 0, Valid: false},
		complex128(1): {Value: 0, Valid: false},
	} {
		suite.Run(fmt.Sprintf("%T(%v)", value, value), func() {
			test := gommon.Convert(value).ToInt32()
			suite.Equalf(res.Valid, test.Valid, "value=%#v res=%#v", value, res)
			suite.Equalf(res.Value, test.Value, "value=%#v res=%#v", value, res)
		})
	}
}

func (suite *ConvertSuite) TestToInt64() {
	for value, res := range map[any]gommon.Value[int64]{
		struct{}{}:    {Value: 0, Valid: false},
		"test":        {Value: 0, Valid: false},
		"1":           {Value: 1, Valid: true},
		false:         {Value: 0, Valid: true},
		true:          {Value: 1, Valid: true},
		-1:            {Value: -1, Valid: true},
		0:             {Value: 0, Valid: true},
		1:             {Value: 1, Valid: true},
		int8(-1):      {Value: -1, Valid: true},
		int8(0):       {Value: 0, Valid: true},
		int8(1):       {Value: 1, Valid: true},
		int16(-1):     {Value: -1, Valid: true},
		int16(0):      {Value: 0, Valid: true},
		int16(1):      {Value: 1, Valid: true},
		int32(-1):     {Value: -1, Valid: true},
		int32(0):      {Value: 0, Valid: true},
		int32(1):      {Value: 1, Valid: true},
		int64(-1):     {Value: -1, Valid: true},
		int64(0):      {Value: 0, Valid: true},
		int64(1):      {Value: 1, Valid: true},
		uint(0):       {Value: 0, Valid: true},
		uint(1):       {Value: 1, Valid: true},
		uint8(0):      {Value: 0, Valid: true},
		uint8(1):      {Value: 1, Valid: true},
		uint16(0):     {Value: 0, Valid: true},
		uint16(1):     {Value: 1, Valid: true},
		uint32(0):     {Value: 0, Valid: true},
		uint32(1):     {Value: 1, Valid: true},
		uint64(0):     {Value: 0, Valid: true},
		uint64(1):     {Value: 1, Valid: true},
		uintptr(0):    {Value: 0, Valid: true},
		uintptr(1):    {Value: 1, Valid: true},
		float32(0):    {Value: 0, Valid: true},
		float32(1):    {Value: 1, Valid: true},
		float64(0):    {Value: 0, Valid: true},
		float64(1):    {Value: 1, Valid: true},
		complex64(0):  {Value: 0, Valid: false},
		complex64(1):  {Value: 0, Valid: false},
		complex128(0): {Value: 0, Valid: false},
		complex128(1): {Value: 0, Valid: false},
	} {
		suite.Run(fmt.Sprintf("%T(%v)", value, value), func() {
			test := gommon.Convert(value).ToInt64()
			suite.Equalf(res.Valid, test.Valid, "value=%#v res=%#v", value, res)
			suite.Equalf(res.Value, test.Value, "value=%#v res=%#v", value, res)
		})
	}
}

func (suite *ConvertSuite) TestToUint() {
	for value, res := range map[any]gommon.Value[uint]{
		struct{}{}:    {Value: 0x0, Valid: false},
		"test":        {Value: 0x0, Valid: false},
		"1":           {Value: 0x1, Valid: true},
		false:         {Value: 0x0, Valid: true},
		true:          {Value: 0x1, Valid: true},
		-1:            {Value: 0xffffffffffffffff, Valid: true},
		0:             {Value: 0x0, Valid: true},
		1:             {Value: 0x1, Valid: true},
		int8(-1):      {Value: 0xffffffffffffffff, Valid: true},
		int8(0):       {Value: 0x0, Valid: true},
		int8(1):       {Value: 0x1, Valid: true},
		int16(-1):     {Value: 0xffffffffffffffff, Valid: true},
		int16(0):      {Value: 0x0, Valid: true},
		int16(1):      {Value: 0x1, Valid: true},
		int32(-1):     {Value: 0xffffffffffffffff, Valid: true},
		int32(0):      {Value: 0x0, Valid: true},
		int32(1):      {Value: 0x1, Valid: true},
		int64(-1):     {Value: 0xffffffffffffffff, Valid: true},
		int64(0):      {Value: 0x0, Valid: true},
		int64(1):      {Value: 0x1, Valid: true},
		uint(0):       {Value: 0x0, Valid: true},
		uint(1):       {Value: 0x1, Valid: true},
		uint8(0):      {Value: 0x0, Valid: true},
		uint8(1):      {Value: 0x1, Valid: true},
		uint16(0):     {Value: 0x0, Valid: true},
		uint16(1):     {Value: 0x1, Valid: true},
		uint32(0):     {Value: 0x0, Valid: true},
		uint32(1):     {Value: 0x1, Valid: true},
		uint64(0):     {Value: 0x0, Valid: true},
		uint64(1):     {Value: 0x1, Valid: true},
		uintptr(0):    {Value: 0x0, Valid: true},
		uintptr(1):    {Value: 0x1, Valid: true},
		float32(0):    {Value: 0x0, Valid: true},
		float32(1):    {Value: 0x1, Valid: true},
		float64(0):    {Value: 0x0, Valid: true},
		float64(1):    {Value: 0x1, Valid: true},
		complex64(0):  {Value: 0x0, Valid: false},
		complex64(1):  {Value: 0x0, Valid: false},
		complex128(0): {Value: 0x0, Valid: false},
		complex128(1): {Value: 0x0, Valid: false},
	} {
		suite.Run(fmt.Sprintf("%T(%v)", value, value), func() {
			test := gommon.Convert(value).ToUint()
			suite.Equalf(res.Valid, test.Valid, "value=%#v res=%#v", value, res)
			suite.Equalf(res.Value, test.Value, "value=%#v res=%#v", value, res)
		})
	}
}

func (suite *ConvertSuite) TestToUint8() {
	for value, res := range map[any]gommon.Value[uint8]{
		struct{}{}:    {Value: 0x0, Valid: false},
		"test":        {Value: 0x0, Valid: false},
		"1":           {Value: 0x1, Valid: true},
		false:         {Value: 0x0, Valid: true},
		true:          {Value: 0x1, Valid: true},
		-1:            {Value: 0xff, Valid: true},
		0:             {Value: 0x0, Valid: true},
		1:             {Value: 0x1, Valid: true},
		int8(-1):      {Value: 0xff, Valid: true},
		int8(0):       {Value: 0x0, Valid: true},
		int8(1):       {Value: 0x1, Valid: true},
		int16(-1):     {Value: 0xff, Valid: true},
		int16(0):      {Value: 0x0, Valid: true},
		int16(1):      {Value: 0x1, Valid: true},
		int32(-1):     {Value: 0xff, Valid: true},
		int32(0):      {Value: 0x0, Valid: true},
		int32(1):      {Value: 0x1, Valid: true},
		int64(-1):     {Value: 0xff, Valid: true},
		int64(0):      {Value: 0x0, Valid: true},
		int64(1):      {Value: 0x1, Valid: true},
		uint(0):       {Value: 0x0, Valid: true},
		uint(1):       {Value: 0x1, Valid: true},
		uint8(0):      {Value: 0x0, Valid: true},
		uint8(1):      {Value: 0x1, Valid: true},
		uint16(0):     {Value: 0x0, Valid: true},
		uint16(1):     {Value: 0x1, Valid: true},
		uint32(0):     {Value: 0x0, Valid: true},
		uint32(1):     {Value: 0x1, Valid: true},
		uint64(0):     {Value: 0x0, Valid: true},
		uint64(1):     {Value: 0x1, Valid: true},
		uintptr(0):    {Value: 0x0, Valid: true},
		uintptr(1):    {Value: 0x1, Valid: true},
		float32(0):    {Value: 0x0, Valid: true},
		float32(1):    {Value: 0x1, Valid: true},
		float64(0):    {Value: 0x0, Valid: true},
		float64(1):    {Value: 0x1, Valid: true},
		complex64(0):  {Value: 0x0, Valid: false},
		complex64(1):  {Value: 0x0, Valid: false},
		complex128(0): {Value: 0x0, Valid: false},
		complex128(1): {Value: 0x0, Valid: false},
	} {
		suite.Run(fmt.Sprintf("%T(%v)", value, value), func() {
			test := gommon.Convert(value).ToUint8()
			suite.Equalf(res.Valid, test.Valid, "value=%#v res=%#v", value, res)
			suite.Equalf(res.Value, test.Value, "value=%#v res=%#v", value, res)
		})
	}
}

func (suite *ConvertSuite) TestToUint16() {
	for value, res := range map[any]gommon.Value[uint16]{
		struct{}{}:    {Value: 0x0, Valid: false},
		"test":        {Value: 0x0, Valid: false},
		"1":           {Value: 0x1, Valid: true},
		false:         {Value: 0x0, Valid: true},
		true:          {Value: 0x1, Valid: true},
		-1:            {Value: 0xffff, Valid: true},
		0:             {Value: 0x0, Valid: true},
		1:             {Value: 0x1, Valid: true},
		int8(-1):      {Value: 0xffff, Valid: true},
		int8(0):       {Value: 0x0, Valid: true},
		int8(1):       {Value: 0x1, Valid: true},
		int16(-1):     {Value: 0xffff, Valid: true},
		int16(0):      {Value: 0x0, Valid: true},
		int16(1):      {Value: 0x1, Valid: true},
		int32(-1):     {Value: 0xffff, Valid: true},
		int32(0):      {Value: 0x0, Valid: true},
		int32(1):      {Value: 0x1, Valid: true},
		int64(-1):     {Value: 0xffff, Valid: true},
		int64(0):      {Value: 0x0, Valid: true},
		int64(1):      {Value: 0x1, Valid: true},
		uint(0):       {Value: 0x0, Valid: true},
		uint(1):       {Value: 0x1, Valid: true},
		uint8(0):      {Value: 0x0, Valid: true},
		uint8(1):      {Value: 0x1, Valid: true},
		uint16(0):     {Value: 0x0, Valid: true},
		uint16(1):     {Value: 0x1, Valid: true},
		uint32(0):     {Value: 0x0, Valid: true},
		uint32(1):     {Value: 0x1, Valid: true},
		uint64(0):     {Value: 0x0, Valid: true},
		uint64(1):     {Value: 0x1, Valid: true},
		uintptr(0):    {Value: 0x0, Valid: true},
		uintptr(1):    {Value: 0x1, Valid: true},
		float32(0):    {Value: 0x0, Valid: true},
		float32(1):    {Value: 0x1, Valid: true},
		float64(0):    {Value: 0x0, Valid: true},
		float64(1):    {Value: 0x1, Valid: true},
		complex64(0):  {Value: 0x0, Valid: false},
		complex64(1):  {Value: 0x0, Valid: false},
		complex128(0): {Value: 0x0, Valid: false},
		complex128(1): {Value: 0x0, Valid: false},
	} {
		suite.Run(fmt.Sprintf("%T(%v)", value, value), func() {
			test := gommon.Convert(value).ToUint16()
			suite.Equalf(res.Valid, test.Valid, "value=%#v res=%#v", value, res)
			suite.Equalf(res.Value, test.Value, "value=%#v res=%#v", value, res)
		})
	}
}

func (suite *ConvertSuite) TestToUint32() {
	for value, res := range map[any]gommon.Value[uint32]{
		struct{}{}:    {Value: 0x0, Valid: false},
		"test":        {Value: 0x0, Valid: false},
		"1":           {Value: 0x1, Valid: true},
		false:         {Value: 0x0, Valid: true},
		true:          {Value: 0x1, Valid: true},
		-1:            {Value: 0xffffffff, Valid: true},
		0:             {Value: 0x0, Valid: true},
		1:             {Value: 0x1, Valid: true},
		int8(-1):      {Value: 0xffffffff, Valid: true},
		int8(0):       {Value: 0x0, Valid: true},
		int8(1):       {Value: 0x1, Valid: true},
		int16(-1):     {Value: 0xffffffff, Valid: true},
		int16(0):      {Value: 0x0, Valid: true},
		int16(1):      {Value: 0x1, Valid: true},
		int32(-1):     {Value: 0xffffffff, Valid: true},
		int32(0):      {Value: 0x0, Valid: true},
		int32(1):      {Value: 0x1, Valid: true},
		int64(-1):     {Value: 0xffffffff, Valid: true},
		int64(0):      {Value: 0x0, Valid: true},
		int64(1):      {Value: 0x1, Valid: true},
		uint(0):       {Value: 0x0, Valid: true},
		uint(1):       {Value: 0x1, Valid: true},
		uint8(0):      {Value: 0x0, Valid: true},
		uint8(1):      {Value: 0x1, Valid: true},
		uint16(0):     {Value: 0x0, Valid: true},
		uint16(1):     {Value: 0x1, Valid: true},
		uint32(0):     {Value: 0x0, Valid: true},
		uint32(1):     {Value: 0x1, Valid: true},
		uint64(0):     {Value: 0x0, Valid: true},
		uint64(1):     {Value: 0x1, Valid: true},
		uintptr(0):    {Value: 0x0, Valid: true},
		uintptr(1):    {Value: 0x1, Valid: true},
		float32(0):    {Value: 0x0, Valid: true},
		float32(1):    {Value: 0x1, Valid: true},
		float64(0):    {Value: 0x0, Valid: true},
		float64(1):    {Value: 0x1, Valid: true},
		complex64(0):  {Value: 0x0, Valid: false},
		complex64(1):  {Value: 0x0, Valid: false},
		complex128(0): {Value: 0x0, Valid: false},
		complex128(1): {Value: 0x0, Valid: false},
	} {
		suite.Run(fmt.Sprintf("%T(%v)", value, value), func() {
			test := gommon.Convert(value).ToUint32()
			suite.Equalf(res.Valid, test.Valid, "value=%#v res=%#v", value, res)
			suite.Equalf(res.Value, test.Value, "value=%#v res=%#v", value, res)
		})
	}
}

func (suite *ConvertSuite) TestToUint64() {
	for value, res := range map[any]gommon.Value[uint64]{
		struct{}{}:    {Value: 0x0, Valid: false},
		"test":        {Value: 0x0, Valid: false},
		"1":           {Value: 0x1, Valid: true},
		false:         {Value: 0x0, Valid: true},
		true:          {Value: 0x1, Valid: true},
		-1:            {Value: 0xffffffffffffffff, Valid: true},
		0:             {Value: 0x0, Valid: true},
		1:             {Value: 0x1, Valid: true},
		int8(-1):      {Value: 0xffffffffffffffff, Valid: true},
		int8(0):       {Value: 0x0, Valid: true},
		int8(1):       {Value: 0x1, Valid: true},
		int16(-1):     {Value: 0xffffffffffffffff, Valid: true},
		int16(0):      {Value: 0x0, Valid: true},
		int16(1):      {Value: 0x1, Valid: true},
		int32(-1):     {Value: 0xffffffffffffffff, Valid: true},
		int32(0):      {Value: 0x0, Valid: true},
		int32(1):      {Value: 0x1, Valid: true},
		int64(-1):     {Value: 0xffffffffffffffff, Valid: true},
		int64(0):      {Value: 0x0, Valid: true},
		int64(1):      {Value: 0x1, Valid: true},
		uint(0):       {Value: 0x0, Valid: true},
		uint(1):       {Value: 0x1, Valid: true},
		uint8(0):      {Value: 0x0, Valid: true},
		uint8(1):      {Value: 0x1, Valid: true},
		uint16(0):     {Value: 0x0, Valid: true},
		uint16(1):     {Value: 0x1, Valid: true},
		uint32(0):     {Value: 0x0, Valid: true},
		uint32(1):     {Value: 0x1, Valid: true},
		uint64(0):     {Value: 0x0, Valid: true},
		uint64(1):     {Value: 0x1, Valid: true},
		uintptr(0):    {Value: 0x0, Valid: true},
		uintptr(1):    {Value: 0x1, Valid: true},
		float32(0):    {Value: 0x0, Valid: true},
		float32(1):    {Value: 0x1, Valid: true},
		float64(0):    {Value: 0x0, Valid: true},
		float64(1):    {Value: 0x1, Valid: true},
		complex64(0):  {Value: 0x0, Valid: false},
		complex64(1):  {Value: 0x0, Valid: false},
		complex128(0): {Value: 0x0, Valid: false},
		complex128(1): {Value: 0x0, Valid: false},
	} {
		suite.Run(fmt.Sprintf("%T(%v)", value, value), func() {
			test := gommon.Convert(value).ToUint64()
			suite.Equalf(res.Valid, test.Valid, "value=%#v res=%#v", value, res)
			suite.Equalf(res.Value, test.Value, "value=%#v res=%#v", value, res)
		})
	}
}

func (suite *ConvertSuite) TestToUintPtr() {
	for value, res := range map[any]gommon.Value[uintptr]{
		struct{}{}:    {Value: 0x0, Valid: false},
		"test":        {Value: 0x0, Valid: false},
		"1":           {Value: 0x1, Valid: true},
		false:         {Value: 0x0, Valid: false},
		true:          {Value: 0x0, Valid: false},
		-1:            {Value: 0xffffffffffffffff, Valid: true},
		0:             {Value: 0x0, Valid: true},
		1:             {Value: 0x1, Valid: true},
		int8(-1):      {Value: 0xffffffffffffffff, Valid: true},
		int8(0):       {Value: 0x0, Valid: true},
		int8(1):       {Value: 0x1, Valid: true},
		int16(-1):     {Value: 0xffffffffffffffff, Valid: true},
		int16(0):      {Value: 0x0, Valid: true},
		int16(1):      {Value: 0x1, Valid: true},
		int32(-1):     {Value: 0xffffffffffffffff, Valid: true},
		int32(0):      {Value: 0x0, Valid: true},
		int32(1):      {Value: 0x1, Valid: true},
		int64(-1):     {Value: 0xffffffffffffffff, Valid: true},
		int64(0):      {Value: 0x0, Valid: true},
		int64(1):      {Value: 0x1, Valid: true},
		uint(0):       {Value: 0x0, Valid: true},
		uint(1):       {Value: 0x1, Valid: true},
		uint8(0):      {Value: 0x0, Valid: true},
		uint8(1):      {Value: 0x1, Valid: true},
		uint16(0):     {Value: 0x0, Valid: true},
		uint16(1):     {Value: 0x1, Valid: true},
		uint32(0):     {Value: 0x0, Valid: true},
		uint32(1):     {Value: 0x1, Valid: true},
		uint64(0):     {Value: 0x0, Valid: true},
		uint64(1):     {Value: 0x1, Valid: true},
		uintptr(0):    {Value: 0x0, Valid: true},
		uintptr(1):    {Value: 0x1, Valid: true},
		float32(0):    {Value: 0x0, Valid: true},
		float32(1):    {Value: 0x1, Valid: true},
		float64(0):    {Value: 0x0, Valid: true},
		float64(1):    {Value: 0x1, Valid: true},
		complex64(0):  {Value: 0x0, Valid: false},
		complex64(1):  {Value: 0x0, Valid: false},
		complex128(0): {Value: 0x0, Valid: false},
		complex128(1): {Value: 0x0, Valid: false},
	} {
		suite.Run(fmt.Sprintf("%T(%v)", value, value), func() {
			test := gommon.Convert(value).ToUintPtr()
			suite.Equalf(res.Valid, test.Valid, "value=%#v res=%#v", value, res)
			suite.Equalf(res.Value, test.Value, "value=%#v res=%#v", value, res)
		})
	}
}

func (suite *ConvertSuite) TestToFloat32() {
	for value, res := range map[any]gommon.Value[float32]{
		struct{}{}:    {Value: 0, Valid: false},
		"test":        {Value: 0, Valid: false},
		"1":           {Value: 1, Valid: true},
		false:         {Value: 0, Valid: true},
		true:          {Value: 1, Valid: true},
		-1:            {Value: -1, Valid: true},
		0:             {Value: 0, Valid: true},
		1:             {Value: 1, Valid: true},
		int8(-1):      {Value: -1, Valid: true},
		int8(0):       {Value: 0, Valid: true},
		int8(1):       {Value: 1, Valid: true},
		int16(-1):     {Value: -1, Valid: true},
		int16(0):      {Value: 0, Valid: true},
		int16(1):      {Value: 1, Valid: true},
		int32(-1):     {Value: -1, Valid: true},
		int32(0):      {Value: 0, Valid: true},
		int32(1):      {Value: 1, Valid: true},
		int64(-1):     {Value: -1, Valid: true},
		int64(0):      {Value: 0, Valid: true},
		int64(1):      {Value: 1, Valid: true},
		uint(0):       {Value: 0, Valid: true},
		uint(1):       {Value: 1, Valid: true},
		uint8(0):      {Value: 0, Valid: true},
		uint8(1):      {Value: 1, Valid: true},
		uint16(0):     {Value: 0, Valid: true},
		uint16(1):     {Value: 1, Valid: true},
		uint32(0):     {Value: 0, Valid: true},
		uint32(1):     {Value: 1, Valid: true},
		uint64(0):     {Value: 0, Valid: true},
		uint64(1):     {Value: 1, Valid: true},
		uintptr(0):    {Value: 0, Valid: true},
		uintptr(1):    {Value: 1, Valid: true},
		float32(0):    {Value: 0, Valid: true},
		float32(1):    {Value: 1, Valid: true},
		float64(0):    {Value: 0, Valid: true},
		float64(1):    {Value: 1, Valid: true},
		complex64(0):  {Value: 0, Valid: false},
		complex64(1):  {Value: 0, Valid: false},
		complex128(0): {Value: 0, Valid: false},
		complex128(1): {Value: 0, Valid: false},
	} {
		suite.Run(fmt.Sprintf("%T(%v)", value, value), func() {
			test := gommon.Convert(value).ToFloat32()
			suite.Equalf(res.Valid, test.Valid, "value=%#v res=%#v", value, res)
			suite.Equalf(res.Value, test.Value, "value=%#v res=%#v", value, res)
		})
	}
}

func (suite *ConvertSuite) TestToFloat64() {
	for value, res := range map[any]gommon.Value[float64]{
		struct{}{}:    {Value: 0, Valid: false},
		"test":        {Value: 0, Valid: false},
		"1":           {Value: 1, Valid: true},
		false:         {Value: 0, Valid: true},
		true:          {Value: 1, Valid: true},
		-1:            {Value: -1, Valid: true},
		0:             {Value: 0, Valid: true},
		1:             {Value: 1, Valid: true},
		int8(-1):      {Value: -1, Valid: true},
		int8(0):       {Value: 0, Valid: true},
		int8(1):       {Value: 1, Valid: true},
		int16(-1):     {Value: -1, Valid: true},
		int16(0):      {Value: 0, Valid: true},
		int16(1):      {Value: 1, Valid: true},
		int32(-1):     {Value: -1, Valid: true},
		int32(0):      {Value: 0, Valid: true},
		int32(1):      {Value: 1, Valid: true},
		int64(-1):     {Value: -1, Valid: true},
		int64(0):      {Value: 0, Valid: true},
		int64(1):      {Value: 1, Valid: true},
		uint(0):       {Value: 0, Valid: true},
		uint(1):       {Value: 1, Valid: true},
		uint8(0):      {Value: 0, Valid: true},
		uint8(1):      {Value: 1, Valid: true},
		uint16(0):     {Value: 0, Valid: true},
		uint16(1):     {Value: 1, Valid: true},
		uint32(0):     {Value: 0, Valid: true},
		uint32(1):     {Value: 1, Valid: true},
		uint64(0):     {Value: 0, Valid: true},
		uint64(1):     {Value: 1, Valid: true},
		uintptr(0):    {Value: 0, Valid: true},
		uintptr(1):    {Value: 1, Valid: true},
		float32(0):    {Value: 0, Valid: true},
		float32(1):    {Value: 1, Valid: true},
		float64(0):    {Value: 0, Valid: true},
		float64(1):    {Value: 1, Valid: true},
		complex64(0):  {Value: 0, Valid: false},
		complex64(1):  {Value: 0, Valid: false},
		complex128(0): {Value: 0, Valid: false},
		complex128(1): {Value: 0, Valid: false},
	} {
		suite.Run(fmt.Sprintf("%T(%v)", value, value), func() {
			test := gommon.Convert(value).ToFloat64()
			suite.Equalf(res.Valid, test.Valid, "value=%#v res=%#v", value, res)
			suite.Equalf(res.Value, test.Value, "value=%#v res=%#v", value, res)
		})
	}
}

func (suite *ConvertSuite) TestToComplex64() {
	for value, res := range map[any]gommon.Value[complex64]{
		struct{}{}:    {Value: 0, Valid: false},
		"test":        {Value: 0, Valid: false},
		"1":           {Value: 1, Valid: true},
		false:         {Value: 0, Valid: false},
		true:          {Value: 0, Valid: false},
		-1:            {Value: 0, Valid: false},
		0:             {Value: 0, Valid: false},
		1:             {Value: 0, Valid: false},
		int8(-1):      {Value: 0, Valid: false},
		int8(0):       {Value: 0, Valid: false},
		int8(1):       {Value: 0, Valid: false},
		int16(-1):     {Value: 0, Valid: false},
		int16(0):      {Value: 0, Valid: false},
		int16(1):      {Value: 0, Valid: false},
		int32(-1):     {Value: 0, Valid: false},
		int32(0):      {Value: 0, Valid: false},
		int32(1):      {Value: 0, Valid: false},
		int64(-1):     {Value: 0, Valid: false},
		int64(0):      {Value: 0, Valid: false},
		int64(1):      {Value: 0, Valid: false},
		uint(0):       {Value: 0, Valid: false},
		uint(1):       {Value: 0, Valid: false},
		uint8(0):      {Value: 0, Valid: false},
		uint8(1):      {Value: 0, Valid: false},
		uint16(0):     {Value: 0, Valid: false},
		uint16(1):     {Value: 0, Valid: false},
		uint32(0):     {Value: 0, Valid: false},
		uint32(1):     {Value: 0, Valid: false},
		uint64(0):     {Value: 0, Valid: false},
		uint64(1):     {Value: 0, Valid: false},
		uintptr(0):    {Value: 0, Valid: false},
		uintptr(1):    {Value: 0, Valid: false},
		float32(0):    {Value: 0, Valid: false},
		float32(1):    {Value: 0, Valid: false},
		float64(0):    {Value: 0, Valid: false},
		float64(1):    {Value: 0, Valid: false},
		complex64(0):  {Value: 0, Valid: true},
		complex64(1):  {Value: 1, Valid: true},
		complex128(0): {Value: 0, Valid: true},
		complex128(1): {Value: 1, Valid: true},
	} {
		suite.Run(fmt.Sprintf("%T(%v)", value, value), func() {
			test := gommon.Convert(value).ToComplex64()
			suite.Equalf(res.Valid, test.Valid, "value=%#v res=%#v", value, res)
			suite.Equalf(res.Value, test.Value, "value=%#v res=%#v", value, res)
		})
	}
}

func (suite *ConvertSuite) TestToComplex128() {
	for value, res := range map[any]gommon.Value[complex128]{
		struct{}{}:    {Value: 0, Valid: false},
		"test":        {Value: 0, Valid: false},
		"1":           {Value: 1, Valid: true},
		false:         {Value: 0, Valid: false},
		true:          {Value: 0, Valid: false},
		-1:            {Value: 0, Valid: false},
		0:             {Value: 0, Valid: false},
		1:             {Value: 0, Valid: false},
		int8(-1):      {Value: 0, Valid: false},
		int8(0):       {Value: 0, Valid: false},
		int8(1):       {Value: 0, Valid: false},
		int16(-1):     {Value: 0, Valid: false},
		int16(0):      {Value: 0, Valid: false},
		int16(1):      {Value: 0, Valid: false},
		int32(-1):     {Value: 0, Valid: false},
		int32(0):      {Value: 0, Valid: false},
		int32(1):      {Value: 0, Valid: false},
		int64(-1):     {Value: 0, Valid: false},
		int64(0):      {Value: 0, Valid: false},
		int64(1):      {Value: 0, Valid: false},
		uint(0):       {Value: 0, Valid: false},
		uint(1):       {Value: 0, Valid: false},
		uint8(0):      {Value: 0, Valid: false},
		uint8(1):      {Value: 0, Valid: false},
		uint16(0):     {Value: 0, Valid: false},
		uint16(1):     {Value: 0, Valid: false},
		uint32(0):     {Value: 0, Valid: false},
		uint32(1):     {Value: 0, Valid: false},
		uint64(0):     {Value: 0, Valid: false},
		uint64(1):     {Value: 0, Valid: false},
		uintptr(0):    {Value: 0, Valid: false},
		uintptr(1):    {Value: 0, Valid: false},
		float32(0):    {Value: 0, Valid: false},
		float32(1):    {Value: 0, Valid: false},
		float64(0):    {Value: 0, Valid: false},
		float64(1):    {Value: 0, Valid: false},
		complex64(0):  {Value: 0, Valid: true},
		complex64(1):  {Value: 1, Valid: true},
		complex128(0): {Value: 0, Valid: true},
		complex128(1): {Value: 1, Valid: true},
	} {
		suite.Run(fmt.Sprintf("%T(%v)", value, value), func() {
			test := gommon.Convert(value).ToComplex128()
			suite.Equalf(res.Valid, test.Valid, "value=%#v res=%#v", value, res)
			suite.Equalf(res.Value, test.Value, "value=%#v res=%#v", value, res)
		})
	}
}

func ExampleCompact_bytes() {
	fmt.Printf("%#v\n", gommon.Compact([]byte{0x00, 0x61, 0x62, 0x00, 0x63, 0x64, 0x00}))
	// Output: []byte{0x61, 0x62, 0x63, 0x64}
}

func ExampleCompact_complex64() {
	v := gommon.Compact([]complex64{0, 1.9, 2.8, 3.7, 0.5})
	fmt.Printf("%T, %v\n", v, v)
	// Output: []complex64, [(1.9+0i) (2.8+0i) (3.7+0i) (0.5+0i)]
}

func ExampleCompact_complex128() {
	v := gommon.Compact([]complex128{0, 1.9, 2.8, 3.7, 0.5})
	fmt.Printf("%T, %v\n", v, v)
	// Output: []complex128, [(1.9+0i) (2.8+0i) (3.7+0i) (0.5+0i)]
}

func ExampleCompact_float32() {
	v := gommon.Compact([]float32{0, 1.9, 2.8, 3.7, 0.5})
	fmt.Printf("%T, %v\n", v, v)
	// Output: []float32, [1.9 2.8 3.7 0.5]
}

func ExampleCompact_float64() {
	v := gommon.Compact([]float64{0, 1.9, 2.8, 3.7, 0.5})
	fmt.Printf("%T, %v\n", v, v)
	// Output: []float64, [1.9 2.8 3.7 0.5]
}

func ExampleCompact_int() {
	v := gommon.Compact([]int{0, 1, 2, 0, 3, 4, 0})
	fmt.Printf("%T, %v\n", v, v)
	// Output: []int, [1 2 3 4]
}

func ExampleCompact_int8() {
	v := gommon.Compact([]int8{0, 1, 2, 0, 3, 4, 0})
	fmt.Printf("%T, %v\n", v, v)
	// Output: []int8, [1 2 3 4]
}

func ExampleCompact_int16() {
	v := gommon.Compact([]int16{0, 1, 2, 0, 3, 4, 0})
	fmt.Printf("%T, %v\n", v, v)
	// Output: []int16, [1 2 3 4]
}

func ExampleCompact_int32() {
	v := gommon.Compact([]int32{0, 1, 2, 0, 3, 4, 0})
	fmt.Printf("%T, %v\n", v, v)
	// Output: []int32, [1 2 3 4]
}

func ExampleCompact_int64() {
	v := gommon.Compact([]int64{0, 1, 2, 0, 3, 4, 0})
	fmt.Printf("%T, %v\n", v, v)
	// Output: []int64, [1 2 3 4]
}

func ExampleCompact_runes() {
	v := gommon.Compact([]rune{0x00, 0x61, 0x62, 0x00, 0x63, 0x64, 0x00})
	fmt.Printf("%T, %#x\n", v, v)
	// Output: []int32, [0x61 0x62 0x63 0x64]
}

func ExampleCompact_strings() {
	v := gommon.Compact([]string{"", "foo", "", "bar", ""})
	fmt.Printf("%T, %q\n", v, v)
	// Output: []string, ["foo" "bar"]
}

func ExampleCompact_uint() {
	v := gommon.Compact([]uint{0, 1, 2, 0, 3, 4, 0})
	fmt.Printf("%T, %v\n", v, v)
	// Output: []uint, [1 2 3 4]
}

func ExampleCompact_uint8() {
	v := gommon.Compact([]uint8{0, 1, 2, 0, 3, 4, 0})
	fmt.Printf("%T, %v\n", v, v)
	// Output: []uint8, [1 2 3 4]
}

func ExampleCompact_uint16() {
	v := gommon.Compact([]uint16{0, 1, 2, 0, 3, 4, 0})
	fmt.Printf("%T, %v\n", v, v)
	// Output: []uint16, [1 2 3 4]
}

func ExampleCompact_uint32() {
	v := gommon.Compact([]uint32{0, 1, 2, 0, 3, 4, 0})
	fmt.Printf("%T, %v\n", v, v)
	// Output: []uint32, [1 2 3 4]
}

func ExampleCompact_uint64() {
	v := gommon.Compact([]uint64{0, 1, 2, 0, 3, 4, 0})
	fmt.Printf("%T, %v\n", v, v)
	// Output: []uint64, [1 2 3 4]
}

func ExampleCompact_uintPtr() {
	v := gommon.Compact([]uintptr{0, 1, 2, 0, 3, 4, 0})
	fmt.Printf("%T, %v\n", v, v)
	// Output: []uintptr, [1 2 3 4]
}

func ExampleDefault_bytes() {
	fmt.Printf("%#x\n", gommon.Default(byte(0x79), byte(0x6e)))
	fmt.Printf("%#x\n", gommon.Default(byte(0x00), byte(0x6e)))
	// Output:
	// 0x79
	// 0x6e
}

func ExampleDefault_complex64() {
	fmt.Printf("%#v\n", gommon.Default(complex64(1.9), complex64(2.8)))
	fmt.Printf("%#v\n", gommon.Default(complex64(0.0), complex64(2.8)))
	// Output:
	// (1.9+0i)
	// (2.8+0i)
}

func ExampleDefault_complex128() {
	fmt.Printf("%#v\n", gommon.Default(complex128(1.9), complex128(2.8)))
	fmt.Printf("%#v\n", gommon.Default(complex128(0.0), complex128(2.8)))
	// Output:
	// (1.9+0i)
	// (2.8+0i)
}

func ExampleDefault_float32() {
	fmt.Printf("%#v\n", gommon.Default(float32(1.9), float32(2.8)))
	fmt.Printf("%#v\n", gommon.Default(float32(0.0), float32(2.8)))
	// Output:
	// 1.9
	// 2.8
}

func ExampleDefault_float64() {
	fmt.Printf("%#v\n", gommon.Default(1.9, 2.8))
	fmt.Printf("%#v\n", gommon.Default(0.0, 2.8))
	// Output:
	// 1.9
	// 2.8
}

func ExampleDefault_int() {
	fmt.Printf("%v\n", gommon.Default(1, 2))
	fmt.Printf("%v\n", gommon.Default(0, 2))
	// Output:
	// 1
	// 2
}

func ExampleDefault_int8() {
	fmt.Printf("%v\n", gommon.Default(int8(1), int8(2)))
	fmt.Printf("%v\n", gommon.Default(int8(0), int8(2)))
	// Output:
	// 1
	// 2
}

func ExampleDefault_int16() {
	fmt.Printf("%v\n", gommon.Default(int16(1), int16(2)))
	fmt.Printf("%v\n", gommon.Default(int16(0), int16(2)))
	// Output:
	// 1
	// 2
}

func ExampleDefault_int32() {
	fmt.Printf("%v\n", gommon.Default(int32(1), int32(2)))
	fmt.Printf("%v\n", gommon.Default(int32(0), int32(2)))
	// Output:
	// 1
	// 2
}

func ExampleDefault_int64() {
	fmt.Printf("%v\n", gommon.Default(int64(1), int64(2)))
	fmt.Printf("%v\n", gommon.Default(int64(0), int64(2)))
	// Output:
	// 1
	// 2
}

func ExampleDefault_runes() {
	fmt.Printf("%#x\n", gommon.Default(rune(0x79), rune(0x6e)))
	fmt.Printf("%#x\n", gommon.Default(rune(0x00), rune(0x6e)))
	// Output:
	// 0x79
	// 0x6e
}

func ExampleDefault_strings() {
	fmt.Printf("%s\n", gommon.Default("value1", "value2"))
	fmt.Printf("%s\n", gommon.Default("", "value2"))
	// Output:
	// value1
	// value2
}

func ExampleDefault_uint() {
	fmt.Printf("%v\n", gommon.Default(uint(1), uint(2)))
	fmt.Printf("%v\n", gommon.Default(uint(0), uint(2)))
	// Output:
	// 1
	// 2
}

func ExampleDefault_uint8() {
	fmt.Printf("%v\n", gommon.Default(uint8(1), uint8(2)))
	fmt.Printf("%v\n", gommon.Default(uint8(0), uint8(2)))
	// Output:
	// 1
	// 2
}

func ExampleDefault_uint16() {
	fmt.Printf("%v\n", gommon.Default(uint16(1), uint16(2)))
	fmt.Printf("%v\n", gommon.Default(uint16(0), uint16(2)))
	// Output:
	// 1
	// 2
}

func ExampleDefault_uint32() {
	fmt.Printf("%v\n", gommon.Default(uint32(1), uint32(2)))
	fmt.Printf("%v\n", gommon.Default(uint32(0), uint32(2)))
	// Output:
	// 1
	// 2
}

func ExampleDefault_uint64() {
	fmt.Printf("%v\n", gommon.Default(uint64(1), uint64(2)))
	fmt.Printf("%v\n", gommon.Default(uint64(0), uint64(2)))
	// Output:
	// 1
	// 2
}

func ExampleDefault_uintPtr() {
	fmt.Printf("%v\n", gommon.Default(uintptr(1), uintptr(2)))
	fmt.Printf("%v\n", gommon.Default(uintptr(0), uintptr(2)))
	// Output:
	// 1
	// 2
}

func ExampleEmpty_bytes() {
	fmt.Println(gommon.Empty(byte(0x00)))
	fmt.Println(gommon.Empty(byte(0x61)))
	// Output:
	// true
	// false
}

func ExampleEmpty_complex64() {
	fmt.Println(gommon.Empty(complex64(0.0)))
	fmt.Println(gommon.Empty(complex64(2.8)))
	// Output:
	// true
	// false
}

func ExampleEmpty_complex128() {
	fmt.Println(gommon.Empty(complex128(0.0)))
	fmt.Println(gommon.Empty(complex128(2.8)))
	// Output:
	// true
	// false
}

func ExampleEmpty_float32() {
	fmt.Println(gommon.Empty(float32(0.0)))
	fmt.Println(gommon.Empty(float32(2.8)))
	// Output:
	// true
	// false
}

func ExampleEmpty_float64() {
	fmt.Println(gommon.Empty(0.0))
	fmt.Println(gommon.Empty(2.8))
	// Output:
	// true
	// false
}

func ExampleEmpty_int() {
	fmt.Println(gommon.Empty(0))
	fmt.Println(gommon.Empty(1))
	// Output:
	// true
	// false
}

func ExampleEmpty_int8() {
	fmt.Println(gommon.Empty(int8(0)))
	fmt.Println(gommon.Empty(int8(1)))
	// Output:
	// true
	// false
}

func ExampleEmpty_int16() {
	fmt.Println(gommon.Empty(int16(0)))
	fmt.Println(gommon.Empty(int16(1)))
	// Output:
	// true
	// false
}

func ExampleEmpty_int32() {
	fmt.Println(gommon.Empty(int32(0)))
	fmt.Println(gommon.Empty(int32(1)))
	// Output:
	// true
	// false
}

func ExampleEmpty_int64() {
	fmt.Println(gommon.Empty(int64(0)))
	fmt.Println(gommon.Empty(int64(1)))
	// Output:
	// true
	// false
}

func ExampleEmpty_runes() {
	fmt.Println(gommon.Empty(rune(0x00)))
	fmt.Println(gommon.Empty(rune(0x6e)))
	// Output:
	// true
	// false
}

func ExampleEmpty_strings() {
	fmt.Println(gommon.Empty(""))
	fmt.Println(gommon.Empty("value"))
	// Output:
	// true
	// false
}

func ExampleEmpty_uint() {
	fmt.Println(gommon.Empty(uint(0)))
	fmt.Println(gommon.Empty(uint(1)))
	// Output:
	// true
	// false
}

func ExampleEmpty_uint8() {
	fmt.Println(gommon.Empty(uint8(0)))
	fmt.Println(gommon.Empty(uint8(1)))
	// Output:
	// true
	// false
}

func ExampleEmpty_uint16() {
	fmt.Println(gommon.Empty(uint16(0)))
	fmt.Println(gommon.Empty(uint16(1)))
	// Output:
	// true
	// false
}

func ExampleEmpty_uint32() {
	fmt.Println(gommon.Empty(uint32(0)))
	fmt.Println(gommon.Empty(uint32(1)))
	// Output:
	// true
	// false
}

func ExampleEmpty_uint64() {
	fmt.Println(gommon.Empty(uint64(0)))
	fmt.Println(gommon.Empty(uint64(1)))
	// Output:
	// true
	// false
}

func ExampleEmpty_uintPtr() {
	fmt.Println(gommon.Empty(uintptr(0)))
	fmt.Println(gommon.Empty(uintptr(1)))
	// Output:
	// true
	// false
}

func ExampleFirst_string() {
	fmt.Printf("%s\n", gommon.First([]string{"one", "two"}))
	// Output: one
}

func ExampleHas_bytes() {
	fmt.Println(gommon.Has(byte(0x62), byte(0x61), byte(0x62), byte(0x63), byte(0x64)))
	fmt.Println(gommon.Has(byte(0x65), byte(0x61), byte(0x62), byte(0x63), byte(0x64)))
	// Output:
	// true
	// false
}

func ExampleHas_complex64() {
	fmt.Println(gommon.Has(complex64(2.8), complex64(1.9), complex64(2.8), complex64(3.7)))
	fmt.Println(gommon.Has(complex64(5), complex64(1.9), complex64(2.8), complex64(3.7)))
	// Output:
	// true
	// false
}

func ExampleHas_complex128() {
	fmt.Println(gommon.Has(complex128(2.8), complex128(1.9), complex128(2.8), complex128(3.7)))
	fmt.Println(gommon.Has(complex128(5), complex128(1.9), complex128(2.8), complex128(3.7)))
	// Output:
	// true
	// false
}

func ExampleHas_float32() {
	fmt.Println(gommon.Has(float32(2.8), float32(1.9), float32(2.8), float32(3.7)))
	fmt.Println(gommon.Has(float32(5), float32(1.9), float32(2.8), float32(3.7)))
	// Output:
	// true
	// false
}

func ExampleHas_float64() {
	fmt.Println(gommon.Has(2.8, 1.9, 2.8, 3.7))
	fmt.Println(gommon.Has(float64(5), 1.9, 2.8, 3.7))
	// Output:
	// true
	// false
}

func ExampleHas_int() {
	fmt.Println(gommon.Has(2, 1, 2, 3))
	fmt.Println(gommon.Has(5, 1, 2, 3))
	// Output:
	// true
	// false
}

func ExampleHas_int8() {
	fmt.Println(gommon.Has(int8(2), int8(1), int8(2), int8(3)))
	fmt.Println(gommon.Has(int8(5), int8(1), int8(2), int8(3)))
	// Output:
	// true
	// false
}

func ExampleHas_int16() {
	fmt.Println(gommon.Has(int16(2), int16(1), int16(2), int16(3)))
	fmt.Println(gommon.Has(int16(5), int16(1), int16(2), int16(3)))
	// Output:
	// true
	// false
}

func ExampleHas_int32() {
	fmt.Println(gommon.Has(int32(2), int32(1), int32(2), int32(3)))
	fmt.Println(gommon.Has(int32(5), int32(1), int32(2), int32(3)))
	// Output:
	// true
	// false
}

func ExampleHas_int64() {
	fmt.Println(gommon.Has(int64(2), int64(1), int64(2), int64(3)))
	fmt.Println(gommon.Has(int64(5), int64(1), int64(2), int64(3)))
	// Output:
	// true
	// false
}

func ExampleHas_runes() {
	fmt.Println(gommon.Has(rune(0x62), rune(0x61), rune(0x62), rune(0x63), rune(0x64)))
	fmt.Println(gommon.Has(rune(0x65), rune(0x61), rune(0x62), rune(0x63), rune(0x64)))
	// Output:
	// true
	// false
}

func ExampleHas_strings() {
	fmt.Println(gommon.Has("value2", "value1", "value2", "value3"))
	fmt.Println(gommon.Has("value5", "value1", "value2", "value3"))
	// Output:
	// true
	// false
}

func ExampleHas_uint() {
	fmt.Println(gommon.Has(uint(2), uint(1), uint(2), uint(3)))
	fmt.Println(gommon.Has(uint(5), uint(1), uint(2), uint(3)))
	// Output:
	// true
	// false
}

func ExampleHas_uint8() {
	fmt.Println(gommon.Has(uint8(2), uint8(1), uint8(2), uint8(3)))
	fmt.Println(gommon.Has(uint8(5), uint8(1), uint8(2), uint8(3)))
	// Output:
	// true
	// false
}

func ExampleHas_uint16() {
	fmt.Println(gommon.Has(uint16(2), uint16(1), uint16(2), uint16(3)))
	fmt.Println(gommon.Has(uint16(5), uint16(1), uint16(2), uint16(3)))
	// Output:
	// true
	// false
}

func ExampleHas_uint32() {
	fmt.Println(gommon.Has(uint32(2), uint32(1), uint32(2), uint32(3)))
	fmt.Println(gommon.Has(uint32(5), uint32(1), uint32(2), uint32(3)))
	// Output:
	// true
	// false
}

func ExampleHas_uint64() {
	fmt.Println(gommon.Has(uint64(2), uint64(1), uint64(2), uint64(3)))
	fmt.Println(gommon.Has(uint64(5), uint64(1), uint64(2), uint64(3)))
	// Output:
	// true
	// false
}

func ExampleHas_uintPtr() {
	fmt.Println(gommon.Has(uintptr(2), uintptr(1), uintptr(2), uintptr(3)))
	fmt.Println(gommon.Has(uintptr(5), uintptr(1), uintptr(2), uintptr(3)))
	// Output:
	// true
	// false
}

func ExampleMustParse_bool() {
	fmt.Println(gommon.MustParse[bool]("error"))
	fmt.Println(gommon.MustParse[bool]("true"))
	fmt.Println(gommon.MustParse[bool]("false"))
	// Output:
	// false
	// true
	// false
}

func ExampleMustParse_complex64() {
	fmt.Println(gommon.MustParse[complex64]("error"))
	fmt.Println(gommon.MustParse[complex64]("123.987"))
	fmt.Println(gommon.MustParse[complex64]("-987.123"))
	// Output:
	// (0+0i)
	// (123.987+0i)
	// (-987.123+0i)
}

func ExampleMustParse_complex128() {
	fmt.Println(gommon.MustParse[complex128]("error"))
	fmt.Println(gommon.MustParse[complex128]("123.987"))
	fmt.Println(gommon.MustParse[complex128]("-987.123"))
	// Output:
	// (0+0i)
	// (123.987+0i)
	// (-987.123+0i)
}

func ExampleMustParse_float32() {
	fmt.Println(gommon.MustParse[float32]("error"))
	fmt.Println(gommon.MustParse[float32]("123.987"))
	fmt.Println(gommon.MustParse[float32]("-987.123"))
	// Output:
	// 0
	// 123.987
	// -987.123
}

func ExampleMustParse_float64() {
	fmt.Println(gommon.MustParse[float64]("error"))
	fmt.Println(gommon.MustParse[float64]("123.987"))
	fmt.Println(gommon.MustParse[float64]("-987.123"))
	// Output:
	// 0
	// 123.987
	// -987.123
}

func ExampleMustParse_int() {
	fmt.Println(gommon.MustParse[int]("error"))
	fmt.Println(gommon.MustParse[int]("123"))
	fmt.Println(gommon.MustParse[int]("-128"))
	// Output:
	// 0
	// 123
	// -128
}

func ExampleMustParse_int8() {
	fmt.Println(gommon.MustParse[int8]("error"))
	fmt.Println(gommon.MustParse[int8]("123"))
	fmt.Println(gommon.MustParse[int8]("-128"))
	// Output:
	// 0
	// 123
	// -128
}

func ExampleMustParse_int16() {
	fmt.Println(gommon.MustParse[int16]("error"))
	fmt.Println(gommon.MustParse[int16]("123"))
	fmt.Println(gommon.MustParse[int16]("-128"))
	// Output:
	// 0
	// 123
	// -128
}

func ExampleMustParse_int32() {
	fmt.Println(gommon.MustParse[int32]("error"))
	fmt.Println(gommon.MustParse[int32]("123"))
	fmt.Println(gommon.MustParse[int32]("-128"))
	// Output:
	// 0
	// 123
	// -128
}

func ExampleMustParse_int64() {
	fmt.Println(gommon.MustParse[int64]("error"))
	fmt.Println(gommon.MustParse[int64]("123"))
	fmt.Println(gommon.MustParse[int64]("-128"))
	// Output:
	// 0
	// 123
	// -128
}

func ExampleMustParse_strings() {
	fmt.Println(gommon.MustParse[string]("ok"))
	// Output: ok
}

func ExampleMustParse_uint() {
	fmt.Println(gommon.MustParse[uint]("error"))
	fmt.Println(gommon.MustParse[uint]("123"))
	// Output:
	// 0
	// 123
}

func ExampleMustParse_uint8() {
	fmt.Println(gommon.MustParse[uint8]("error"))
	fmt.Println(gommon.MustParse[uint8]("123"))
	// Output:
	// 0
	// 123
}

func ExampleMustParse_uint16() {
	fmt.Println(gommon.MustParse[uint16]("error"))
	fmt.Println(gommon.MustParse[uint16]("123"))
	// Output:
	// 0
	// 123
}

func ExampleMustParse_uint32() {
	fmt.Println(gommon.MustParse[uint32]("error"))
	fmt.Println(gommon.MustParse[uint32]("123"))
	// Output:
	// 0
	// 123
}

func ExampleMustParse_uint64() {
	fmt.Println(gommon.MustParse[uint64]("error"))
	fmt.Println(gommon.MustParse[uint64]("123"))
	// Output:
	// 0
	// 123
}

func ExampleMustParse_uintptr() {
	fmt.Println(gommon.MustParse[uintptr]("error"))
	fmt.Println(gommon.MustParse[uintptr]("1a"))
	// Output:
	// 0
	// 26
}

func ExampleMustParse_duration() {
	fmt.Println(gommon.MustParse[time.Duration]("error"))
	fmt.Println(gommon.MustParse[time.Duration]("5m45s"))
	// Output:
	// 0s
	// 5m45s
}

func ExampleMustParse_time() {
	fmt.Println(gommon.MustParse[time.Time]("error").UTC())
	fmt.Println(gommon.MustParse[time.Time]("2006-01-02T15:04:05Z").UTC())
	// Output:
	// 0001-01-01 00:00:00 +0000 UTC
	// 2006-01-02 15:04:05 +0000 UTC
}

func ExampleMustParse_url() {
	fmt.Println(gommon.MustParse[*url.URL]("://user:abc"))
	fmt.Println(gommon.MustParse[*url.URL]("http://example.domain.tld:8080/path?query=value"))
	// Output:
	// <nil>
	// http://example.domain.tld:8080/path?query=value
}

func ExampleMustParse_urlValues() {
	fmt.Println(gommon.MustParse[url.Values]("key;value"))
	fmt.Println(gommon.MustParse[url.Values]("key=value"))
	// Output:
	// map[]
	// map[key:[value]]
}

func ExampleMustParse_base64() {
	fmt.Println(string(gommon.MustParse[gommon.Base64]("SGVsbG8")))
	fmt.Println(string(gommon.MustParse[gommon.Base64]("SGVsbG8=")))
	// Output:
	//
	// Hello
}

func ExampleMustParse_dsn() {
	fmt.Println(gommon.MustParse[gommon.DSN]("invalid:/unknown"))
	fmt.Println(gommon.MustParse[gommon.DSN]("https://user:pass@tcp(domain.tld:1234)/path?key=value"))
	// Output:
	//
	// https://user:pass@tcp(domain.tld:1234)/path?key=value
}

func ExampleMustParse_timestamp() {
	fmt.Println(gommon.MustParse[gommon.Timestamp]("error").Time().UTC())
	fmt.Println(gommon.MustParse[gommon.Timestamp]("1136214245").Time().UTC())
	// Output:
	// 1970-01-01 00:00:00 +0000 UTC
	// 2006-01-02 15:04:05 +0000 UTC
}

func ExampleParse_bool() {
	fmt.Println(gommon.Parse[bool]("error"))
	fmt.Println(gommon.Parse[bool]("true"))
	fmt.Println(gommon.Parse[bool]("false"))
	// Output:
	// false strconv.ParseBool: parsing "error": invalid syntax
	// true <nil>
	// false <nil>
}

func ExampleParse_complex64() {
	fmt.Println(gommon.Parse[complex64]("error"))
	fmt.Println(gommon.Parse[complex64]("123.987"))
	fmt.Println(gommon.Parse[complex64]("-987.123"))
	// Output:
	// (0+0i) strconv.ParseComplex: parsing "error": invalid syntax
	// (123.987+0i) <nil>
	// (-987.123+0i) <nil>
}

func ExampleParse_complex128() {
	fmt.Println(gommon.Parse[complex128]("error"))
	fmt.Println(gommon.Parse[complex128]("123.987"))
	fmt.Println(gommon.Parse[complex128]("-987.123"))
	// Output:
	// (0+0i) strconv.ParseComplex: parsing "error": invalid syntax
	// (123.987+0i) <nil>
	// (-987.123+0i) <nil>
}

func ExampleParse_float32() {
	fmt.Println(gommon.Parse[float32]("error"))
	fmt.Println(gommon.Parse[float32]("123.987"))
	fmt.Println(gommon.Parse[float32]("-987.123"))
	// Output:
	// 0 strconv.ParseFloat: parsing "error": invalid syntax
	// 123.987 <nil>
	// -987.123 <nil>
}

func ExampleParse_float64() {
	fmt.Println(gommon.Parse[float64]("error"))
	fmt.Println(gommon.Parse[float64]("123.987"))
	fmt.Println(gommon.Parse[float64]("-987.123"))
	// Output:
	// 0 strconv.ParseFloat: parsing "error": invalid syntax
	// 123.987 <nil>
	// -987.123 <nil>
}

func ExampleParse_int() {
	fmt.Println(gommon.Parse[int]("error"))
	fmt.Println(gommon.Parse[int]("123"))
	fmt.Println(gommon.Parse[int]("-128"))
	// Output:
	// 0 strconv.ParseInt: parsing "error": invalid syntax
	// 123 <nil>
	// -128 <nil>
}

func ExampleParse_int8() {
	fmt.Println(gommon.Parse[int8]("error"))
	fmt.Println(gommon.Parse[int8]("123"))
	fmt.Println(gommon.Parse[int8]("-128"))
	// Output:
	// 0 strconv.ParseInt: parsing "error": invalid syntax
	// 123 <nil>
	// -128 <nil>
}

func ExampleParse_int16() {
	fmt.Println(gommon.Parse[int16]("error"))
	fmt.Println(gommon.Parse[int16]("123"))
	fmt.Println(gommon.Parse[int16]("-128"))
	// Output:
	// 0 strconv.ParseInt: parsing "error": invalid syntax
	// 123 <nil>
	// -128 <nil>
}

func ExampleParse_int32() {
	fmt.Println(gommon.Parse[int32]("error"))
	fmt.Println(gommon.Parse[int32]("123"))
	fmt.Println(gommon.Parse[int32]("-128"))
	// Output:
	// 0 strconv.ParseInt: parsing "error": invalid syntax
	// 123 <nil>
	// -128 <nil>
}

func ExampleParse_int64() {
	fmt.Println(gommon.Parse[int64]("error"))
	fmt.Println(gommon.Parse[int64]("123"))
	fmt.Println(gommon.Parse[int64]("-128"))
	// Output:
	// 0 strconv.ParseInt: parsing "error": invalid syntax
	// 123 <nil>
	// -128 <nil>
}

func ExampleParse_strings() {
	fmt.Println(gommon.Parse[string]("ok"))
	// Output: ok <nil>
}

func ExampleParse_uint() {
	fmt.Println(gommon.Parse[uint]("error"))
	fmt.Println(gommon.Parse[uint]("123"))
	// Output:
	// 0 strconv.ParseUint: parsing "error": invalid syntax
	// 123 <nil>
}

func ExampleParse_uint8() {
	fmt.Println(gommon.Parse[uint8]("error"))
	fmt.Println(gommon.Parse[uint8]("123"))
	// Output:
	// 0 strconv.ParseUint: parsing "error": invalid syntax
	// 123 <nil>
}

func ExampleParse_uint16() {
	fmt.Println(gommon.Parse[uint16]("error"))
	fmt.Println(gommon.Parse[uint16]("123"))
	// Output:
	// 0 strconv.ParseUint: parsing "error": invalid syntax
	// 123 <nil>
}

func ExampleParse_uint32() {
	fmt.Println(gommon.Parse[uint32]("error"))
	fmt.Println(gommon.Parse[uint32]("123"))
	// Output:
	// 0 strconv.ParseUint: parsing "error": invalid syntax
	// 123 <nil>
}

func ExampleParse_uint64() {
	fmt.Println(gommon.Parse[uint64]("error"))
	fmt.Println(gommon.Parse[uint64]("123"))
	// Output:
	// 0 strconv.ParseUint: parsing "error": invalid syntax
	// 123 <nil>
}

func ExampleParse_uintptr() {
	fmt.Println(gommon.Parse[uintptr]("error"))
	fmt.Println(gommon.Parse[uintptr]("1a"))
	// Output:
	// 0 strconv.ParseUint: parsing "error": invalid syntax
	// 26 <nil>
}

func ExampleParse_duration() {
	fmt.Println(gommon.Parse[time.Duration]("error"))
	fmt.Println(gommon.Parse[time.Duration]("5m45s"))
	// Output:
	// 0s time: invalid duration "error"
	// 5m45s <nil>
}

func ExampleParse_time() {
	fmt.Println(gommon.Parse[time.Time]("error"))
	fmt.Println(gommon.Parse[time.Time]("2006-01-02T15:04:05Z"))
	// Output:
	// 0001-01-01 00:00:00 +0000 UTC parsing time "error" as "2006-01-02T15:04:05Z07:00": cannot parse "error" as "2006"
	// 2006-01-02 15:04:05 +0000 UTC <nil>
}

func ExampleParse_timestamp() {
	fmt.Println(gommon.Parse[gommon.Timestamp]("error"))
	fmt.Println(gommon.Parse[gommon.Timestamp]("1136214245"))
	// Output:
	// 0 strconv.ParseInt: parsing "error": invalid syntax
	// 1136214245 <nil>
}

func ExamplePtr_bool() {
	ptr := gommon.Ptr(true)
	fmt.Printf("%T = %v\n", ptr, *ptr)
	// Output: *bool = true
}

func ExamplePtr_byte() {
	ptr := gommon.Ptr(byte(0x61))
	fmt.Printf("%T = %v\n", ptr, *ptr)
	// Output: *uint8 = 97
}

func ExamplePtr_complex64() {
	ptr := gommon.Ptr(complex64(123.987))
	fmt.Printf("%T = %v\n", ptr, *ptr)
	// Output: *complex64 = (123.987+0i)
}

func ExamplePtr_complex128() {
	ptr := gommon.Ptr(complex128(123.987))
	fmt.Printf("%T = %v\n", ptr, *ptr)
	// Output: *complex128 = (123.987+0i)
}

func ExamplePtr_float32() {
	ptr := gommon.Ptr(float32(123.987))
	fmt.Printf("%T = %v\n", ptr, *ptr)
	// Output: *float32 = 123.987
}

func ExamplePtr_float64() {
	ptr := gommon.Ptr(float64(123.987))
	fmt.Printf("%T = %v\n", ptr, *ptr)
	// Output: *float64 = 123.987
}

func ExamplePtr_int() {
	ptr := gommon.Ptr(int(123))
	fmt.Printf("%T = %v\n", ptr, *ptr)
	// Output: *int = 123
}

func ExamplePtr_int8() {
	ptr := gommon.Ptr(int8(123))
	fmt.Printf("%T = %v\n", ptr, *ptr)
	// Output: *int8 = 123
}

func ExamplePtr_int16() {
	ptr := gommon.Ptr(int16(123))
	fmt.Printf("%T = %v\n", ptr, *ptr)
	// Output: *int16 = 123
}

func ExamplePtr_int32() {
	ptr := gommon.Ptr(int32(123))
	fmt.Printf("%T = %v\n", ptr, *ptr)
	// Output: *int32 = 123
}

func ExamplePtr_int64() {
	ptr := gommon.Ptr(int64(123))
	fmt.Printf("%T = %v\n", ptr, *ptr)
	// Output: *int64 = 123
}

func ExamplePtr_rune() {
	ptr := gommon.Ptr(rune(0x61))
	fmt.Printf("%T = %v\n", ptr, *ptr)
	// Output: *int32 = 97
}

func ExamplePtr_string() {
	ptr := gommon.Ptr("value")
	fmt.Printf("%T = %v\n", ptr, *ptr)
	// Output: *string = value
}

func ExamplePtr_uint() {
	ptr := gommon.Ptr(uint(123))
	fmt.Printf("%T = %v\n", ptr, *ptr)
	// Output: *uint = 123
}

func ExamplePtr_uint8() {
	ptr := gommon.Ptr(uint8(123))
	fmt.Printf("%T = %v\n", ptr, *ptr)
	// Output: *uint8 = 123
}

func ExamplePtr_uint16() {
	ptr := gommon.Ptr(uint16(123))
	fmt.Printf("%T = %v\n", ptr, *ptr)
	// Output: *uint16 = 123
}

func ExamplePtr_uint32() {
	ptr := gommon.Ptr(uint32(123))
	fmt.Printf("%T = %v\n", ptr, *ptr)
	// Output: *uint32 = 123
}

func ExamplePtr_uint64() {
	ptr := gommon.Ptr(uint64(123))
	fmt.Printf("%T = %v\n", ptr, *ptr)
	// Output: *uint64 = 123
}

func ExamplePtr_uintPtr() {
	ptr := gommon.Ptr(uintptr(123))
	fmt.Printf("%T = %v\n", ptr, *ptr)
	// Output: *uintptr = 123
}

func ExampleSplit_bool() {
	v := gommon.Split[bool]("true,,false", ",")
	fmt.Printf("%T, %v\n", v, v)
	// Output: []bool, [true false]
}

func ExampleSplit_complex64() {
	v := gommon.Split[complex64]("123.987,0,-987.123", ",")
	fmt.Printf("%T, %v\n", v, v)
	// Output: []complex64, [(123.987+0i) (0+0i) (-987.123+0i)]
}

func ExampleSplit_complex128() {
	v := gommon.Split[complex128]("123.987,0,-987.123", ",")
	fmt.Printf("%T, %v\n", v, v)
	// Output: []complex128, [(123.987+0i) (0+0i) (-987.123+0i)]
}

func ExampleSplit_float32() {
	v := gommon.Split[float32]("123.987,0,-987.123", ",")
	fmt.Printf("%T, %v\n", v, v)
	// Output: []float32, [123.987 0 -987.123]
}

func ExampleSplit_float64() {
	v := gommon.Split[float64]("123.987,0,-987.123", ",")
	fmt.Printf("%T, %v\n", v, v)
	// Output: []float64, [123.987 0 -987.123]
}

func ExampleSplit_int() {
	v := gommon.Split[int]("1,0,2", ",")
	fmt.Printf("%T, %v\n", v, v)
	// Output: []int, [1 0 2]
}

func ExampleSplit_int8() {
	v := gommon.Split[int8]("1,0,2", ",")
	fmt.Printf("%T, %v\n", v, v)
	// Output: []int8, [1 0 2]
}

func ExampleSplit_int16() {
	v := gommon.Split[int16]("1,0,2", ",")
	fmt.Printf("%T, %v\n", v, v)
	// Output: []int16, [1 0 2]
}

func ExampleSplit_int32() {
	v := gommon.Split[int32]("1,0,2", ",")
	fmt.Printf("%T, %v\n", v, v)
	// Output: []int32, [1 0 2]
}

func ExampleSplit_int64() {
	v := gommon.Split[int64]("1,0,2", ",")
	fmt.Printf("%T, %v\n", v, v)
	// Output: []int64, [1 0 2]
}

func ExampleSplit_strings() {
	v := gommon.Split[string]("value1,value2,value3", ",")
	fmt.Printf("%T, %v\n", v, v)
	// Output: []string, [value1 value2 value3]
}

func ExampleSplit_uint() {
	v := gommon.Split[uint]("1,0,2", ",")
	fmt.Printf("%T, %v\n", v, v)
	// Output: []uint, [1 0 2]
}

func ExampleSplit_uint8() {
	v := gommon.Split[uint8]("1,0,2", ",")
	fmt.Printf("%T, %v\n", v, v)
	// Output: []uint8, [1 0 2]
}

func ExampleSplit_uint16() {
	v := gommon.Split[uint16]("1,0,2", ",")
	fmt.Printf("%T, %v\n", v, v)
	// Output: []uint16, [1 0 2]
}

func ExampleSplit_uint32() {
	v := gommon.Split[uint32]("1,0,2", ",")
	fmt.Printf("%T, %v\n", v, v)
	// Output: []uint32, [1 0 2]
}

func ExampleSplit_uint64() {
	v := gommon.Split[uint64]("1,0,2", ",")
	fmt.Printf("%T, %v\n", v, v)
	// Output: []uint64, [1 0 2]
}

func ExampleSplitSlice_bool() {
	v := gommon.SplitSlice[bool]([]string{"true,", "false"}, ",")
	fmt.Printf("%T, %v\n", v, v)
	// Output: []bool, [true false]
}

func ExampleSplitSlice_complex64() {
	v := gommon.SplitSlice[complex64]([]string{"123.987,0", "-987.123"}, ",")
	fmt.Printf("%T, %v\n", v, v)
	// Output: []complex64, [(123.987+0i) (0+0i) (-987.123+0i)]
}

func ExampleSplitSlice_complex128() {
	v := gommon.SplitSlice[complex128]([]string{"123.987,0", "-987.123"}, ",")
	fmt.Printf("%T, %v\n", v, v)
	// Output: []complex128, [(123.987+0i) (0+0i) (-987.123+0i)]
}

func ExampleSplitSlice_float32() {
	v := gommon.SplitSlice[float32]([]string{"123.987,0,-987.123"}, ",")
	fmt.Printf("%T, %v\n", v, v)
	// Output: []float32, [123.987 0 -987.123]
}

func ExampleSplitSlice_float64() {
	v := gommon.SplitSlice[float64]([]string{"123.987,0,-987.123"}, ",")
	fmt.Printf("%T, %v\n", v, v)
	// Output: []float64, [123.987 0 -987.123]
}

func ExampleSplitSlice_int() {
	v := gommon.SplitSlice[int]([]string{"1,0", "2"}, ",")
	fmt.Printf("%T, %v\n", v, v)
	// Output: []int, [1 0 2]
}

func ExampleSplitSlice_int8() {
	v := gommon.SplitSlice[int8]([]string{"1,0", "2"}, ",")
	fmt.Printf("%T, %v\n", v, v)
	// Output: []int8, [1 0 2]
}

func ExampleSplitSlice_int16() {
	v := gommon.SplitSlice[int16]([]string{"1,0", "2"}, ",")
	fmt.Printf("%T, %v\n", v, v)
	// Output: []int16, [1 0 2]
}

func ExampleSplitSlice_int32() {
	v := gommon.SplitSlice[int32]([]string{"1,0", "2"}, ",")
	fmt.Printf("%T, %v\n", v, v)
	// Output: []int32, [1 0 2]
}

func ExampleSplitSlice_int64() {
	v := gommon.SplitSlice[int64]([]string{"1,0", "2"}, ",")
	fmt.Printf("%T, %v\n", v, v)
	// Output: []int64, [1 0 2]
}

func ExampleSplitSlice_strings() {
	v := gommon.SplitSlice[string]([]string{"value1,value2", "value3"}, ",")
	fmt.Printf("%T, %v\n", v, v)
	// Output: []string, [value1 value2 value3]
}

func ExampleSplitSlice_uint() {
	v := gommon.SplitSlice[uint]([]string{"1,0", "2"}, ",")
	fmt.Printf("%T, %v\n", v, v)
	// Output: []uint, [1 0 2]
}

func ExampleSplitSlice_uint8() {
	v := gommon.SplitSlice[uint8]([]string{"1,0", "2"}, ",")
	fmt.Printf("%T, %v\n", v, v)
	// Output: []uint8, [1 0 2]
}

func ExampleSplitSlice_uint16() {
	v := gommon.SplitSlice[uint16]([]string{"1,0", "2"}, ",")
	fmt.Printf("%T, %v\n", v, v)
	// Output: []uint16, [1 0 2]
}

func ExampleSplitSlice_uint32() {
	v := gommon.SplitSlice[uint32]([]string{"1,0", "2"}, ",")
	fmt.Printf("%T, %v\n", v, v)
	// Output: []uint32, [1 0 2]
}

func ExampleSplitSlice_uint64() {
	v := gommon.SplitSlice[uint64]([]string{"1,0", "2"}, ",")
	fmt.Printf("%T, %v\n", v, v)
	// Output: []uint64, [1 0 2]
}

func ExampleTernary_bytes() {
	fmt.Printf("%#x\n", gommon.Ternary(true, byte(0x79), byte(0x6e)))
	fmt.Printf("%#x\n", gommon.Ternary(false, byte(0x79), byte(0x6e)))
	// Output:
	// 0x79
	// 0x6e
}

func ExampleTernary_complex64() {
	fmt.Printf("%#v\n", gommon.Ternary(true, complex64(1.9), complex64(2.8)))
	fmt.Printf("%#v\n", gommon.Ternary(false, complex64(1.9), complex64(2.8)))
	// Output:
	// (1.9+0i)
	// (2.8+0i)
}

func ExampleTernary_complex128() {
	fmt.Printf("%#v\n", gommon.Ternary(true, complex128(1.9), complex128(2.8)))
	fmt.Printf("%#v\n", gommon.Ternary(false, complex128(1.9), complex128(2.8)))
	// Output:
	// (1.9+0i)
	// (2.8+0i)
}

func ExampleTernary_float32() {
	fmt.Printf("%#v\n", gommon.Ternary(true, float32(1.9), float32(2.8)))
	fmt.Printf("%#v\n", gommon.Ternary(false, float32(1.9), float32(2.8)))
	// Output:
	// 1.9
	// 2.8
}

func ExampleTernary_float64() {
	fmt.Printf("%#v\n", gommon.Ternary(true, 1.9, 2.8))
	fmt.Printf("%#v\n", gommon.Ternary(false, 1.9, 2.8))
	// Output:
	// 1.9
	// 2.8
}

func ExampleTernary_int() {
	fmt.Printf("%v\n", gommon.Ternary(true, 1, 2))
	fmt.Printf("%v\n", gommon.Ternary(false, 1, 2))
	// Output:
	// 1
	// 2
}

func ExampleTernary_int8() {
	fmt.Printf("%v\n", gommon.Ternary(true, int8(1), int8(2)))
	fmt.Printf("%v\n", gommon.Ternary(false, int8(1), int8(2)))
	// Output:
	// 1
	// 2
}

func ExampleTernary_int16() {
	fmt.Printf("%v\n", gommon.Ternary(true, int16(1), int16(2)))
	fmt.Printf("%v\n", gommon.Ternary(false, int16(1), int16(2)))
	// Output:
	// 1
	// 2
}

func ExampleTernary_int32() {
	fmt.Printf("%v\n", gommon.Ternary(true, int32(1), int32(2)))
	fmt.Printf("%v\n", gommon.Ternary(false, int32(1), int32(2)))
	// Output:
	// 1
	// 2
}

func ExampleTernary_int64() {
	fmt.Printf("%v\n", gommon.Ternary(true, int64(1), int64(2)))
	fmt.Printf("%v\n", gommon.Ternary(false, int64(1), int64(2)))
	// Output:
	// 1
	// 2
}

func ExampleTernary_runes() {
	fmt.Printf("%#x\n", gommon.Ternary(true, rune(0x79), rune(0x6e)))
	fmt.Printf("%#x\n", gommon.Ternary(false, rune(0x79), rune(0x6e)))
	// Output:
	// 0x79
	// 0x6e
}

func ExampleTernary_strings() {
	fmt.Printf("%s\n", gommon.Ternary(true, "True", "False"))
	fmt.Printf("%s\n", gommon.Ternary(false, "True", "False"))
	// Output:
	// True
	// False
}

func ExampleTernary_uint() {
	fmt.Printf("%v\n", gommon.Ternary(true, uint(1), uint(2)))
	fmt.Printf("%v\n", gommon.Ternary(false, uint(1), uint(2)))
	// Output:
	// 1
	// 2
}

func ExampleTernary_uint8() {
	fmt.Printf("%v\n", gommon.Ternary(true, uint8(1), uint8(2)))
	fmt.Printf("%v\n", gommon.Ternary(false, uint8(1), uint8(2)))
	// Output:
	// 1
	// 2
}

func ExampleTernary_uint16() {
	fmt.Printf("%v\n", gommon.Ternary(true, uint16(1), uint16(2)))
	fmt.Printf("%v\n", gommon.Ternary(false, uint16(1), uint16(2)))
	// Output:
	// 1
	// 2
}

func ExampleTernary_uint32() {
	fmt.Printf("%v\n", gommon.Ternary(true, uint32(1), uint32(2)))
	fmt.Printf("%v\n", gommon.Ternary(false, uint32(1), uint32(2)))
	// Output:
	// 1
	// 2
}

func ExampleTernary_uint64() {
	fmt.Printf("%v\n", gommon.Ternary(true, uint64(1), uint64(2)))
	fmt.Printf("%v\n", gommon.Ternary(false, uint64(1), uint64(2)))
	// Output:
	// 1
	// 2
}

func ExampleTernary_uintPtr() {
	fmt.Printf("%v\n", gommon.Ternary(true, uintptr(1), uintptr(2)))
	fmt.Printf("%v\n", gommon.Ternary(false, uintptr(1), uintptr(2)))
	// Output:
	// 1
	// 2
}

func ExampleUniq_bytes() {
	fmt.Printf("%#v\n", gommon.Uniq([]byte{0x61, 0x62, 0x61, 0x63}))
	// Output: []byte{0x61, 0x62, 0x63}
}

func ExampleUniq_complex64() {
	fmt.Printf("%v\n", gommon.Uniq([]complex64{1.9, 2.8, 1.9, 3.7}))
	// Output: [(1.9+0i) (2.8+0i) (3.7+0i)]
}

func ExampleUniq_complex128() {
	fmt.Printf("%v\n", gommon.Uniq([]complex128{1.9, 2.8, 1.9, 3.7}))
	// Output: [(1.9+0i) (2.8+0i) (3.7+0i)]
}

func ExampleUniq_float32() {
	fmt.Printf("%v\n", gommon.Uniq([]float32{1.9, 2.8, 1.9, 3.7}))
	// Output: [1.9 2.8 3.7]
}

func ExampleUniq_float64() {
	fmt.Printf("%v\n", gommon.Uniq([]float64{1.9, 2.8, 1.9, 3.7}))
	// Output: [1.9 2.8 3.7]
}

func ExampleUniq_int() {
	fmt.Printf("%v\n", gommon.Uniq([]int{1, 2, 1, 3}))
	// Output: [1 2 3]
}

func ExampleUniq_int8() {
	fmt.Printf("%v\n", gommon.Uniq([]int8{1, 2, 1, 3}))
	// Output: [1 2 3]
}

func ExampleUniq_int16() {
	fmt.Printf("%v\n", gommon.Uniq([]int16{1, 2, 1, 3}))
	// Output: [1 2 3]
}

func ExampleUniq_int32() {
	fmt.Printf("%v\n", gommon.Uniq([]int32{1, 2, 1, 3}))
	// Output: [1 2 3]
}

func ExampleUniq_int64() {
	fmt.Printf("%v\n", gommon.Uniq([]int64{1, 2, 1, 3}))
	// Output: [1 2 3]
}

func ExampleUniq_runes() {
	fmt.Printf("%#x\n", gommon.Uniq([]rune{0x61, 0x62, 0x61, 0x63}))
	// Output: [0x61 0x62 0x63]
}

func ExampleUniq_strings() {
	fmt.Printf("%v\n", gommon.Uniq([]string{"v1", "v2", "v1", "v3"}))
	// Output: [v1 v2 v3]
}

func ExampleUniq_uint() {
	fmt.Printf("%v\n", gommon.Uniq([]uint{1, 2, 1, 3}))
	// Output: [1 2 3]
}

func ExampleUniq_uint8() {
	fmt.Printf("%v\n", gommon.Uniq([]uint8{1, 2, 1, 3}))
	// Output: [1 2 3]
}

func ExampleUniq_uint16() {
	fmt.Printf("%v\n", gommon.Uniq([]uint16{1, 2, 1, 3}))
	// Output: [1 2 3]
}

func ExampleUniq_uint32() {
	fmt.Printf("%v\n", gommon.Uniq([]uint32{1, 2, 1, 3}))
	// Output: [1 2 3]
}

func ExampleUniq_uint64() {
	fmt.Printf("%v\n", gommon.Uniq([]uint64{1, 2, 1, 3}))
	// Output: [1 2 3]
}

func ExampleUniq_uintPtr() {
	fmt.Printf("%v\n", gommon.Uniq([]uintptr{1, 2, 1, 3}))
	// Output: [1 2 3]
}
