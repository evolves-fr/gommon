package gommon_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/evolves-fr/gommon"
)

func TestPathExist(t *testing.T) {
	assert.Equal(t, true, gommon.PathExist("./tests"))
	assert.Equal(t, false, gommon.PathExist("./unknown/"))
}

func TestPathEmpty(t *testing.T) {
	assert.Equal(t, false, gommon.PathEmpty("./tests"))
	assert.Equal(t, true, gommon.PathEmpty("./unknown/"))
}
