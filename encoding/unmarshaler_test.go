package encoding_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/evolves-fr/gommon/encoding"
)

func TestUnmarshal(t *testing.T) {
	var object Object

	assert.NoError(t, encoding.Unmarshal([]byte("test1|test2"), &object))
	assert.Equal(t, "test1", object.Value1)
	assert.Equal(t, "test2", object.Value2)

	assert.Error(t, encoding.Unmarshal([]byte("test1|test2"), struct{}{}))
}
