package store_test

import (
	"testing"

	"github.com/stretchr/testify/suite"
	"gitlab.com/evolves-fr/gommon/store"
)

type TestSuite struct {
	suite.Suite
}

func (suite *TestSuite) SetupSuite() {
	suite.Implements((*store.Store)(nil), store.StandardStore())
}

func (suite *TestSuite) test(key, value any) {
	// Check not exist
	suite.Equal(false, store.Has(key))
	suite.Nil(store.Get(key))

	// Set and check value
	store.Set(key, value)
	suite.Equal(true, store.Has(key))
	suite.Equal(value, store.Get(key))

	// Remove and check value
	store.Remove(key)
	suite.Equal(false, store.Has(key))
	suite.Nil(store.Get(key))
}

func (suite *TestSuite) TestUint8() {
	suite.test("uint8", uint8(255))
}

func (suite *TestSuite) TestUint16() {
	suite.test("uint16", uint16(65535))
}

func (suite *TestSuite) TestUint32() {
	suite.test("uint32", uint32(4294967295))
}

func (suite *TestSuite) TestUint64() {
	suite.test("uint64", uint64(18446744073709551615))
}

func (suite *TestSuite) TestInt8() {
	suite.test("int8", int8(127))
}

func (suite *TestSuite) TestInt16() {
	suite.test("int16", int16(32767))
}

func (suite *TestSuite) TestInt32() {
	suite.test("int32", int32(2147483647))
}

func (suite *TestSuite) TestInt64() {
	suite.test("int64", int64(9223372036854775807))
}

func (suite *TestSuite) TestFloat32() {
	suite.test("float32", float32(123456789.123456789))
}

func (suite *TestSuite) TestFloat64() {
	suite.test("float64", float64(123456789.123456789))
}

func (suite *TestSuite) TestComplex64() {
	suite.test("complex64", complex64(123456789.123456789))
}

func (suite *TestSuite) TestComplex128() {
	suite.test("complex128", complex128(123456789.123456789))
}

func (suite *TestSuite) TestString() {
	suite.test("string", "value")
}

func (suite *TestSuite) TestInt() {
	suite.test("int", 2147483647)
}

func (suite *TestSuite) TestUint() {
	suite.test("uint", uint(4294967295))
}

func (suite *TestSuite) TestUintPtr() {
	suite.test("uintptr", uintptr(4294967295))
}

func (suite *TestSuite) TestByte() {
	suite.test("byte", byte(0x64))
}

func (suite *TestSuite) TestRune() {
	suite.test("rune", rune(0x65))
}

func (suite *TestSuite) TestStruct() {
	type TestStruct struct {
		Value string
	}

	suite.test("struct", TestStruct{Value: "value"})
}

func (suite *TestSuite) TestSlice() {
	suite.test("slice", []string{"value1", "value2"})
}

func (suite *TestSuite) TestMap() {
	suite.test("map", map[string]string{"key1": "value1", "key2": "value2"})
}

func TestStore(t *testing.T) {
	t.Parallel()

	suite.Run(t, new(TestSuite))
}
