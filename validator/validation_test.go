package validator_test

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/evolves-fr/gommon/validator"
)

func Test_isDuration(t *testing.T) {
	t.Parallel()

	const tag = "duration"

	// Valid
	for _, value := range []any{
		"1s",
		"1h30s",
	} {
		value := value

		t.Run(fmt.Sprintf("%v", value), func(t *testing.T) {
			t.Parallel()

			assert.NoError(t, validator.New().Var(value, tag))
		})
	}

	// Invalid
	for _, value := range []any{
		"test",
		123456789,
	} {
		value := value

		t.Run(fmt.Sprintf("%v", value), func(t *testing.T) {
			t.Parallel()

			assert.Error(t, validator.New().Var(value, tag))
		})
	}
}

func Test_isDomain(t *testing.T) {
	t.Parallel()

	const tag = "domain"

	// Valid
	for _, value := range []string{
		"example.com",
		"_25._tcp.SRV.example",
		"punycoded-idna.xn--zckzah",
		"under_score.example",
		"example.",
		"192.0.2.1.",
		"digit.1example.",
		"underscore._example_com.",
	} {
		value := value

		t.Run(value, func(t *testing.T) {
			t.Parallel()

			assert.NoError(t, validator.New().Var(value, tag))
		})
	}

	// Invalid
	for _, value := range []string{
		"192.0.2.1",
		"has spaces.com",
		"easy,typo.example",
		`domain\.escapes.invalid`,
		"no_trailing_.invalid",
		"-leading-or-trailing-.hyphens.invalid",
		"example",
		"digit.1example",
		"underscore._example_com",
	} {
		value := value

		t.Run(value, func(t *testing.T) {
			t.Parallel()

			assert.Error(t, validator.New().Var(value, tag))
		})
	}
}
