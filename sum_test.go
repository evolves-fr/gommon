package gommon_test

import (
	"testing"

	"github.com/stretchr/testify/suite"
	"gitlab.com/evolves-fr/gommon"
)

func TestSum(t *testing.T) {
	t.Parallel()

	suite.Run(t, new(SumSuite))
}

type SumSuite struct{ suite.Suite }

func (suite *SumSuite) TestHash() {
	res := gommon.SumFn(nil, []byte("hello world"))
	suite.Equal("", res)
}

func (suite *SumSuite) TestInvalid() {
	res, err := gommon.Sum("invalid", []byte("hello world"))
	suite.Error(err)
	suite.Equal("", res)
}

func (suite *SumSuite) TestMD5() {
	res, err := gommon.Sum("md5", []byte("hello world"))
	suite.NoError(err)
	suite.Equal("5eb63bbbe01eeed093cb22bb8f5acdc3", res)
}

func (suite *SumSuite) TestSHA1() {
	res, err := gommon.Sum("sha1", []byte("hello world"))
	suite.NoError(err)
	suite.Equal("2aae6c35c94fcfb415dbe95f408b9ce91ee846ed", res)
}

func (suite *SumSuite) TestSHA256() {
	res, err := gommon.Sum("sha256", []byte("hello world"))
	suite.NoError(err)
	suite.Equal("b94d27b9934d3e08a52e52d7da7dabfac484efe37a5380ee9088f7ace2efcde9", res)
}

func (suite *SumSuite) TestSHA512() {
	res, err := gommon.Sum("sha512", []byte("hello world"))
	suite.NoError(err)
	suite.Equal("309ecc489c12d6eb4cc40f50c902f2b4d0ed77ee511a7c7a9bcd3ca86d4cd86f989dd35bc5ff499670da34255b45b0cfd830e81f605dcf7dc5542e93ae9cd76f", res)
}
