package env

import (
	"errors"
	"io"
)

func NewEncoder(w io.Writer) *Encoder {
	return &Encoder{w: w}
}

type Encoder struct {
	w io.Writer
}

func (enc *Encoder) Encode(v map[string]string) error {
	if v == nil {
		return errors.New("values is nil")
	}

	if _, err := enc.w.Write(Environment(v).Encode()); err != nil {
		return err
	}

	return nil
}
