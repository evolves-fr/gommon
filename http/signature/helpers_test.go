package signature_test

import (
	"bytes"
	"context"
	"net/http"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"gitlab.com/evolves-fr/gommon/http/headers"
	"gitlab.com/evolves-fr/gommon/http/signature"
)

const (
	url       = "http://test.localhost/resource"
	validDate = "Mon, 02 Jan 2006 15:04:05 MST"
)

func TestStandardSignature(t *testing.T) {
	t.Parallel()

	assert.Implements(t, (*signature.Signature)(nil), signature.StandardSignature())
}

func TestGenerate(t *testing.T) {
	t.Parallel()

	tests := []struct {
		Name     string
		Method   string
		Body     []byte
		Expected string
	}{
		{
			Name:     "get",
			Method:   http.MethodGet,
			Expected: "APIAuth clientID:SCUPXAjVIxZFkBBnDmPuENYjlTmF+u0dVskXc9drpv0=",
		},
		{
			Name:     "post",
			Method:   http.MethodPost,
			Body:     []byte(`{"key":"values","array":["value1","value2"]}`),
			Expected: "APIAuth clientID:ttt0XkGeuKCXcUEZPRum14pOuThLeBTTiZ75BAelhF4=",
		},
	}

	for _, test := range tests {
		test := test

		t.Run(test.Name, func(t *testing.T) {
			t.Parallel()

			req, err := http.NewRequestWithContext(context.Background(), test.Method, url, bytes.NewReader(test.Body))
			if err != nil {
				t.Fatal(err)
			}

			req.Header.Set(headers.Date, validDate)
			req.Header.Set(headers.ContentType, "application/json")

			assert.Equal(t, test.Expected, signature.Generate(req, "clientID", "secretID"))
		})
	}
}

func TestSign(t *testing.T) {
	t.Parallel()

	tests := []struct {
		Name     string
		Method   string
		Body     []byte
		Expected string
	}{
		{
			Name:     "get",
			Method:   http.MethodGet,
			Expected: "APIAuth clientID:SCUPXAjVIxZFkBBnDmPuENYjlTmF+u0dVskXc9drpv0=",
		},
		{
			Name:     "post",
			Method:   http.MethodPost,
			Body:     []byte(`{"key":"values","array":["value1","value2"]}`),
			Expected: "APIAuth clientID:ttt0XkGeuKCXcUEZPRum14pOuThLeBTTiZ75BAelhF4=",
		},
	}

	for _, test := range tests {
		test := test

		t.Run(test.Name, func(t *testing.T) {
			t.Parallel()

			req, err := http.NewRequestWithContext(context.Background(), test.Method, url, bytes.NewReader(test.Body))
			if err != nil {
				t.Fatal(err)
			}

			req.Header.Set(headers.Date, validDate)
			req.Header.Set(headers.ContentType, "application/json")

			signature.Sign(req, "clientID", "secretID")

			assert.Equal(t, test.Expected, req.Header.Get(headers.Authorization))
		})
	}
}

func TestCheck(t *testing.T) {
	t.Parallel()

	const (
		invalidDate         = "2006-01-02T15:04:05Z07:00"
		validAuth           = "APIAuth clientID:secretID"
		invalidAuthPrefix   = "Auth clientID:secretID"
		invalidAuthClientID = "APIAuth unknown:secretID"
	)

	var (
		date        = time.Now().UTC().Format(time.RFC1123)
		credentials = map[string]string{"clientID": "secretID"}
	)

	tests := []struct {
		Name          string
		Date          string
		Authorization string
		Sign          bool
		Error         string
	}{
		{Name: "not-authenticated", Date: validDate, Authorization: "", Error: "not authenticated"},
		{Name: "malformed-prefix", Date: validDate, Authorization: invalidAuthPrefix, Error: "authorization malformed"},
		{Name: "malformed-date-empty", Date: "", Authorization: validAuth, Error: "unknown date"},
		{Name: "malformed-date-format", Date: invalidDate, Authorization: validAuth, Error: "invalid date"},
		{Name: "unknown-clientID", Date: validDate, Authorization: invalidAuthClientID, Error: "unauthorized"},
		{Name: "expired", Date: validDate, Authorization: validAuth, Error: "authorization expired"},
		{Name: "invalid", Date: date, Authorization: validAuth, Error: "unauthorized"},
		{Name: "valid", Date: date, Sign: true},
	}

	for _, test := range tests {
		test := test

		t.Run(test.Name, func(t *testing.T) {
			t.Parallel()

			req, _ := http.NewRequestWithContext(context.Background(), http.MethodGet, url, nil)

			if len(test.Date) > 0 {
				req.Header.Set(headers.Date, test.Date)
			}

			if len(test.Authorization) > 0 {
				req.Header.Set(headers.Authorization, test.Authorization)
			}

			if test.Sign {
				signature.Sign(req, "clientID", "secretID")
			}

			err := signature.Check(req, credentials)

			if len(test.Error) > 0 {
				assert.EqualError(t, signature.Check(req, credentials), test.Error)
			} else {
				assert.NoError(t, err)
			}
		})
	}
}
