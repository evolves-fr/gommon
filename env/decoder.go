package env

import (
	"fmt"
	"io"
	"reflect"
	"strconv"
	"strings"

	"gitlab.com/evolves-fr/gommon"
)

func NewDecoder(r io.Reader) *Decoder {
	return &Decoder{r: r}
}

type Decoder struct {
	r      io.Reader
	env    Env
	prefix string
}

func (dec *Decoder) Prefix(prefix string) *Decoder {
	dec.prefix = prefix

	return dec
}

func (dec *Decoder) Decode(v any) error {
	var err error
	dec.env, err = LoadDotEnv(dec.r)
	if err != nil {
		return err
	}

	return dec.bind(reflect.Indirect(reflect.ValueOf(v)), "", strings.ToUpper(dec.prefix), "")
}

func (dec *Decoder) bind(vo reflect.Value, name, key, defaultValue string) error {
	// Check reflect.Invalid
	if !vo.IsValid() {
		return fmt.Errorf("type is invalid")
	}

	// Get name
	if gommon.Empty(name) {
		name = vo.Type().Name()
	}

	// Verify if value is writable
	if !vo.CanAddr() || !vo.CanInterface() {
		return fmt.Errorf("%s should be addressable", vo.Type().Name())
	}

	// Value
	value := gommon.Default(dec.env.Get(key), defaultValue)

	// Unmarshaler interface.
	switch unmarshalerType := reflect.TypeOf((*Unmarshaler)(nil)).Elem(); {
	case vo.Type().Implements(unmarshalerType):
		return dec.unmarshal(vo, value, false)
		//return dec.item(vo, value).unmarshal(reflect.New(vo.Type().Elem()), false)
	case reflect.PtrTo(vo.Type()).Implements(unmarshalerType):
		return dec.unmarshal(vo, value, true)
		//return dec.item(vo, value).unmarshal(reflect.New(vo.Type()), true)
	}

	// Process by type
	switch vo.Type().Kind() {
	case reflect.Pointer:
		return dec.bind(vo.Elem(), name, key, defaultValue)
	case reflect.Bool:
		return dec.apply(vo, value, gommon.WrapParse[bool])
	case reflect.Int:
		return dec.apply(vo, value, gommon.WrapParse[int])
	case reflect.Int8:
		return dec.apply(vo, value, gommon.WrapParse[int8])
	case reflect.Int16:
		return dec.apply(vo, value, gommon.WrapParse[int16])
	case reflect.Int32:
		return dec.apply(vo, value, gommon.WrapParse[int32])
	case reflect.Int64:
		return dec.apply(vo, value, gommon.WrapParse[int64])
	case reflect.Uint:
		return dec.apply(vo, value, gommon.WrapParse[uint])
	case reflect.Uint8:
		return dec.apply(vo, value, gommon.WrapParse[uint8])
	case reflect.Uint16:
		return dec.apply(vo, value, gommon.WrapParse[uint16])
	case reflect.Uint32:
		return dec.apply(vo, value, gommon.WrapParse[uint32])
	case reflect.Uint64:
		return dec.apply(vo, value, gommon.WrapParse[uint64])
	case reflect.Uintptr:
		return dec.apply(vo, value, gommon.WrapParse[uintptr])
	case reflect.Float32:
		return dec.apply(vo, value, gommon.WrapParse[float32])
	case reflect.Float64:
		return dec.apply(vo, value, gommon.WrapParse[float64])
	case reflect.Complex64:
		return dec.apply(vo, value, gommon.WrapParse[complex64])
	case reflect.Complex128:
		return dec.apply(vo, value, gommon.WrapParse[complex128])
	case reflect.String:
		return dec.apply(vo, value, gommon.WrapParse[string])
	case reflect.Array:
		for i := 0; i < vo.Type().Len(); i++ {
			var (
				itemName = name + "[" + strconv.Itoa(i) + "]"
				tagValue = dec.parseTag(key, "", "", strconv.Itoa(i))
			)

			if err := dec.bind(vo.Index(i), itemName, tagValue, defaultValue); err != nil {
				return err
			}
		}

		return nil
	case reflect.Map:
		if vo.IsNil() {
			vo.Set(reflect.MakeMap(vo.Type()))
		}

		for _, envKey := range gommon.Uniq(dec.env.KeysWithRegex(fmt.Sprintf(`^(%s_[^_]+).*$`, key))) {
			var (
				id       = strings.ToLower(strings.TrimPrefix(envKey, key+"_"))
				itemName = name + "[" + id + "]"
			)

			mapKey := reflect.Indirect(reflect.New(vo.Type().Key()))
			mapKey.Set(reflect.ValueOf(id))

			mapValue := reflect.Indirect(reflect.New(vo.Type().Elem()))
			if err := dec.bind(mapValue, itemName, envKey, defaultValue); err != nil {
				return err
			}

			vo.SetMapIndex(mapKey, mapValue)
		}

		return nil
	case reflect.Slice:
		envKeys := gommon.Uniq(dec.env.KeysWithRegex(fmt.Sprintf(`^(%s_[^_]+).*$`, key)))

		if len(envKeys) > 0 {
			vo.Set(reflect.MakeSlice(vo.Type(), len(envKeys), len(envKeys)))

			for _, envKey := range envKeys {
				index, err := gommon.Parse[int](strings.TrimPrefix(envKey, key+"_"))
				if err != nil {
					vo.SetLen(vo.Len() - 1)

					return fmt.Errorf("slice key is invalid (%s)", strings.TrimPrefix(envKey, key+"_"))
				}

				var itemName = name + "[" + strconv.Itoa(index) + "]"

				if err = dec.bind(vo.Index(index), itemName, envKey, defaultValue); err != nil {
					vo.SetLen(vo.Len() - 1)

					return err
				}
			}
		} else if !gommon.Empty(defaultValue) {
			list := strings.Split(defaultValue, ",")

			vo.Set(reflect.MakeSlice(vo.Type(), len(list), len(list)))

			for i := 0; i < len(list); i++ {
				var itemName = name + "[" + strconv.Itoa(i) + "]"
				if err := dec.bind(vo.Index(i), itemName, "", list[i]); err != nil {
					vo.SetLen(vo.Len() - 1)

					return err
				}
			}
		}

		return nil
	case reflect.Struct:
		for i := 0; i < vo.Type().NumField(); i++ {
			vof := vo.Type().Field(i)

			defaultValue = vof.Tag.Get("default")

			var (
				itemName = name + "." + vof.Name
				tagValue = dec.parseTag(key, vof.Name, vof.Tag.Get("env"), "")
			)

			if err := dec.bind(vo.Field(i), itemName, tagValue, defaultValue); err != nil {
				return err
			}
		}

		return nil
	default:
		return fmt.Errorf("type `%s` not available", vo.Type().Kind().String())
	}
}

func (dec *Decoder) apply(vo reflect.Value, value string, fn func(value string) (any, error)) error {
	if gommon.Empty(value) {
		return nil
	}

	res, err := fn(value)
	if err != nil {
		return err
	}

	vo.Set(reflect.ValueOf(res))

	return nil
}

func (dec *Decoder) unmarshal(vo reflect.Value, value string, indirect bool) error {
	typeOf := vo.Type()
	if !indirect {
		typeOf = typeOf.Elem()
	}

	valueOf := reflect.New(typeOf)

	if err := valueOf.Interface().(Unmarshaler).UnmarshalENV([]byte(value)); err != nil {
		return err
	}

	if indirect {
		vo.Set(reflect.Indirect(valueOf))
	} else {
		vo.Set(valueOf)
	}

	return nil
}

func (dec *Decoder) parseTag(parent, name, value, index string) string {
	// Split tag with comma
	tagValues := strings.Split(value, ",")
	if gommon.Has("inline", tagValues...) {
		return parent
	}

	// Get tag key value
	suffix := gommon.Ternary(len(tagValues) > 0, tagValues[0], "")

	// Set default struct name instead of tag key
	tag := strings.ToUpper(gommon.Default(suffix, name))

	// Prepare final key
	key := parent

	// Set tag suffix
	if !gommon.Empty(tag) {
		if !gommon.Empty(key) {
			key += "_"
		}

		key += tag
	}

	// Set index suffix
	if !gommon.Empty(index) {
		if !gommon.Empty(key) {
			key += "_"
		}

		key += index
	}

	return key
}
