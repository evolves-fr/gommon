package utils_test

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/evolves-fr/gommon/utils"
)

func TestDomainLevel(t *testing.T) {
	t.Parallel()

	// Valid
	for _, test := range []struct {
		Expected any
		Domain   string
		Level    int
	}{
		{Expected: "org", Domain: "fr.wikipedia.org", Level: 1},
		{Expected: "wikipedia.org", Domain: "fr.wikipedia.org", Level: 2},
		{Expected: "fr.wikipedia.org", Domain: "fr.wikipedia.org", Level: 3},
	} {
		test := test

		t.Run(fmt.Sprintf("%s(%d)", test.Domain, test.Level), func(t *testing.T) {
			t.Parallel()

			assert.Equal(t, test.Expected, utils.DomainLevel(test.Domain, test.Level))
		})
	}

	// Invalid
	for _, test := range []struct {
		Domain string
		Level  int
	}{
		{Domain: "fr.wikipedia.org", Level: 0},
		{Domain: "fr.wikipedia.org", Level: 4},
		{Domain: "192.168.0.1", Level: 4},
	} {
		test := test

		t.Run(fmt.Sprintf("%s(%d)", test.Domain, test.Level), func(t *testing.T) {
			t.Parallel()

			assert.Equal(t, "", utils.DomainLevel(test.Domain, test.Level))
		})
	}
}

func TestTLD(t *testing.T) {
	t.Parallel()

	// Valid
	for value, expected := range map[string]string{
		"example.com":               "com",
		"_25._tcp.SRV.example":      "example",
		"punycoded-idna.xn--zckzah": "xn--zckzah",
		"under_score.example":       "example",
		"example.":                  "example",
		"192.0.2.1.":                "",
		"digit.1example.":           "1example",
		"underscore._example_com.":  "_example_com",
	} {
		value, expected := value, expected

		t.Run(value, func(t *testing.T) {
			t.Parallel()

			assert.Equal(t, expected, utils.TLD(value))
		})
	}

	// Invalid
	for _, value := range []string{
		"192.0.2.1",
		"has spaces.com",
		"easy,typo.example",
		`domain\.escapes.invalid`,
		"no_trailing_.invalid",
		"-leading-or-trailing-.hyphens.invalid",
		"example",
		"digit.1example",
		"underscore._example_com",
	} {
		value := value

		t.Run(value, func(t *testing.T) {
			t.Parallel()

			assert.Equal(t, "", utils.TLD(value))
		})
	}
}
