package handlers

import "net/http"

func StringHandler(str string) http.Handler {
	return stringHandler(str)
}

type stringHandler string

func (h stringHandler) ServeHTTP(w http.ResponseWriter, _ *http.Request) {
	_, _ = w.Write([]byte(h))
}
