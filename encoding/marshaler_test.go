package encoding_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/evolves-fr/gommon/encoding"
)

func TestMarshal(t *testing.T) {
	object := &Object{Value1: "test1", Value2: "test2"}

	res, err := encoding.Marshal(object)
	assert.NoError(t, err)
	assert.Equal(t, "test1|test2", string(res))

	res, err = encoding.Marshal([]string{"test1", "test2"})
	assert.Error(t, err)
	assert.Nil(t, res)
}
