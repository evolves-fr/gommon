package signature

import (
	"bytes"
	"crypto/hmac"
	"crypto/sha256"
	"encoding/base64"
	"encoding/hex"
	"fmt"
	"io/ioutil"
	"net/http"
	"strings"
	"time"

	"gitlab.com/evolves-fr/gommon"
	"gitlab.com/evolves-fr/gommon/errors"
	"gitlab.com/evolves-fr/gommon/http/headers"
)

var (
	ErrNotAuthenticated       = errors.New("not authenticated")
	ErrUnauthorized           = errors.New("unauthorized")
	ErrUnknownDate            = errors.New("unknown date")
	ErrInvalidDate            = errors.New("invalid date")
	ErrAuthorizationMalformed = errors.New("authorization malformed")
	ErrAuthorizationExpired   = errors.New("authorization expired")
)

type Signature interface {
	Generate(req *http.Request, clientID, secretID string) string
	Sign(req *http.Request, clientID, secretID string)
	Check(req *http.Request, credentials map[string]string) error
}

func New(prefix string, duration time.Duration) Signature {
	return &signature{
		Prefix:   prefix,
		Duration: duration,
	}
}

type signature struct {
	Prefix   string
	Duration time.Duration
}

func (s *signature) Generate(req *http.Request, clientID, secretID string) string {
	// Get body
	var (
		payload    []byte
		payloadSum string
	)

	if req.Body != nil {
		// Read reader content to bytes
		payload, _ = ioutil.ReadAll(req.Body)

		// Reset reader
		req.Body = ioutil.NopCloser(bytes.NewBuffer(payload))

		// Check body content
		if payload != nil && !gommon.Empty(string(payload)) {
			// Sum body content
			payloadHexSum := sha256.Sum256(payload)

			// Return body content sum
			payloadSum = hex.EncodeToString(payloadHexSum[:])
		}
	}

	// Format request information values
	signatureRequest := fmt.Sprintf("%s,%s,%s,%s,%s",
		req.Method,
		req.URL.RequestURI(),
		req.Header.Get(headers.ContentType),
		gommon.Default(req.Header.Get(headers.Date), req.Header.Get("X-Date")),
		payloadSum,
	)

	// Generate SHA256 HMAC to Base64
	signatureRequestHmac := hmac.New(sha256.New, []byte(secretID))
	_, _ = signatureRequestHmac.Write([]byte(signatureRequest))
	signatureRequestSumHash := base64.StdEncoding.EncodeToString(signatureRequestHmac.Sum(nil))

	// Return Authorization value
	return fmt.Sprintf("%s %s:%s", s.Prefix, clientID, signatureRequestSumHash)
}

func (s *signature) Sign(req *http.Request, clientID, secretID string) {
	// Set authorization header with signature hash
	req.Header.Set(headers.Authorization, s.Generate(req, clientID, secretID))
}

func (s *signature) Check(req *http.Request, credentials map[string]string) error {
	// Get request information
	var (
		date          = gommon.Default(req.Header.Get(headers.Date), req.Header.Get("X-Date"))
		authorization = req.Header.Get(headers.Authorization)
	)

	// Verify authorization
	if len(authorization) == 0 {
		return ErrNotAuthenticated
	}

	// Check request information
	if !strings.HasPrefix(authorization, s.Prefix) {
		return ErrAuthorizationMalformed
	}

	if date == "" {
		return ErrUnknownDate
	}
	// Get client id request
	clientID := strings.Split(strings.TrimSpace(strings.TrimPrefix(authorization, s.Prefix)), ":")[0]

	// Find clientID in credentials
	secretID, ok := credentials[clientID]
	if !ok {
		return ErrUnauthorized
	}

	// Decode date header
	datetime, err := time.Parse(time.RFC1123, date)
	if err != nil {
		return ErrInvalidDate
	}

	// Check date expiration
	if now := time.Now().UTC(); !(datetime.UTC().After(now.Add(-s.Duration)) && datetime.Before(now)) {
		return ErrAuthorizationExpired
	}

	// Verifying signature
	if !strings.EqualFold(authorization, s.Generate(req, clientID, secretID)) {
		return ErrUnauthorized
	}

	return nil
}
