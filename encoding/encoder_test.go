package encoding_test

import (
	"bytes"
	"errors"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/evolves-fr/gommon/encoding"
	"gitlab.com/evolves-fr/gommon/tests"
)

func TestNewEncoder(t *testing.T) {
	var buffer bytes.Buffer

	assert.NoError(t, encoding.NewEncoder(&buffer).Encode(&Object{Value1: "test1", Value2: "test2"}))
	assert.Equal(t, "test1|test2", buffer.String())

	assert.Error(t, encoding.NewEncoder(nil).Encode(struct{}{}))
	assert.Error(t, encoding.NewEncoder(tests.ErrReadWriter(errors.New("writeError"))).Encode(&Object{}))
}
