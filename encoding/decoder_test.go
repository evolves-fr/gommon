package encoding_test

import (
	"errors"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/evolves-fr/gommon/encoding"
	"gitlab.com/evolves-fr/gommon/tests"
)

func TestNewDecoder(t *testing.T) {
	var object Object

	assert.NoError(t, encoding.NewDecoder(strings.NewReader("test1|test2")).Decode(&object))
	assert.Equal(t, "test1", object.Value1)
	assert.Equal(t, "test2", object.Value2)

	assert.Error(t, encoding.NewDecoder(nil).Decode(struct{}{}))
	assert.Error(t, encoding.NewDecoder(tests.ErrReadWriter(errors.New("readError"))).Decode(struct{}{}))
	assert.Error(t, encoding.NewDecoder(strings.NewReader("test1|test2")).Decode(struct{}{}))
}
