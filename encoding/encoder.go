package encoding

import "io"

func NewEncoder(w io.Writer) *Encoder {
	return &Encoder{w: w}
}

type Encoder struct {
	w io.Writer
}

func (enc *Encoder) Encode(v any) error {
	data, err := Marshal(v)
	if err != nil {
		return err
	}

	if _, err = enc.w.Write(data); err != nil {
		return err
	}

	return nil
}
