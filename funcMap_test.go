package gommon_test

import (
	"testing"

	"github.com/stretchr/testify/suite"
	"gitlab.com/evolves-fr/gommon"
)

func TestFuncMap(t *testing.T) {
	t.Parallel()

	suite.Run(t, new(FuncMapSuite))
}

type FuncMapSuite struct {
	suite.Suite
}

func (suite *FuncMapSuite) TestBool() {
	fn, ok := gommon.FuncMap()["bool"]
	suite.Equal(true, ok)
	suite.NotNil(fn)
}

func (suite *FuncMapSuite) TestInt() {
	fn, ok := gommon.FuncMap()["int"]
	suite.Equal(true, ok)
	suite.NotNil(fn)
}

func (suite *FuncMapSuite) TestInt8() {
	fn, ok := gommon.FuncMap()["int8"]
	suite.Equal(true, ok)
	suite.NotNil(fn)
}

func (suite *FuncMapSuite) TestInt16() {
	fn, ok := gommon.FuncMap()["int16"]
	suite.Equal(true, ok)
	suite.NotNil(fn)
}

func (suite *FuncMapSuite) TestInt32() {
	fn, ok := gommon.FuncMap()["int32"]
	suite.Equal(true, ok)
	suite.NotNil(fn)
}

func (suite *FuncMapSuite) TestInt64() {
	fn, ok := gommon.FuncMap()["int64"]
	suite.Equal(true, ok)
	suite.NotNil(fn)
}

func (suite *FuncMapSuite) TestUint() {
	fn, ok := gommon.FuncMap()["uint"]
	suite.Equal(true, ok)
	suite.NotNil(fn)
}

func (suite *FuncMapSuite) TestUint8() {
	fn, ok := gommon.FuncMap()["uint8"]
	suite.Equal(true, ok)
	suite.NotNil(fn)
}

func (suite *FuncMapSuite) TestUint16() {
	fn, ok := gommon.FuncMap()["uint16"]
	suite.Equal(true, ok)
	suite.NotNil(fn)
}

func (suite *FuncMapSuite) TestUint32() {
	fn, ok := gommon.FuncMap()["uint32"]
	suite.Equal(true, ok)
	suite.NotNil(fn)
}

func (suite *FuncMapSuite) TestUint64() {
	fn, ok := gommon.FuncMap()["uint64"]
	suite.Equal(true, ok)
	suite.NotNil(fn)
}

func (suite *FuncMapSuite) TestUintPtr() {
	fn, ok := gommon.FuncMap()["uintPtr"]
	suite.Equal(true, ok)
	suite.NotNil(fn)
}

func (suite *FuncMapSuite) TestFloat32() {
	fn, ok := gommon.FuncMap()["float32"]
	suite.Equal(true, ok)
	suite.NotNil(fn)
}

func (suite *FuncMapSuite) TestFloat64() {
	fn, ok := gommon.FuncMap()["float64"]
	suite.Equal(true, ok)
	suite.NotNil(fn)
}

func (suite *FuncMapSuite) TestComplex64() {
	fn, ok := gommon.FuncMap()["complex64"]
	suite.Equal(true, ok)
	suite.NotNil(fn)
}

func (suite *FuncMapSuite) TestComplex128() {
	fn, ok := gommon.FuncMap()["complex128"]
	suite.Equal(true, ok)
	suite.NotNil(fn)
}

func (suite *FuncMapSuite) TestDuration() {
	fn, ok := gommon.FuncMap()["duration"]
	suite.Equal(true, ok)
	suite.NotNil(fn)
}

func (suite *FuncMapSuite) TestTime() {
	fn, ok := gommon.FuncMap()["time"]
	suite.Equal(true, ok)
	suite.NotNil(fn)
}

func (suite *FuncMapSuite) TestTimestamp() {
	fn, ok := gommon.FuncMap()["timestamp"]
	suite.Equal(true, ok)
	suite.NotNil(fn)
}

func (suite *FuncMapSuite) TestDSN() {
	fn, ok := gommon.FuncMap()["dsn"]
	suite.Equal(true, ok)
	suite.NotNil(fn)
}

func (suite *FuncMapSuite) TestBase64() {
	fn, ok := gommon.FuncMap()["base64"]
	suite.Equal(true, ok)
	suite.NotNil(fn)
}

func (suite *FuncMapSuite) TestURL() {
	fn, ok := gommon.FuncMap()["url"]
	suite.Equal(true, ok)
	suite.NotNil(fn)
}

func (suite *FuncMapSuite) TestValues() {
	fn, ok := gommon.FuncMap()["values"]
	suite.Equal(true, ok)
	suite.NotNil(fn)
}

func (suite *FuncMapSuite) TestDefault() {
	fn, ok := gommon.FuncMap()["default"]
	suite.Equal(true, ok)
	suite.NotNil(fn)
}

func (suite *FuncMapSuite) TestTernary() {
	fn, ok := gommon.FuncMap()["ternary"]
	suite.Equal(true, ok)
	suite.NotNil(fn)
}

func (suite *FuncMapSuite) TestEmpty() {
	fn, ok := gommon.FuncMap()["empty"]
	suite.Equal(true, ok)
	suite.NotNil(fn)
}

func (suite *FuncMapSuite) TestCompact() {
	fn, ok := gommon.FuncMap()["compact"]
	suite.Equal(true, ok)
	suite.NotNil(fn)
	suite.Equal([]string{"value1", "value2"}, fn.(func(v []string) []string)([]string{"", "value1", "", "value2", ""}))
}

func (suite *FuncMapSuite) TestENV() {
	fn, ok := gommon.FuncMap()["env"]
	suite.Equal(true, ok)
	suite.NotNil(fn)
}

func (suite *FuncMapSuite) TestIndent() {
	fn, ok := gommon.FuncMap()["indent"]
	suite.Equal(true, ok)
	suite.NotNil(fn)
}

func (suite *FuncMapSuite) TestNIndent() {
	fn, ok := gommon.FuncMap()["nindent"]
	suite.Equal(true, ok)
	suite.NotNil(fn)
}

func (suite *FuncMapSuite) TestRegexMatch() {
	fn, ok := gommon.FuncMap()["regexMatch"]
	suite.Equal(true, ok)
	suite.NotNil(fn)
}

func (suite *FuncMapSuite) TestContains() {
	fn, ok := gommon.FuncMap()["contains"]
	suite.Equal(true, ok)
	suite.NotNil(fn)
}

func (suite *FuncMapSuite) TestHasPrefix() {
	fn, ok := gommon.FuncMap()["hasPrefix"]
	suite.Equal(true, ok)
	suite.NotNil(fn)
}

func (suite *FuncMapSuite) TestHasSuffix() {
	fn, ok := gommon.FuncMap()["hasSuffix"]
	suite.Equal(true, ok)
	suite.NotNil(fn)
}

func (suite *FuncMapSuite) TestUpper() {
	fn, ok := gommon.FuncMap()["upper"]
	suite.Equal(true, ok)
	suite.NotNil(fn)
}

func (suite *FuncMapSuite) TestLower() {
	fn, ok := gommon.FuncMap()["lower"]
	suite.Equal(true, ok)
	suite.NotNil(fn)
}

func (suite *FuncMapSuite) TestTitle() {
	fn, ok := gommon.FuncMap()["title"]
	suite.Equal(true, ok)
	suite.NotNil(fn)
}

func (suite *FuncMapSuite) TestSplit() {
	fn, ok := gommon.FuncMap()["split"]
	suite.Equal(true, ok)
	suite.NotNil(fn)
}

func (suite *FuncMapSuite) TestNow() {
	fn, ok := gommon.FuncMap()["now"]
	suite.Equal(true, ok)
	suite.NotNil(fn)
}

func (suite *FuncMapSuite) TestPathBase() {
	fn, ok := gommon.FuncMap()["pathBase"]
	suite.Equal(true, ok)
	suite.NotNil(fn)
}

func (suite *FuncMapSuite) TestPathDir() {
	fn, ok := gommon.FuncMap()["pathDir"]
	suite.Equal(true, ok)
	suite.NotNil(fn)
}

func (suite *FuncMapSuite) TestPathClean() {
	fn, ok := gommon.FuncMap()["pathClean"]
	suite.Equal(true, ok)
	suite.NotNil(fn)
}

func (suite *FuncMapSuite) TestPathExt() {
	fn, ok := gommon.FuncMap()["pathExt"]
	suite.Equal(true, ok)
	suite.NotNil(fn)
}

func (suite *FuncMapSuite) TestPathIsAbs() {
	fn, ok := gommon.FuncMap()["pathIsAbs"]
	suite.Equal(true, ok)
	suite.NotNil(fn)
}

func (suite *FuncMapSuite) TestPathExist() {
	fn, ok := gommon.FuncMap()["pathExist"]
	suite.Equal(true, ok)
	suite.NotNil(fn)
}

func (suite *FuncMapSuite) TestPathEmpty() {
	fn, ok := gommon.FuncMap()["pathEmpty"]
	suite.Equal(true, ok)
	suite.NotNil(fn)
}

func (suite *FuncMapSuite) TestSemverCompare() {
	fn, ok := gommon.FuncMap()["semverCompare"]
	suite.Equal(true, ok)
	suite.NotNil(fn)
}
