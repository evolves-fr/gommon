package gommon

import (
	"errors"
	"fmt"
	"net/url"
	"reflect"
	"strconv"
	"strings"
	"time"
	"unsafe"
)

// Compact accepts a list and removes entries with empty values.
func Compact[T comparable](values []T) []T {
	if values == nil {
		return nil
	}

	list := make([]T, 0)

	for _, value := range values {
		if !Empty(value) {
			list = append(list, value)
		}
	}

	return list
}

// Default takes two values.
// If the first value is not empty, it will be returned, else the second value will be returned.
func Default[T any](v1, v2 T) T {
	return Ternary(Empty(v1), v2, v1)
}

// Empty check the value and return true if value is non-empty.
func Empty[T any](v T) bool {
	vo := reflect.ValueOf(v)

	return reflect.DeepEqual(vo.Interface(), reflect.Zero(vo.Type()).Interface())
}

// First item of slice returned
func First[T any](slice []T) T {
	if len(slice) > 0 {
		return slice[0]
	}

	return Zero[T]()
}

// Has test to see if a list has a particular element.
func Has[T comparable](value T, values ...T) bool {
	if values == nil {
		return false
	}

	for _, v := range values {
		if v == value {
			return true
		}
	}

	return false
}

// MustParse return parsed value, or if error reflect.Zero value.
func MustParse[T any](value string) T {
	v, _ := Parse[T](value)

	return v
}

// Parse string value to standard or underlying type.
func Parse[T any](value string) (T, error) {
	// Result is helper
	Result := func(val any, err error) (T, error) {
		typeOf := reflect.TypeOf(new(T)).Elem()

		if err != nil {
			return reflect.Zero(typeOf).Interface().(T), err
		}

		return reflect.ValueOf(val).Convert(typeOf).Interface().(T), nil
	}

	// Initialize empty type requested
	var t T

	// Get TypeOf reflection
	to := reflect.TypeOf(t)

	// Parse not basic types
	switch to.String() {
	case "time.Duration":
		return Result(time.ParseDuration(value))
	case "time.Time":
		return Result(time.Parse(time.RFC3339, value))
	case "*url.URL":
		return Result(url.Parse(value))
	case "url.Values":
		return Result(url.ParseQuery(value))
	case "gommon.Base64":
		return Result(ParseBase64(value))
	case "gommon.DSN":
		return Result(ParseDSN(value))
	case "gommon.Timestamp":
		return Result(ParseTimestamp(value))
	}

	// Parse basic types
	switch to.Kind() {
	case reflect.String:
		return Result(value, nil)
	case reflect.Bool:
		return Result(ParseBool(value))
	case reflect.Int:
		return Result(strconv.ParseInt(value, 10, 0))
	case reflect.Int8:
		return Result(strconv.ParseInt(value, 10, 8))
	case reflect.Int16:
		return Result(strconv.ParseInt(value, 10, 16))
	case reflect.Int32:
		return Result(strconv.ParseInt(value, 10, 32))
	case reflect.Int64:
		return Result(strconv.ParseInt(value, 10, 64))
	case reflect.Uint:
		return Result(strconv.ParseUint(value, 10, 0))
	case reflect.Uint8:
		return Result(strconv.ParseUint(value, 10, 8))
	case reflect.Uint16:
		return Result(strconv.ParseUint(value, 10, 16))
	case reflect.Uint32:
		return Result(strconv.ParseUint(value, 10, 32))
	case reflect.Uint64:
		return Result(strconv.ParseUint(value, 10, 64))
	case reflect.Uintptr:
		return Result(strconv.ParseUint(value, 16, 64))
	case reflect.Float32:
		return Result(strconv.ParseFloat(value, 32))
	case reflect.Float64:
		return Result(strconv.ParseFloat(value, 64))
	case reflect.Complex64:
		return Result(strconv.ParseComplex(value, 64))
	case reflect.Complex128:
		return Result(strconv.ParseComplex(value, 128))
	default:
		return Result(nil, fmt.Errorf("can't parse %s", to.Kind()))
	}
}

// ParseBool returns the boolean value represented by the string.
// It accepts 1, t, T, TRUE, true, True, On, on, 0, f, F, FALSE, false, False, Off, off.
// Any other value returns an error.
func ParseBool(str string) (bool, error) {
	switch str {
	case "1", "t", "T", "true", "TRUE", "True", "On", "on":
		return true, nil
	case "0", "f", "F", "false", "FALSE", "False", "Off", "off":
		return false, nil
	}
	return false, &strconv.NumError{Func: "ParseBool", Num: str, Err: strconv.ErrSyntax}
}

// Ptr return pointer of the value.
func Ptr[T comparable](v T) *T {
	return &v
}

func UnPtr[T any](v *T) T {
	to := reflect.ValueOf(v)
	if to.Kind() == reflect.Ptr {
		to = to.Elem()
	}
	return to.Interface().(T)
}

// Split a string into a list of type.
func Split[T comparable](value, sep string) []T {
	array := make([]T, 0)

	for _, item := range strings.Split(value, sep) {
		if val, err := Parse[T](item); err == nil {
			array = append(array, val)
		}
	}

	return array
}

// SplitSlice a strings into a list of type.
func SplitSlice[T comparable](values []string, sep string) []T {
	slice := make([]T, 0)

	for _, value := range values {
		for _, item := range strings.Split(value, sep) {
			if val, err := Parse[T](item); err == nil {
				slice = append(slice, val)
			}
		}
	}

	return slice
}

// Ternary takes a test boolean value, and two any values.
// If the test is true, the first value will be returned.
// If the test is empty, the second value will be returned.
func Ternary[T any](v bool, vTrue, vFalse T) T {
	if v {
		return vTrue
	}

	return vFalse
}

// Uniq remove duplicate entries
func Uniq[T comparable](v []T) []T {
	tmp := make([]T, 0)

	for _, value := range v {
		if !Has(value, tmp...) {
			tmp = append(tmp, value)
		}
	}

	return tmp
}

// WrapParse return any instead of type
func WrapParse[T any](value string) (any, error) {
	return Parse[T](value)
}

// Zero return empty item of type
func Zero[T any]() T {
	return reflect.Zero(reflect.TypeOf(new(T)).Elem()).Interface().(T)
}

// Converter interface for Convert
type Converter interface {
	Interface() any
	ToString() Value[string]
	ToBool() Value[bool]
	ToInt() Value[int]
	ToInt8() Value[int8]
	ToInt16() Value[int16]
	ToInt32() Value[int32]
	ToInt64() Value[int64]
	ToUint() Value[uint]
	ToUint8() Value[uint8]
	ToUint16() Value[uint16]
	ToUint32() Value[uint32]
	ToUint64() Value[uint64]
	ToUintPtr() Value[uintptr]
	ToFloat32() Value[float32]
	ToFloat64() Value[float64]
	ToComplex64() Value[complex64]
	ToComplex128() Value[complex128]
}

// Convert value to comparable types
func Convert(v any) Converter { return convert{any: v} }

// convert implement Converter
type convert struct{ any }

func (v convert) Interface() any {
	return v.any
}

func (v convert) ToString() Value[string] {
	switch vo := reflect.ValueOf(v.any); vo.Kind() {
	case reflect.Bool:
		return toV[string](strconv.FormatBool(vo.Bool()))
	case reflect.Int, reflect.Int8, reflect.Int16, reflect.Int32, reflect.Int64:
		return toV[string](strconv.FormatInt(vo.Int(), 10))
	case reflect.Uint, reflect.Uint8, reflect.Uint16, reflect.Uint32, reflect.Uint64:
		return toV[string](strconv.FormatUint(vo.Uint(), 10))
	case reflect.Uintptr:
		return toV[string](strconv.FormatUint(vo.Uint(), 16))
	case reflect.Float32:
		return toV[string](strconv.FormatFloat(vo.Float(), 'f', -1, 32))
	case reflect.Float64:
		return toV[string](strconv.FormatFloat(vo.Float(), 'f', -1, 64))
	case reflect.Complex64:
		return toV[string](strconv.FormatComplex(vo.Complex(), 'f', -1, 64))
	case reflect.Complex128:
		return toV[string](strconv.FormatComplex(vo.Complex(), 'f', -1, 128))
	case reflect.String:
		return toV[string](vo.String())
	default:
		return zeroToV[string]()
	}
}

func (v convert) ToBool() Value[bool] {
	switch vo := reflect.ValueOf(v.any); vo.Kind() {
	case reflect.Bool:
		return toV[bool](vo.Bool())
	case reflect.Int, reflect.Int8, reflect.Int16, reflect.Int32, reflect.Int64:
		return numberToBooleanV(vo.Int())
	case reflect.Uint, reflect.Uint8, reflect.Uint16, reflect.Uint32, reflect.Uint64:
		return numberToBooleanV(vo.Uint())
	case reflect.String:
		return functionToV[bool](strconv.ParseBool(vo.String()))
	default:
		return zeroToV[bool]()
	}
}

func (v convert) ToInt() Value[int] {
	switch vo := reflect.ValueOf(v.any); vo.Kind() {
	case reflect.Bool:
		return Ternary(vo.Bool(), toV[int](1), toV[int](0))
	case reflect.Int, reflect.Int8, reflect.Int16, reflect.Int32, reflect.Int64:
		return convertToV[int](vo.Int())
	case reflect.Uint, reflect.Uint8, reflect.Uint16, reflect.Uint32, reflect.Uint64:
		return convertToV[int](vo.Uint())
	case reflect.Uintptr:
		return convertToV[int](vo.Uint())
	case reflect.Float32, reflect.Float64:
		return convertToV[int](vo.Float())
	case reflect.Complex64, reflect.Complex128:
		return convertToV[int](vo.Complex())
	case reflect.String:
		return convertFunctionToV[int](strconv.ParseInt(vo.String(), 10, 32))
	default:
		return zeroToV[int]()
	}
}

func (v convert) ToInt8() Value[int8] {
	switch vo := reflect.ValueOf(v.any); vo.Kind() {
	case reflect.Bool:
		return Ternary(vo.Bool(), toV[int8](1), toV[int8](0))
	case reflect.Int, reflect.Int8, reflect.Int16, reflect.Int32, reflect.Int64:
		return convertToV[int8](vo.Int())
	case reflect.Uint, reflect.Uint8, reflect.Uint16, reflect.Uint32, reflect.Uint64:
		return convertToV[int8](vo.Uint())
	case reflect.Uintptr:
		return convertToV[int8](vo.Uint())
	case reflect.Float32, reflect.Float64:
		return convertToV[int8](vo.Float())
	case reflect.Complex64, reflect.Complex128:
		return convertToV[int8](vo.Complex())
	case reflect.String:
		return convertFunctionToV[int8](strconv.ParseInt(vo.String(), 10, 8))
	default:
		return zeroToV[int8]()
	}
}

func (v convert) ToInt16() Value[int16] {
	switch vo := reflect.ValueOf(v.any); vo.Kind() {
	case reflect.Bool:
		return Ternary(vo.Bool(), toV[int16](1), toV[int16](0))
	case reflect.Int, reflect.Int8, reflect.Int16, reflect.Int32, reflect.Int64:
		return convertToV[int16](vo.Int())
	case reflect.Uint, reflect.Uint8, reflect.Uint16, reflect.Uint32, reflect.Uint64:
		return convertToV[int16](vo.Uint())
	case reflect.Uintptr:
		return convertToV[int16](vo.Uint())
	case reflect.Float32, reflect.Float64:
		return convertToV[int16](vo.Float())
	case reflect.Complex64, reflect.Complex128:
		return convertToV[int16](vo.Complex())
	case reflect.String:
		return convertFunctionToV[int16](strconv.ParseInt(vo.String(), 10, 16))
	default:
		return zeroToV[int16]()
	}
}

func (v convert) ToInt32() Value[int32] {
	switch vo := reflect.ValueOf(v.any); vo.Kind() {
	case reflect.Bool:
		return Ternary(vo.Bool(), toV[int32](1), toV[int32](0))
	case reflect.Int, reflect.Int8, reflect.Int16, reflect.Int32, reflect.Int64:
		return convertToV[int32](vo.Int())
	case reflect.Uint, reflect.Uint8, reflect.Uint16, reflect.Uint32, reflect.Uint64:
		return convertToV[int32](vo.Uint())
	case reflect.Uintptr:
		return convertToV[int32](vo.Uint())
	case reflect.Float32, reflect.Float64:
		return convertToV[int32](vo.Float())
	case reflect.Complex64, reflect.Complex128:
		return convertToV[int32](vo.Complex())
	case reflect.String:
		return convertFunctionToV[int32](strconv.ParseInt(vo.String(), 10, 32))
	default:
		return zeroToV[int32]()
	}
}

func (v convert) ToInt64() Value[int64] {
	switch vo := reflect.ValueOf(v.any); vo.Kind() {
	case reflect.Bool:
		return Ternary(vo.Bool(), toV[int64](1), toV[int64](0))
	case reflect.Int, reflect.Int8, reflect.Int16, reflect.Int32, reflect.Int64:
		return convertToV[int64](vo.Int())
	case reflect.Uint, reflect.Uint8, reflect.Uint16, reflect.Uint32, reflect.Uint64:
		return convertToV[int64](vo.Uint())
	case reflect.Uintptr:
		return convertToV[int64](vo.Uint())
	case reflect.Float32, reflect.Float64:
		return convertToV[int64](vo.Float())
	case reflect.Complex64, reflect.Complex128:
		return convertToV[int64](vo.Complex())
	case reflect.String:
		return convertFunctionToV[int64](strconv.ParseInt(vo.String(), 10, 64))
	default:
		return zeroToV[int64]()
	}
}

func (v convert) ToUint() Value[uint] {
	switch vo := reflect.ValueOf(v.any); vo.Kind() {
	case reflect.Bool:
		return Ternary(vo.Bool(), toV[uint](1), toV[uint](0))
	case reflect.Int, reflect.Int8, reflect.Int16, reflect.Int32, reflect.Int64:
		return convertToV[uint](vo.Int())
	case reflect.Uint, reflect.Uint8, reflect.Uint16, reflect.Uint32, reflect.Uint64:
		return convertToV[uint](vo.Uint())
	case reflect.Uintptr:
		return convertToV[uint](vo.Uint())
	case reflect.Float32, reflect.Float64:
		return convertToV[uint](vo.Float())
	case reflect.Complex64, reflect.Complex128:
		return convertToV[uint](vo.Complex())
	case reflect.String:
		return convertFunctionToV[uint](strconv.ParseUint(vo.String(), 10, 32))
	default:
		return zeroToV[uint]()
	}
}

func (v convert) ToUint8() Value[uint8] {
	switch vo := reflect.ValueOf(v.any); vo.Kind() {
	case reflect.Bool:
		return Ternary(vo.Bool(), toV[uint8](1), toV[uint8](0))
	case reflect.Int, reflect.Int8, reflect.Int16, reflect.Int32, reflect.Int64:
		return convertToV[uint8](vo.Int())
	case reflect.Uint, reflect.Uint8, reflect.Uint16, reflect.Uint32, reflect.Uint64:
		return convertToV[uint8](vo.Uint())
	case reflect.Uintptr:
		return convertToV[uint8](vo.Uint())
	case reflect.Float32, reflect.Float64:
		return convertToV[uint8](vo.Float())
	case reflect.Complex64, reflect.Complex128:
		return convertToV[uint8](vo.Complex())
	case reflect.String:
		return convertFunctionToV[uint8](strconv.ParseUint(vo.String(), 10, 8))
	default:
		return zeroToV[uint8]()
	}
}

func (v convert) ToUint16() Value[uint16] {
	switch vo := reflect.ValueOf(v.any); vo.Kind() {
	case reflect.Bool:
		return Ternary(vo.Bool(), toV[uint16](1), toV[uint16](0))
	case reflect.Int, reflect.Int8, reflect.Int16, reflect.Int32, reflect.Int64:
		return convertToV[uint16](vo.Int())
	case reflect.Uint, reflect.Uint8, reflect.Uint16, reflect.Uint32, reflect.Uint64:
		return convertToV[uint16](vo.Uint())
	case reflect.Uintptr:
		return convertToV[uint16](vo.Uint())
	case reflect.Float32, reflect.Float64:
		return convertToV[uint16](vo.Float())
	case reflect.Complex64, reflect.Complex128:
		return convertToV[uint16](vo.Complex())
	case reflect.String:
		return convertFunctionToV[uint16](strconv.ParseUint(vo.String(), 10, 16))
	default:
		return zeroToV[uint16]()
	}
}

func (v convert) ToUint32() Value[uint32] {
	switch vo := reflect.ValueOf(v.any); vo.Kind() {
	case reflect.Bool:
		return Ternary(vo.Bool(), toV[uint32](1), toV[uint32](0))
	case reflect.Int, reflect.Int8, reflect.Int16, reflect.Int32, reflect.Int64:
		return convertToV[uint32](vo.Int())
	case reflect.Uint, reflect.Uint8, reflect.Uint16, reflect.Uint32, reflect.Uint64:
		return convertToV[uint32](vo.Uint())
	case reflect.Uintptr:
		return convertToV[uint32](vo.Uint())
	case reflect.Float32, reflect.Float64:
		return convertToV[uint32](vo.Float())
	case reflect.Complex64, reflect.Complex128:
		return convertToV[uint32](vo.Complex())
	case reflect.String:
		return convertFunctionToV[uint32](strconv.ParseUint(vo.String(), 10, 32))
	default:
		return zeroToV[uint32]()
	}
}

func (v convert) ToUint64() Value[uint64] {
	switch vo := reflect.ValueOf(v.any); vo.Kind() {
	case reflect.Bool:
		return Ternary(vo.Bool(), toV[uint64](1), toV[uint64](0))
	case reflect.Int, reflect.Int8, reflect.Int16, reflect.Int32, reflect.Int64:
		return convertToV[uint64](vo.Int())
	case reflect.Uint, reflect.Uint8, reflect.Uint16, reflect.Uint32, reflect.Uint64:
		return convertToV[uint64](vo.Uint())
	case reflect.Uintptr:
		return convertToV[uint64](vo.Uint())
	case reflect.Float32, reflect.Float64:
		return convertToV[uint64](vo.Float())
	case reflect.Complex64, reflect.Complex128:
		return convertToV[uint64](vo.Complex())
	case reflect.String:
		return convertFunctionToV[uint64](strconv.ParseUint(vo.String(), 10, 64))
	default:
		return zeroToV[uint64]()
	}
}

func (v convert) ToUintPtr() Value[uintptr] {
	switch vo := reflect.ValueOf(v.any); vo.Kind() {
	case reflect.Int, reflect.Int8, reflect.Int16, reflect.Int32, reflect.Int64:
		return convertToV[uintptr](vo.Int())
	case reflect.Uint, reflect.Uint8, reflect.Uint16, reflect.Uint32, reflect.Uint64:
		return convertToV[uintptr](vo.Uint())
	case reflect.Uintptr:
		return convertToV[uintptr](vo.Uint())
	case reflect.Float32, reflect.Float64:
		return convertToV[uintptr](vo.Float())
	case reflect.Complex64, reflect.Complex128:
		return convertToV[uintptr](vo.Complex())
	case reflect.String:
		return convertFunctionToV[uintptr](strconv.ParseUint(vo.String(), 16, 64))
	default:
		return zeroToV[uintptr]()
	}
}

func (v convert) ToFloat32() Value[float32] {
	switch vo := reflect.ValueOf(v.any); vo.Kind() {
	case reflect.Bool:
		return Ternary(vo.Bool(), toV[float32](1), toV[float32](0))
	case reflect.Int, reflect.Int8, reflect.Int16, reflect.Int32, reflect.Int64:
		return convertToV[float32](vo.Int())
	case reflect.Uint, reflect.Uint8, reflect.Uint16, reflect.Uint32, reflect.Uint64:
		return convertToV[float32](vo.Uint())
	case reflect.Uintptr:
		return convertToV[float32](vo.Uint())
	case reflect.Float32, reflect.Float64:
		return convertToV[float32](vo.Float())
	case reflect.String:
		return convertFunctionToV[float32](strconv.ParseFloat(vo.String(), 32))
	default:
		return zeroToV[float32]()
	}
}

func (v convert) ToFloat64() Value[float64] {
	switch vo := reflect.ValueOf(v.any); vo.Kind() {
	case reflect.Bool:
		return Ternary(vo.Bool(), toV[float64](1), toV[float64](0))
	case reflect.Int, reflect.Int8, reflect.Int16, reflect.Int32, reflect.Int64:
		return convertToV[float64](vo.Int())
	case reflect.Uint, reflect.Uint8, reflect.Uint16, reflect.Uint32, reflect.Uint64:
		return convertToV[float64](vo.Uint())
	case reflect.Uintptr:
		return convertToV[float64](vo.Uint())
	case reflect.Float32, reflect.Float64:
		return convertToV[float64](vo.Float())
	case reflect.String:
		return convertFunctionToV[float64](strconv.ParseFloat(vo.String(), 64))
	default:
		return zeroToV[float64]()
	}
}

func (v convert) ToComplex64() Value[complex64] {
	switch vo := reflect.ValueOf(v.any); vo.Kind() {
	case reflect.Complex64, reflect.Complex128:
		return convertToV[complex64](vo.Complex())
	case reflect.String:
		return convertFunctionToV[complex64](strconv.ParseComplex(vo.String(), 64))
	default:
		return zeroToV[complex64]()
	}
}

func (v convert) ToComplex128() Value[complex128] {
	switch vo := reflect.ValueOf(v.any); vo.Kind() {
	case reflect.Complex64, reflect.Complex128:
		return convertToV[complex128](vo.Complex())
	case reflect.String:
		return convertFunctionToV[complex128](strconv.ParseComplex(vo.String(), 128))
	default:
		return zeroToV[complex128]()
	}
}

// NewCopy create and copy a new object
func NewCopy[T any](v T) T {
	src := reflect.ValueOf(v)
	dst := reflect.Indirect(reflect.New(src.Type()))

	if err := CopyValue(src, dst); err != nil {
		panic(err)
	}

	return dst.Interface().(T)
}

// Copy source into destination
func Copy[T any](src T, dst *T) {
	if err := CopyValue(reflect.ValueOf(src), reflect.Indirect(reflect.ValueOf(dst))); err != nil {
		panic(err)
	}
}

// CopyValue source reflection object to destination reflection object
func CopyValue(src, dst reflect.Value) error {
	// Verify if source and destination has same type
	if src.Kind() != dst.Kind() {
		return errors.New("source and destination do not have the same type for " + src.Type().Name())
	}

	// Ignore functions
	if src.Kind() == reflect.Func {
		return nil
	}

	// Convert source to addressable
	if !src.CanAddr() {
		tmp := reflect.New(src.Type()).Elem()
		tmp.Set(src)
		src = tmp
	}

	// Read unexported field
	if !src.CanSet() || !src.CanInterface() {
		src = reflect.NewAt(src.Type(), unsafe.Pointer(src.UnsafeAddr())).Elem()
	}

	// Copy pointer
	if src.Kind() == reflect.Pointer {
		// Ignore if source in empty
		if src.IsNil() {
			return nil
		}

		// Initialize temporary object
		tmp := reflect.New(src.Elem().Type())

		// Copy source child into temporary
		if err := CopyValue(src.Elem(), tmp.Elem()); err != nil {
			return err
		}

		// Write temporary into destination
		reflect.NewAt(dst.Type(), unsafe.Pointer(dst.UnsafeAddr())).Elem().Set(tmp)

		return nil
	}

	// Copy struct
	if src.Kind() == reflect.Struct {
		for i := 0; i < src.NumField(); i++ {
			if err := CopyValue(src.Field(i), dst.Field(i)); err != nil {
				return err
			}
		}

		for i := 0; i < src.NumMethod(); i++ {
			if err := CopyValue(src.Method(i), dst.Method(i)); err != nil {
				return err
			}
		}

		return nil
	}

	// Copy map
	if src.Kind() == reflect.Map {
		// Ignore if source in empty
		if src.IsNil() {
			return nil
		}

		// Initialize temporary map
		tmp := reflect.MakeMap(src.Type())

		// Copy source child into temporary
		for iter := src.MapRange(); iter.Next(); {
			k := reflect.Indirect(reflect.New(iter.Key().Type()))
			v := reflect.Indirect(reflect.New(iter.Value().Type()))

			if err := CopyValue(iter.Key(), k); err != nil {
				return err
			}

			if err := CopyValue(iter.Value(), v); err != nil {
				return err
			}

			tmp.SetMapIndex(k, v)
		}

		// Write temporary into destination
		reflect.NewAt(dst.Type(), unsafe.Pointer(dst.UnsafeAddr())).Elem().Set(tmp)

		return nil
	}

	// Copy slice
	if src.Kind() == reflect.Slice {
		// Ignore if source in empty
		if src.IsNil() {
			return nil
		}

		// Initialize temporary slice
		tmp := reflect.MakeSlice(src.Type(), src.Len(), src.Cap())

		// Copy source child into temporary
		for i := 0; i < src.Len(); i++ {
			if err := CopyValue(src.Index(i), tmp.Index(i)); err != nil {
				return err
			}
		}

		// Write temporary into destination
		reflect.NewAt(dst.Type(), unsafe.Pointer(dst.UnsafeAddr())).Elem().Set(tmp)

		return nil
	}

	// Copy array
	if src.Kind() == reflect.Array {
		// Initialize temporary array
		tmp := reflect.Indirect(reflect.New(src.Type()))

		// Copy source child into temporary
		for i := 0; i < src.Len(); i++ {
			if err := CopyValue(src.Index(i), tmp.Index(i)); err != nil {
				return err
			}
		}

		// Write temporary into destination
		reflect.NewAt(dst.Type(), unsafe.Pointer(dst.UnsafeAddr())).Elem().Set(tmp)

		return nil
	}

	// Verify if destination is writable
	if !dst.CanAddr() {
		return errors.New("destination should be addressable")
	}

	// Write everything into destination
	reflect.NewAt(dst.Type(), unsafe.Pointer(dst.UnsafeAddr())).Elem().Set(src)

	return nil
}
