package env_test

import (
	"bytes"
	"fmt"
	"os"

	"gitlab.com/evolves-fr/gommon/env"
)

func ExampleUnmarshal() {
	type Config struct {
		String string            `env:"STRING"`
		Slice  []string          `env:"SLICE"`
		Map    map[string]string `env:"MAP"`
		Struct struct {
			Int int `env:"INT"`
		} `env:",inline"`
	}

	_ = os.Setenv("STRING", "value")
	_ = os.Setenv("SLICE_0", "slice-value1")
	_ = os.Setenv("SLICE_1", "slice-value2")
	_ = os.Setenv("SLICE_2", "slice-value3")
	_ = os.Setenv("MAP_KEY1", "map-value1")
	_ = os.Setenv("MAP_KEY2", "map-value2")
	_ = os.Setenv("MAP_KEY3", "map-value3")
	_ = os.Setenv("INT", "123")

	var config Config

	if err := env.Unmarshal(&config); err != nil {
		panic(err)
	}

	fmt.Printf("String = %v\n", config.String)
	fmt.Printf("Slice  = %v\n", config.Slice)
	fmt.Printf("Map    = %v\n", config.Map)
	fmt.Printf("Struct = %#v\n", config.Struct)

	// Output:
	// String = value
	// Slice  = [slice-value1 slice-value2 slice-value3]
	// Map    = map[key1:map-value1 key2:map-value2 key3:map-value3]
	// Struct = struct { Int int "env:\"INT\"" }{Int:123}
}

func ExampleUnmarshalWithPrefix() {
	type Config struct {
		String string            `env:"STRING"`
		Slice  []string          `env:"SLICE"`
		Map    map[string]string `env:"MAP"`
		Struct struct {
			Int int `env:"INT"`
		} `env:",inline"`
	}

	_ = os.Setenv("TEST_STRING", "value")
	_ = os.Setenv("TEST_SLICE_0", "slice-value1")
	_ = os.Setenv("TEST_SLICE_1", "slice-value2")
	_ = os.Setenv("TEST_SLICE_2", "slice-value3")
	_ = os.Setenv("TEST_MAP_KEY1", "map-value1")
	_ = os.Setenv("TEST_MAP_KEY2", "map-value2")
	_ = os.Setenv("TEST_MAP_KEY3", "map-value3")
	_ = os.Setenv("TEST_INT", "123")

	var config Config

	if err := env.UnmarshalWithPrefix(&config, "TEST"); err != nil {
		panic(err)
	}

	fmt.Printf("String = %v\n", config.String)
	fmt.Printf("Slice  = %v\n", config.Slice)
	fmt.Printf("Map    = %v\n", config.Map)
	fmt.Printf("Struct = %#v\n", config.Struct)

	// Output:
	// String = value
	// Slice  = [slice-value1 slice-value2 slice-value3]
	// Map    = map[key1:map-value1 key2:map-value2 key3:map-value3]
	// Struct = struct { Int int "env:\"INT\"" }{Int:123}
}

func ExampleUnmarshalDotEnv() {
	type Config struct {
		String string            `env:"STRING"`
		Slice  []string          `env:"SLICE"`
		Map    map[string]string `env:"MAP"`
		Struct struct {
			Int int `env:"INT"`
		} `env:",inline"`
	}

	var dotenv bytes.Buffer
	dotenv.WriteString("STRING=value\n")
	dotenv.WriteString("SLICE_0=slice-value1\n")
	dotenv.WriteString("SLICE_1=slice-value2\n")
	dotenv.WriteString("SLICE_2=slice-value3\n")
	dotenv.WriteString("MAP_KEY1=map-value1\n")
	dotenv.WriteString("MAP_KEY2=map-value2\n")
	dotenv.WriteString("MAP_KEY3=map-value3\n")
	dotenv.WriteString("INT=123\n")

	var config Config

	if err := env.UnmarshalDotEnv(dotenv.Bytes(), &config); err != nil {
		panic(err)
	}

	fmt.Printf("String = %v\n", config.String)
	fmt.Printf("Slice  = %v\n", config.Slice)
	fmt.Printf("Map    = %v\n", config.Map)
	fmt.Printf("Struct = %#v\n", config.Struct)

	// Output:
	// String = value
	// Slice  = [slice-value1 slice-value2 slice-value3]
	// Map    = map[key1:map-value1 key2:map-value2 key3:map-value3]
	// Struct = struct { Int int "env:\"INT\"" }{Int:123}
}

func ExampleUnmarshalDotEnvWithPrefix() {
	type Config struct {
		String string            `env:"STRING"`
		Slice  []string          `env:"SLICE"`
		Map    map[string]string `env:"MAP"`
		Struct struct {
			Int int `env:"INT"`
		} `env:",inline"`
	}

	var dotenv bytes.Buffer
	dotenv.WriteString("TEST_STRING=value\n")
	dotenv.WriteString("TEST_SLICE_0=slice-value1\n")
	dotenv.WriteString("TEST_SLICE_1=slice-value2\n")
	dotenv.WriteString("TEST_SLICE_2=slice-value3\n")
	dotenv.WriteString("TEST_MAP_KEY1=map-value1\n")
	dotenv.WriteString("TEST_MAP_KEY2=map-value2\n")
	dotenv.WriteString("TEST_MAP_KEY3=map-value3\n")
	dotenv.WriteString("TEST_INT=123\n")

	var config Config

	if err := env.UnmarshalDotEnvWithPrefix(dotenv.Bytes(), &config, "TEST"); err != nil {
		panic(err)
	}

	fmt.Printf("String = %v\n", config.String)
	fmt.Printf("Slice  = %v\n", config.Slice)
	fmt.Printf("Map    = %v\n", config.Map)
	fmt.Printf("Struct = %#v\n", config.Struct)

	// Output:
	// String = value
	// Slice  = [slice-value1 slice-value2 slice-value3]
	// Map    = map[key1:map-value1 key2:map-value2 key3:map-value3]
	// Struct = struct { Int int "env:\"INT\"" }{Int:123}
}
