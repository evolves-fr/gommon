package gommon_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/evolves-fr/gommon"
)

func TestSemverCompare(t *testing.T) {
	for constraint, version := range map[string]string{
		"1.0.0":   "1.0.0",
		">=1.0.0": "1.0.0",
		">1.0.0":  "2.0.0",
	} {
		res, err := gommon.SemverCompare(constraint, version)
		assert.NoError(t, err)
		assert.Equal(t, true, res)
	}

	for constraint, version := range map[string]string{
		"1.0.0":   "1.0.1",
		">=1.1.0": "1.0.0",
		">1.0.0":  "0.2.0",
	} {
		res, err := gommon.SemverCompare(constraint, version)
		assert.NoError(t, err)
		assert.Equal(t, false, res)
	}

	for constraint, version := range map[string]string{
		">=":      "1.0.0",
		">=1.0.0": "1.",
	} {
		res, err := gommon.SemverCompare(constraint, version)
		assert.Error(t, err)
		assert.Equal(t, false, res)
	}
}
