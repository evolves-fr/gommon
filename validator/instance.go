package validator

import v10 "github.com/go-playground/validator/v10"

func New() *v10.Validate {
	validate := v10.New()

	validators := map[string]v10.Func{
		"duration": isDuration,
		"domain":   isDomain,
	}

	for tag, fn := range validators {
		_ = validate.RegisterValidation(tag, fn)
	}

	return validate
}
