package tests

import "io"

func ErrReadWriter(err error) io.ReadWriter {
	return &errReadWriter{err: err}
}

type errReadWriter struct {
	err error
}

func (rw *errReadWriter) Read([]byte) (int, error) {
	return 0, rw.err
}

func (rw *errReadWriter) Write([]byte) (int, error) {
	return 0, rw.err
}
